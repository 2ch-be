$(document).ready(function(){
	var load     = 0;
	var text_max = 250;
		$('#sendMessage').validate({
		    rules: {
		      zxcvbnm: {
		        required: true,
		        number: true,
		        minlength: 9,
				maxlength: 9
		      },
		      subj: {
		        required: true
		      },
		      msg: {
		        required: true,
		      }
		    },
			// highlight: function(element) {
			// 	$(element)
			// 	.closest('.form-group').addClass('has-error').removeClass('has-success');
			// },
			// success: function(element) {
			// 	element
			// 	.closest('.form-group').removeClass('has-error').addClass('has-success');
			// }
			highlight: function(element) {
			    $(element).closest('.form-group').addClass('has-error');
			},
			unhighlight: function(element) {
			    $(element).closest('.form-group').removeClass('has-error');
			},
			errorElement: 'span',
			errorClass: 'help-block',
			errorPlacement: function(error, element) {
			    if(element.parent('.input-group').length) {
			        error.insertAfter(element.parent());
			    } else {
			        error.insertAfter(element);
			    }
			}
	  });

	  var load = 0;
	  $('#page').scroll(function(){
	      var r = $('#page')[0].scrollHeight - $('#page').height();

	      if($('#page').scrollTop() == r){

	      load++;

	      $.post( "try.php", {load:load}, function( data ) {
	  		    $( "#list" ).append( data );
	      });
	    }
	  });

	  $('#textarea_feedback').val(text_max);

	  $('#messageBox').keyup(function() {
	      var text_length = $('#messageBox').val().length;
	      var text_remaining = text_max - text_length;

	      $('#textarea_feedback').val(text_remaining);
	  });
});