<?php
include("../encrypt_artist.php");
header('Content-Type: application/json');
if(!isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
{
	header("Location: ../artist.php");
} 
if(!empty($_SESSION['artistid'])){
	if(!empty($_POST['category'])){
		$category  = trim($_POST['category']);
		$id        = rand(1000000,9999999);
		$writeData = "<id>{$id}</id><aid>".$_SESSION['artistid']."</aid><classname>{$category}</classname>\n";
		
		$filename  = ADBD_PATH."class.log";
		$check     = file(ADBD_PATH."class.log");
		$thou      = TRUE;
		foreach ($check as $key => $value) {
			if($category==within_str($value, "<classname>", "</classname>")) {
				$thou = FALSE;
				break;
			}
		}
		if($thou){
				if (!file_exists($filename)) {
					if(!file_put_contents($filename,$writeData)) {
						$writeSuccess = false;
					}
						$writeSuccess = true;
				} else {
					$handle = fopen($filename, "a");
					if(!fwrite($handle, $writeData)) {
						$writeSuccess = false;
					}else{
						$writeSuccess = true;
					}
					fclose($handle);
				}
			if($writeSuccess){
				echo json_encode(array("a" => "success", "b" => $id));
			}else{
				echo json_encode(array("a" => "FALSE"));
			}
		}else{
			echo json_encode(array("a" => "FALSE"));
		}

	}else{
			echo json_encode(array("a" => "FALSE"));
	}
}else{
		echo json_encode(array("a" => "FALSE"));
}

?>