<?php 
include 'config.php'; 
$page = "Conversation Display";
include 'include/header.php'; 
$inbox = new Message;
?>
<body>
        <div class="container margintopandbottom">
        <?php include 'menu.php'; ?>
        <div class="col-md-12">
        <div class="panel panel-default"><h1 class="centertext">Be Messaging Monitoring System</h1>
        </div>
        <div class="well well-sm centerdiv main2">
        <div class="row">
        <div class="col-md-6">
       <table class="table table-bordered table-striped table-responsive">
        <tr>
                <td><strong>No.</strong></td>
                <td><strong>Conversation</strong></td>
                <td><strong>Number of Messages</strong></td>
                
        </tr>
        <?php
        
        if(!empty($_GET['em'])){
                $email = trim($_GET['em']);
                if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
                        echo "invalid email";
                        exit();
                }
                
                $uid   = $inbox->GetID($email,$id_mail_path);
                $key   = $inbox->loggedEmail($_GET['em'], MDB_PATH);
                $count = 0;
                $breaker = 0;
                if($d = opendir($key)){
                        while(($file = readdir($d))!==false){ 
			//echo $file;			
				if(!($file == "." || $file == ".." ||$file =="del" ||  $file == "noti.txt")){
                                if(!($file == "ban.txt")){
                                $linecount = 0;
                                $fi = fopen($key.$file, "r");
                                while(!feof($fi)){
                                  $line = fgets($fi);
                                  $linecount++;
                                }

                                $linecount = $linecount-1;
                                $file = substr($file, 0, -4);
                               
                                        $count++;
                                        $arrayContainer[$file] = $linecount;

                                }   

                        }

                        
                        }
                        
                        asort($arrayContainer);
                        $arrayContainer = array_reverse($arrayContainer, TRUE);

                        
                        foreach ($arrayContainer as $file => $linecount) {
                                $breaker++;
                                echo "<tr>
                                <td>{$breaker}</td>
                                <td><a href=\"conversation.php?f={$file}&em={$email}\">{$file}</a></td>
                                <td>{$linecount}</td>
                                </tr>";
                        }
                        
                }

        }

echo "</table></div>";
echo "<h4><span class=\"label label-default\">File: {$_GET['f']}</span></h4>";
echo "<div class=\"col-md-6 conv\">";
        if(!empty($_GET['f'])) {
                // $fl = trim()
                 if(!filter_var($_GET['f'], FILTER_VALIDATE_INT)){
                                        echo "invalid file name";
                                        exit();
                }
                $d             = strip_tags(addslashes(trim($_GET['f'])));
                $em             = strip_tags(addslashes(trim($_GET['em'])));
                $messages      = $inbox->specificMDB($em, $d, FALSE);
                $senderCounter = $inbox->specificMDB($em, $d, TRUE);      
             for ($i=0; $i < count($messages); $i++) {
                $leftRightCounter = ($inbox->within_str($messages[$i], "<from>", "</from>")==$uid) ? "r mdes panel panel-default pull-left txt" : "s mdes panel panel-default pull-right txt";
                echo "<div class=\"".$leftRightCounter."\">". urldecode($inbox->within_str($messages[$i], "<msg>", "</msg>")) . "<br><small class=\"d pull-right\">". date("Y-m-d g:i a",urldecode($inbox->within_str($messages[$i], "<id>", "</id>"))) ."</small></div>";
                echo "<div class='clearfix'></div>";
}

                
        }

         ?>
        
        
        </div>
        
        </div>  
        </div>
        </div>
</div>

</body>
</html>
