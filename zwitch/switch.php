<?php
$ind_path = "/home/auth/secure_html/control/indicator.txt";
$aa = "";

if(isset($_POST['switch'])) {
  $mode = abs(file_get_contents($ind_path));
  
  $bak_inbox = "/home/auth/secure_html/control/replacement/inbox.bak.php";
  $bak_msg = "/home/auth/secure_html/control/replacement/message.bak.php";
  $bak_mproc = "/home/auth/secure_html/control/replacement/message_proc.bak.php";
  $bak_p = "/home/auth/secure_html/control/replacement/p.bak.php";
  $bak_reply = "/home/auth/secure_html/control/replacement/reply.bak.php";

  $orig_inbox = "/home/auth/public_html/inbox.php";
  $orig_msg = "/home/auth/public_html/message.php";
  $orig_mproc = "/home/auth/public_html/message_proc.php";
  $orig_p = "/home/auth/public_html/test/p.php";
  $orig_reply = "/home/auth/public_html/reply.php";

  $b_orig_inbox = "/home/auth/secure_html/control/original/inbox.orig.php";
  $b_orig_msg = "/home/auth/secure_html/control/original/message.orig.php";
  $b_orig_mproc = "/home/auth/secure_html/control/original/message_proc.orig.php";
  $b_orig_p = "/home/auth/secure_html/control/original/p.orig.php";
  $b_orig_reply = "/home/auth/secure_html/control/original/reply.orig.php";

  if($mode == "0") {
	file_put_contents($orig_inbox,file_get_contents($bak_inbox));
	file_put_contents($orig_msg,file_get_contents($bak_msg));
	file_put_contents($orig_mproc,file_get_contents($bak_mproc));
	file_put_contents($orig_p,file_get_contents($bak_p));
	file_put_contents($orig_reply,file_get_contents($bak_reply));
	file_put_contents($ind_path,"1");
    $aa = "<h4>BE Messaging successfully turned <a style=\"color:red;\">OFF</a></h4>";
  } elseif($mode == "1") {
        file_put_contents($orig_inbox,file_get_contents($b_orig_inbox));
        file_put_contents($orig_msg,file_get_contents($b_orig_msg));
        file_put_contents($orig_mproc,file_get_contents($b_orig_mproc));
        file_put_contents($orig_p,file_get_contents($b_orig_p));
        file_put_contents($orig_reply,file_get_contents($b_orig_reply));
        file_put_contents($ind_path,"0");
    $aa = "<h4>BE Messaging successfully turned <a style=\"color:green;\">ON</a></h4>";
  }
}

$mode = abs(file_get_contents($ind_path));
if($mode == "0")
  $btn = "<input type=\"submit\" name=\"switch\" class=\"btn btn-default\" value=\"OFF\">";
elseif($mode == "1")
  $btn = "<input type=\"submit\" name=\"switch\" class=\"btn btn-default\" value=\"ON\">";

        include 'include/header.php';
?>
  <body><center>
 
<div class="container margintopandbottom">
        <?php include 'menu.php'; ?>
  <form method="post">
<div class="panel panel-default ban"><h2>BE Messaging Switch</h2><br><br>
    Turn <?php echo $btn; ?> BE Messaging.
    <br><br>
    <?php echo $aa; ?>
    </div>
  </form>
</div>
</center>
  </body>
</html>
