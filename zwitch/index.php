<?php 
include 'config.php'; 
include 'include/header.php'; 

function array_sort($array, $on, $order=SORT_ASC) {
  $new_array = array();
  $sortable_array = array();
  if (count($array) > 0) {
    foreach ($array as $k => $v) {
      if (is_array($v)) {
        foreach ($v as $k2 => $v2) {
          if ($k2 == $on) {
            $sortable_array[$k] = $v2;
          }
        }
      } else {
        $sortable_array[$k] = $v;
      }
    }
 
    switch ($order) {
      case SORT_ASC:
        asort($sortable_array);
      break;
      case SORT_DESC:
        arsort($sortable_array);
      break;
    }
 
    foreach ($sortable_array as $k => $v) {
      $new_array[$k] = $array[$k];
    }
  }
 
  return $new_array;
}
?>
<body>
<div class="container margintopandbottom">
        <?php include 'menu.php'; ?>
        <div class="col-md-12">        
        <div class="panel panel-default"><h1 class="centertext">Be Messaging Monitoring System</h1></div>
       <?php
            $inbox = new Message;
                                                
                    
                      $dir = file(DB_PATH."idlist.log");
                    foreach($dir as $key){
                                        $array[0] = explode("<><>", $key);                                                                                               
                                        $worldofArray['directory'] = $inbox->loggedEmail($array[0][1], MDB_PATH); 
                                        $worldofArray['id'] = $array[0][0]; 
                                        if(is_dir($worldofArray['directory'])){
                                                $count = 0;
                                                $hi = explode("/", $worldofArray['directory']);
                                                $strReplaced = $array[0][1];

                                                foreach (glob($worldofArray['directory']."*.dat") as $filename){
                                                        $count++;
                                                        $h = explode("/", $filename);
                                                }
						if($count>0){
                                                $arrayContainer[] = array('directory' => $worldofArray['directory'], 'id' => $worldofArray['id'], 'conversation_count' => $count, 'email' => $strReplaced );
                                 		 }
					}
                                 }                                                                        
                                                   $arrayContainer = array_sort($arrayContainer, 'conversation_count', SORT_DESC);
                            $returned = array_values($arrayContainer);
                            // print_r($returned);
            ?>
       <table class="table table-bordered table-striped table-responsive">
        <thead>
                <td><strong>ID</strong></td>
                <td><strong>Email</strong></td>
                <td><strong>Number of Conversation</strong></td>
        </thead>
                <?php
                $count = count($arrayContainer)/10;
                if(strpos($count,".")>0)
                  $count = $count+1;
                $p = (isset($_GET['p'])&&is_numeric($_GET['p']))?($_GET['p']*10):10;
                $s = (isset($_GET['p'])&&is_numeric($_GET['p']))?$p-10:0;

                for ($i=$s; $i < $p; $i++) { 
                  if(isset($returned[$i]))
                  echo "<tr><td>{$returned[$i]['id']}</td><td><a href=\"conversation.php?em={$returned[$i]['email']}\">{$returned[$i]['email']}</a></td><td>{$returned[$i]['conversation_count']}</td></tr>";                  
                }
                echo "<ul class=\"pagination\" id='page'>";
                for ($i=1; $i <= $count; $i++){ 
                        $active = ($_GET['p']==$i) ? 'class="active"' : '';               
                        echo "<li {$active}><a href='{$livesitePath}index.php?p={$i}'>{$i}</a></li>";
                }
                echo "</ul>";
                ?>
        </table>
        </div>  

</div>
</body>
</html>
