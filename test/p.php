<?php 
include('../encrypt.php');

// SANITIZE
$uid = trim($_GET['i']);
$uid = filter_var($uid,FILTER_SANITIZE_NUMBER_INT);

// VALIDATEE
if (strlen($uid) != 9){
	echo "NG";
	exit;
}

$read  = file($id_mail_path);
$found = 0;
for ($i=0; $i < count($read); $i++) {
	$udata = explode("<><>", trim($read[$i]));

	if ($udata[0] == $uid && $found == 0) {
		$tmp   = explode("<><>", $read[$i]);
		$email = $tmp[1];
		$found = 1;
	}
}

if ($found == 0){
        echo "NG";
        exit;
}

$email = trim($email);

	$email = filter_var($email, FILTER_SANITIZE_EMAIL);
	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		exit;	
	}

	$fdir   = substr($email, 0, 1);
	$sdir   = substr($email, 1, 1);
	$data   = @file(DB_PATH."$fdir/$sdir/$email");	
	$ico    = trim($data[5]);
	$pts    = trim($data[3]);
	$info   = trim(urldecode($data[6]));
	if (strpos($ico, "-PREM")) {
          $ico = explode("-PREM", $ico);
          $icoimg = "<img src=\"".$livesitePath2."/premium/".$ico[0]."\" class='pic'/>";
        } else {
          $icoimg = "<img src=\"".$livesitePath2."/ico/".$ico."\" class='pic'/>";
        }
	#$icoimg = "<img src=\"".$livesitePath2."/ico/".$ico."\" height=\"\" class='pic'/>";
	$triper = '';
	$tmp_triper = trim($data[7]);

  if(isset($data[7]) && !empty($data[7]) && !empty($tmp_triper))
	$triper = "<strong>トリップ:</strong> ◆{$data[7]}<br>";
  else
	$triper = "";
	//$triper = "<strong>トリップ:</strong> {$data[7]}<br>";
	
include "../include/header.php";


$emoticon = array(
	'amazed'   => '::amazed::',
	'cry'      => '::cry::',
	'okay'     => '::okay::',
	'treasure' => '::treasure::',
	'angry'    => '::angry::',
	'gangster' => '::gangster::', 
	'shame'    => '::shame::',
	'wink'     => '::wink::',
	'annoyed'  => '::annoyed::',
	'laugh'    => '::laugh::',
	'sick'     => '::sick::',
	'blush'    => '::blush::',
	'sing'     => '::sing::',
	'chocked'  => '::chocked::',
	'smile'    => '::smile::',
	'confused' => '::confused::',
	'ninja'    => '::ninja::',
	'tongue'   => '::tongue::',
	'lawyer'   => '::lawyer::',
	'guru'     => '::guru::'
 );

$ultravariable = array();
foreach ($emoticon as $key => $value) {
        $ultravariable[] = "<li class=\"lialign\"><img src='{$livesitePath}images/{$key}.gif' data-alt='{$value}'></li>";
}
$sEmote = implode("", $ultravariable);
?>

<body>
<div class="well well-sm pad3">

	<div class = "nav nav-header">
<?php
if($_SERVER['SERVER_NAME'] == "be.2ch.net") {
?>
	<a href="<?php echo $livesitePath ?>" class="pull-right"><img src="<?php echo $livesitePath ?>css/img/2ch_logo.gif"/></a>
<?php
} elseif ($_SERVER['SERVER_NAME'] == "be.bbspink.com") {
?>
	<a href="<?php echo $livesitePath ?>" class="pull-right"><img src="<?php echo $livesitePath ?>css/img/pink.png"/></a>
<?php
}
?>
		<h5 class="pull-right"> BE 2.0 </h5>
	</div><!-- nav nav-header-->

	<hr>
	<!-- navigation-->
    <?php
        if (isset($_COOKIE['DMDM']) && isset($_COOKIE['MDMD'])) {
                require '../include/nav.php';
        }
    ?>
	<!-- end navigation-->

	
	<?php 
	    if(!empty($_SESSION['alert'])){
	        echo $_SESSION['alert'];
	        $_SESSION['alert'] = '';
	        echo "<div class=\"clearfix\"></div>";
	    }
	?>

	<div class="panel panel-default pull-right">
		<div class="btn-group">
			<button type="button" class="btn btn-default" id="btnMsg" >
				<span class="glyphicon glyphicon-envelope"></span>
			</button>
		</div><!--btn-group-->
	</div><!--panel panel-default pull-right-->


	<div class="panel panel-default pull-right">
            <button type="button" class="btn btn-default" id="btnBE">
                    <span class="glyphicon glyphicon-thumbs-up"></span>
            </button>
    </div><!--panel panel-default pull-right-->

	<br><br>

	<div class="ic panel panel-default pull-left">
		<?php echo $icoimg; ?>
	</div><!--ic panel panel-default pull-left-->

	<div class="info panel panel-default txt">
		<h5><strong>受信者 ID:</strong> <span class="badge"><?php echo $data[0] ?></span></h5>
		<h5><strong>ポイント:</strong> <span class="badge"><?php echo $pts ?></span></h5>
		<h5><strong></strong><?php echo $triper ?></h5>

        <hr class="hrw">

		<h5><strong>紹介文</strong></h5>
		<h5 class="just txt">
			<?php 
$str = $info;

$str = preg_replace("/(http:\/\/\w+\.2ch\.net\/test\/read\.cgi\/(\w+\/\d+)(?:\/[-n\d]*)?)/", "<a href=\"$1\" target=\"_blank\" class=\"btn btn-success btn-xs\"><span class=\"glyphicon glyphicon-ok\"></span> $2</a>", $str);

$str = preg_replace("/(http:\/\/\w+\.bbspink\.com\/test\/read\.cgi\/(\w+\/\d+)(?:\/[-n\d]*)?)/", "<a href=\"$1\" target=\"_blank\" class=\"btn btn-danger btn-xs\"><span class=\"glyphicon glyphicon-warning-sign\"></span> $2</a>", $str);

$str = preg_replace("/https?:\/\/be\.(2ch\.net|bbspink\.com)\/(?:user\/|test\/p\.php\?i=)(\d{9})/", "<a href=\"https://be.$1/user/$2\" target=\"_blank\" class=\"btn btn-warning btn-xs\"><span class=\"glyphicon glyphicon-user\"></span> $2</a>", $str);

$str = preg_replace("/http:\/\/info\.2ch\.net\/index\.php\/([^\?&\s]+)/", "<a href=\"http://info.2ch.net/index.php/$1\" target=\"_blank\" class=\"btn btn-primary btn-xs\"><span class=\"glyphicon glyphicon-book\"></span> $1</a>", $str);

$str = preg_replace("/https?:\/\/twitter\.com\/(\w+)/", "<a href=\"https://twitter.com/$1\" target=\"_blank\" class=\"btn btn-info btn-xs\">@$1</a>", $str);

echo $str; ?> 
		</h5>

	</div><!--info panel panel-default-->

        <div class="panel3 panel panel-default" id="BEform" style="display: none;">
        	<form class="form" method="post" action="<?php echo $livesitePath ?>melontobe.php" id="sendMessage">
        		<div class="form-group">
        			<div class="input-group">
                        <span class="input-group-addon">BEポイント(要MP) :</span>
                        <input type="hidden" name="recipient" value="<?php echo $_GET['i']; ?>">
                        <input type="text" class="form-control" name="points" required>
                        <div class="clearfix"></div>
        	       </div>
        		</div>
        		<input type="reset" value="キャンセル" class="btn btn-default textpanel pull-left" id="cancel"/>
        		<input type="submit" value="贈る" name="pTransfer" class="btn btn-default textpanel pull-left" id="send"/>
                <div class="clearfix"></div>
        	</form>
        </div>


		<div class="panel3 panel panel-default" id="SendForm">
			<form class="form" method="post" action="<?php echo $livesitePath ?>message_proc.php" id="sendMessage">
				<input type="hidden" value="<?php echo $_GET['i']; ?>" name="zxcvbnm"> <!-- need enc -->
				<div class="form-group">
					<div class="input-group">
		                <span class="input-group-addon">件名 :</span>
		                <input type="text" class="form-control" name="subj" required>
		                <div class="clearfix"></div>
			       </div>
				</div>
				<div class="form-group">
					<textarea rows="4" class="form-control top" name="msg" maxlength="250" id="messageBox" required\></textarea>
					<br>
					<input type="text" id="textarea_feedback" class="form-control" readonly="readonly">
					<span>キャラクター左</span>
				</div>
				<div class="clearfix"></div>
				<div class="btn-group pull-right">
					 <input type="button" class="btn btn-default dropdown-toggle pull-left textpanel" data-toggle="dropdown" value="絵文字">
				        <ul class="dropdown-menu dropdown-menu-right" role="menu" id="emoji">
				        <?php echo $sEmote ?>
				        </ul>
					<input type="reset" value="キャンセル" class="btn btn-default textpanel pull-left" id="cancel"/>
					<input type="submit" value="送る" class="btn btn-default textpanel pull-left" id="send"/>
				</div>
		        <div class="clearfix"></div>
			</form>
		</div>
	</div>
	
</div> <!--well well-lg-->
<?php
define('__ROOT__', dirname(dirname(__FILE__))); 
require __ROOT__.'/include/footer.php'; 
?>
