<?php

$a = $_REQUEST;

$username = trim($a['email']);
$int = trim($a['points']);

if ($username == null){
  echo "No username";
  exit;
}

if (filter_var($username,FILTER_VALIDATE_EMAIL)) {
  $username = (filter_var($username, FILTER_SANITIZE_EMAIL));
} else {
  echo "Invalid email";
  exit;
}

if(!ctype_digit(ltrim((string)$int, '-'))) {
  echo("Invalid amount of points");
  exit;
}

$ip = $_SERVER['REMOTE_ADDR'];

//echo "correct";

$exec = shell_exec("php /home/auth/secure_html/setpoints.php $username $int $ip") or die("error!");
echo $exec;

?>

