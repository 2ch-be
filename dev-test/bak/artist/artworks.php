<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Artist Page</title>
	<link rel="stylesheet" href="css/bootstrap.css">	
	<link rel="stylesheet" href="css/astyle.css">
</head>
<body>
	
	<div class="container body">
		<div class="row">
			<div class="col-md-3">
				<div class="panel panel-default">
					<div class="panel-heading"><center><h4>Artist List</h4></center></div>
					<ul id="al" style="list-style:none; display:inline;">
						<li><a href="#">Artist Name</a></li>
						<li><a href="#">Artist Name</a></li>
						<li><a href="#">Artist Name</a></li>
						<li><a href="#">Artist Name</a></li>
						<li><a href="#">Artist Name</a></li>
					</ul>
				</div>
			</div>
			<div class="col-md-9">
				<div class="row">
					<div class="col-md-4">
						<div class="panel panel-default showcase">
							<a href="#" class="thumbnail"><img src="shai.jpg" class="img-responsive" alt=""></a>
							<h5 class="pull-left"><a href="#">Name of Artist</a></h5>
							<h5 class="pull-right"><a href="#">MP</a></h5>
							<div class="clearfix"></div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="panel panel-default showcase">
							<a href="#" class="thumbnail"><img src="shai.jpg" class="img-responsive" alt=""></a>
							<h5 class="pull-left"><a href="#">Name of Artist</a></h5>
							<h5 class="pull-right"><a href="#">MP</a></h5>
							<div class="clearfix"></div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="panel panel-default showcase">
							<a href="#" class="thumbnail"><img src="shai.jpg" class="img-responsive" alt=""></a>
							<h5 class="pull-left"><a href="#">Name of Artist</a></h5>
							<h5 class="pull-right"><a href="#">MP</a></h5>
							<div class="clearfix"></div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>


	<script src="js/jquery-1.9.1.js"></script>
	<script src="js/bootstrap.js"></script>
</body>
</html>

