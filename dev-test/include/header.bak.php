<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="Shift_JIS">
    <title>ステータスページ</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="<?php echo $livesitePath ?>css/bootstrap.min.css" rel="stylesheet"/>
    <link href="<?php echo $livesitePath ?>css/style1.css" rel="stylesheet"/>
    <link href="<?php echo $livesitePath ?>css/style2.css" rel="stylesheet"/>
    <link href="<?php echo $livesitePath ?>css/dropdown-enhancement.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $livesitePath ?>css/style.css"/>
    <link type="text/css" rel="stylesheet" href="<?php echo $livesitePath ?>css/onoff.css" />
    <script type="text/javascript" src="<?php echo $livesitePath ?>js/jquery-1.9.1.js"></script>
    <script type="text/javascript">
            $(document).ready(function() {

                var $t = $('.pagination');

                $('#trip_c').keyup(function(e){
                var code = (e.keyCode ? e.keyCode : e.which);
                if (code==13) {
                    e.preventDefault();
                    calctrip();
                }
                });

                $('#caltrip').click(function(){
                calctrip();
                });

                $('#btnMelon').click(function(){
                        $('#melonForm').slideToggle("fast");
                });

                $('#btnMsg').click(function(){
                        $('#SendForm').slideToggle("fast");
                });

                $('#cancel').click(function(){
                        $('#SendForm').slideUp("fast");
                });                    
                
                // $t.animate({"scrollTop": $('.pagination')[0].scrollHeight}, "slow");   

                $( ".pagination" ).scrollTop( $('.pagination')[0].scrollHeight );  


        });
            
        function calctrip(){
            var ss = $('#trip_c').val();
            ss = escape(ss);
                    $.post(
                        'trip.php',
                        {s:ss},
                        function(e){
                            $('#trip_c').val(e);
                            $('#caltrip').attr('disabled','disabled');
                            setTimeout(function(){
                            $('#caltrip').removeAttr('disabled');
                            },1000);
                        }
                    );
        }
    </script>

  </head>
