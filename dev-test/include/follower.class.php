<?php
Class Follower{
	public 	$array;
	public $filename;
	private $Message;

	public function __construct($message){
	 	$this->Message = new Message;
	}

	public function setFollowLines($file){
		$handle = fopen($file, "r");
	 	while (($line = fgets($handle)) !== false) {
			$line = trim($line);
			$this->array[$line] = $line;
		}
	}

	public function getFollowLines($value){
		if(isset($value)){
			return $this->array[$value];
		}else{
			return $this->array;
		}
	}

	public function addLines($value, $firsttime = FALSE){
		if($firsttime){
			file_put_contents($this->filename, $value.PHP_EOL);
		}else{
			file_put_contents($this->filename, $value.PHP_EOL,FILE_APPEND);
		}
	}

	public function removeID($array, $value){
		unset($array[$value]);
		$array = implode(PHP_EOL, $array);
		$array = (empty($array)) ? trim($array) : $array.PHP_EOL ;
		// $array .= PHP_EOL;
		file_put_contents($this->filename, $array);
		return $array;
	}

	public function checkFollow($userID){
		if($userID=="NG"){
			return "NG";
		}else{
				$npath = $this->Message->GetPath(FDB_PATH, "FDB");
				
				if(!$npath){
					return "NG";
				}else{
					$this->filename = $npath."/follow.txt";

						if(file_exists($this->filename)){
							static::setFollowLines($this->filename);
							$value = static::getFollowLines($userID);
							return ($value!=NULL) ? TRUE : FALSE;
						}else{
							//create a file please.
							//then return false.
							$fh = fopen($this->filename, 'w');
						}
					
				}
		}
		
	}

}

?>