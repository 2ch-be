$(document).ready(function(){
		var load     = 0;
		var text_max = 250;
		$('#sendMessage').validate({
	    rules: {
	      subj: {
	        required: true
	      },
	      msg: {
	        required: true,
	      }
	    },
			highlight: function(element) {
				$(element)
				.closest('.form-group').addClass('has-error').removeClass('has-success');
			},
			success: function(element) {
				element
				.closest('.form-group').removeClass('has-error').addClass('has-success');
			}
	  });

	  // $('#sendMessage').validate();
	
	  $('#page').scroll(function(){
	      var r = $('#page')[0].scrollHeight- $('#page').height();
	      //alert($('#page').scrollTop() + " and " + r);

	      if($('#page').scrollTop() == r){

	      load++;
	      //alert(load + "scroll detected" + "scrollHeight and page height: " + r);
	      //$.post("try.php", {load:load}, function( data ))
	      //$('#page').append( data );

	      $.post( "try.php", {load:load}, function( data ) {
	  		    $( "#list" ).append( data );
	      });
	    }
	  });
	  $('#textarea_feedback').val(text_max);

	  $('#messageBox').keyup(function() {
	      var text_length = $('#messageBox').val().length;
	      var text_remaining = text_max - text_length;

	      $('#textarea_feedback').val(text_remaining);
	  });
});