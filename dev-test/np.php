<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="Shift_JIS">
    <title>新しいパスワード</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="stylesheet" type="text/css" href="../css/style.css"/>
  </head>
  <body>
<?php
include('encrypt.php');
if (isset($_GET['vhj']) && isset($_GET['i']) && !empty($_GET['vhj']) && !empty($_GET['i'])) {

	$ide = Decrypt($_GET['i'],KEY);
	$em = Decrypt($_GET['vhj'],KEY);

		$ide = trim($ide);
		$em = trim($em);
    $em = filter_var($em, FILTER_SANITIZE_STRING);
	  $email = explode("|",$em);
	  
	  if(isset($email[1]) && $email[1]==$ide)
	    $email = $email[0];
	  else
	    $email = "";

	  $email = filter_var($email, FILTER_SANITIZE_EMAIL);
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      echo "無効なメール";
      exit;
    }

	  $fdir = substr($email, 0, 1);
		$sdir = substr($email, 1, 1);
		$data = @file(DB_PATH."$fdir/$sdir/$email");

		if(count($data)>3) {
			if (trim($data[count($data)-1]) !=FORGOT) {
				echo "期限切れのリンク";
				exit;
			}
		} else {
			echo "Error";
			exit;
		}

} elseif (isset($_POST['newpass'])) {
	
	$pass = trim($_POST['pass']);
	$pass = filter_var($pass, FILTER_SANITIZE_STRING);
	$pass = strip_tags(addslashes($pass));
	$cpass = trim($_POST['cpass']);
	$cpass = filter_var($cpass, FILTER_SANITIZE_STRING);
	$cpass = strip_tags(addslashes($cpass));

	$email = trim($_POST['email']);
	$email = filter_var($email, FILTER_SANITIZE_EMAIL);
	if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
	echo "bad"; exit;}

	if ($pass != $cpass) {
		echo "新しいパスワードが一致しません。";
		exit;
	}else{
		if(!preg_match("/[a-z0-9 \!\"\#\$\%\&\'\(\)\*\+\,\-\.\/\:\;\<\=\>\?\@\[\]\^\_\{\}\|\~]{4,32}+/i",$pass))
			die("無効なパスワードです。");
	}
	
	$fdir = substr($email, 0, 1);
	$sdir = substr($email, 1, 1);
	$data = @file(DB_PATH."$fdir/$sdir/$email");
	for ($i=0; $i < count($data); $i++) {
		$data[$i] = trim($data[$i]);
	}
			if (trim($data[count($data)-1]) !=FORGOT) {
				echo "期限切れのリンク";
				exit;
			}

	$pass = hash("sha256", $pass."kahitanupo");
	$data[2]=$pass;
	unset($data[count($data)-1]);
	$writeData = implode("\n",$data);
	//$writeData = $data[0]."\n".$data[1]."\n".$pass."\n".$data[3]."\n".$data[4]."\n".$data[5]."\n";
	file_put_contents(DB_PATH."$fdir/$sdir/$email", $writeData);
	echo "パスワードを再設定しました。";
	exit;
} else {
	echo "ここで見るものは何もない";
	exit;
}
?>
  <div class="containerz">
    <div class="navbar-collapse">
      <ul class="nav navbar-nav">
      </ul>          
    </div>
  </div>

  <div class="container">
    <center>
      <div class="form-signin" style = "font-size: 1.2em;text-align:right;">
	<a href="http://www.2ch.net/">
		<img src="http://www.2ch.net/images/2ch_logo.gif">
	</a><br>
        BE 2.0 
      </div>
    </center>
  </div>

  <div class="container">
    <center>
    <form class="form-signin" method="post" action="np.php">
      <h3 class="form-signin-heading">新しいパスワード</h3>
      <input type="hidden" name="email" value="<?php echo $email; ?>">
      <input type="password" name="pass" placeholder="新しいパスワード" required autofocus>
      <input type="password" name="cpass" placeholder="新しいパスワードを再入力" required >
      <br><br>
      <button name="newpass" type="submit">提出する</button><br><br><br>
    </form>
  </center>
  </div>
  </body>
</html>
