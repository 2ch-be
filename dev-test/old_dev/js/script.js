$(document).ready(function(){

		$('#sendMessage').validate({
	    rules: {
	      subj: {
	        required: true
	      },
	      msg: {
	        required: true,
	      }
	    },
			highlight: function(element) {
				$(element)
				.closest('.form-group').addClass('has-error').removeClass('has-success');
			},
			success: function(element) {
				element
				.closest('.form-group').removeClass('has-error').addClass('has-success');
			}
	  });

	  // $('#sendMessage').validate();
	  var load = 0;
	  $('#page').scroll(function(){
	      var r = $('#page')[0].scrollHeight- $('#page').height();
	      //alert($('#page').scrollTop() + " and " + r);

	      if($('#page').scrollTop() == r){

	      load++;
	      //alert(load + "scroll detected" + "scrollHeight and page height: " + r);
	      //$.post("try.php", {load:load}, function( data ))
	      //$('#page').append( data );

	      $.post( "try.php", {load:load}, function( data ) {
	  		    $( "#list" ).append( data );
	      });
	    }
	  });

});