<?php
session_start();
$sub = strip_tags(addslashes(trim($_POST['zxcvbnm'])));
$msg = strip_tags(addslashes(trim($_POST['msg'])));

if (strlen($msg) > 250) {
	$echo = "Content too long";
	exit;
}

$from = substr($sub, 0, 9);
$to = substr($sub, 9, 9);
$file = substr($sub, 18);
//echo $from."-".$to."-".$file;
//exit;

include('../encrypt.php');

$reply          = new Message;
$msg            = $reply->BeSanitize($msg);
$reply_receiver = $reply->FindUser($to,$id_mail_path);

$fdir           = substr($reply_receiver, 0, 1);
$sdir           = substr($reply_receiver, 1, 1);
$udir           = str_replace("@", "-", $reply_receiver);
$file_path      = MDB_PATH."{$fdir}/{$sdir}/{$udir}/{$file}.dat";
$ufile_path     = MDB_PATH."{$fdir}/{$sdir}/{$udir}/u_{$file}.dat";
$tmp_copy_path  = MDB_PATH."{$fdir}/{$sdir}/{$udir}";

//echo $file_path;
//exit;

$sender_path = $reply->FindUser($from,$id_mail_path);
$fdir2 = substr($sender_path, 0, 1);
$sdir2 = substr($sender_path, 1, 1);
$udir2 = str_replace("@", "-", $sender_path);
$sender_file_path = MDB_PATH."{$fdir2}/{$sdir2}/{$udir2}/{$file}.dat";
$usender_file_path = MDB_PATH."{$fdir2}/{$sdir2}/{$udir2}/u_{$file}.dat";
$time = time();
$writeData = "<id>{$time}</id><from>{$from}</from><subj></subj><msg>{$msg}</msg>\n";

echo $file_path."<br>";
echo $sender_file_path;
if (!file_exists($file_path)) {
	if (file_exists($sender_file_path)) {
		if (!file_exists($tmp_copy_path)) {
			@mkdir(MDB_PATH."{$fdir}");
			@mkdir(MDB_PATH."{$fdir}/{$sdir}");
			@mkdir(MDB_PATH."{$fdir}/{$sdir}/{$udir}");
		}
		$aa = file($sender_file_path);
		$aa = implode("\n", $aa);
		file_put_contents($file_path, $aa);
		$handle = fopen($sender_file_path, "a+");
		if(!fwrite($handle, $writeData)) {
			echo "Error 1!";
			exit;
		}
		fclose($handle);
	} else {
		echo "Error 2!";
	}
	if (trim($sender_file_path) <> trim($file_path)) {
		$handle = fopen($file_path, "a+");
		//var_dump($file_path);
		//exit;
		if(!fwrite($handle, $writeData)) {
			echo "Error 3!";
			exit;
		}
		fclose($handle);
	}
	// echo "Success!";
	rename($file_path, $ufile_path);
	if (trim($sender_file_path) <> trim($file_path)) {
		rename($sender_file_path, $usender_file_path);
	}
	$_SESSION['msg'] = "Message succesfully sent";
	header('Location: ../inbox.php');

} else {
	//if (trim($sender_file_path) <> trim($file_path)) {
		$handle = fopen($sender_file_path, "a+");
		if(!fwrite($handle, $writeData)) {
			echo "Error 4";
			exit;
		}
		fclose($handle);
	//}
	if (trim($sender_file_path) <> trim($file_path)) {
		$handle = fopen($file_path, "a+");
		if(!fwrite($handle, $writeData)) {
			echo "Error 5";
			exit;
		}
		fclose($handle);
	}
	rename($file_path, $ufile_path);
	if (trim($sender_file_path) <> trim($file_path)) {
		rename($sender_file_path, $usender_file_path);
	}
	$_SESSION['msg'] = "Message succesfully sent";
	header('Location: ../inbox.php');	
}

?>