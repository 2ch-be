<?php

if (!isset($_COOKIE['DMDM']) && !isset($_COOKIE['MDMD'])) {
        header('location:index.php');
        exit;
}
include('../encrypt.php');

$id   = strip_tags(addslashes(trim($_POST['zxcvbnm'])));
$subj = strip_tags(addslashes(trim($_POST['subj'])));
$inf  = strip_tags(addslashes(trim($_POST['msg'])));

$message = new Message;
$sanitize  = $message->BeSanitize($inf);
if ($sanitize<>false) {
	$user   = $message->FindUser($id,$id_mail_path);
	$sender = $message->GetID($_COOKIE['DMDM'],$id_mail_path);
	$write  = $message->WriteMDB($user,$sanitize,$sender,$subj,$id);

	if ($write) {
		echo "Message Sent";
	} else {
		echo "Error, please try again later";
	}
} else {
	echo "Content too long!";
}

?>
