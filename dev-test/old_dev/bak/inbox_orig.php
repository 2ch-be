<?php
include('encrypt.php');
require 'include/inbox.class.php';
require 'include/header.php';

if (!isset($_COOKIE['DMDM']) && !isset($_COOKIE['MDMD'])) {
	header('location:index.php');
	exit;
}

/*********************************************
	function GetImgTrip($mail) {
		$mail = trim($mail);
		$fdir = substr($mail, 0, 1);
		$sdir = substr($mail, 1, 1);
		$impo = file_get_contents(DB_PATH."$fdir/$sdir/$mail");
		return $impo;		
	}
1402473560
//*********************************************/
// $init = new Message;
$init  = new Message;
$uid   = $init->GetID($_COOKIE['DMDM'],$id_mail_path);
// echo 
if(isset($_POST['delete'])){
	if(!file_exists($_SESSION['sLoggedDir'].'del')){
		chdir($_SESSION['sLoggedDir']);
		mkdir('del');
	}

	if(is_dir($_SESSION['sLoggedDir'].'del')){
		$delete = $_POST['delete'].".dat";
		if(file_exists($_SESSION['sLoggedDir'].$delete)){
			rename($_SESSION['sLoggedDir'].$delete, $_SESSION['sLoggedDir'].'del/'.$delete);
			$_SESSION['msg'] = "<div class=\"alert alert-danger\">Message has been successfully deleted.</div>";
			header('Location: '.$livesitePath.'inbox.php');
			exit;
		}else{
			$_SESSION['msg'] = "<div class=\"alert alert-danger\">Doesn't Exists</div>";
			header('Location: '.$livesitePath.'inbox.php');
			exit;
		}
	}
}
?>

<body>
	<div class="well well-sm pad3">
		<!-- navigation-->
		 <?php require 'include/nav.php' ?>


		<!-- <div class="pad panel panel-default sp fxmrgn b "> -->
			<!--Showing messages -->
			
			<?php
			$inbox = new inboxRead();
			$files = $inbox->getInbox();
			?>
                        <?php 
                                       
                                                echo "<div class=\"alert alert-danger\" id=\"delmsg\" style=\"display:none;\">
                                                        Are you sure you want to delete the conversation?
                                                        <input type=\"submit\" class=\"btn btn-default btn-sm\" id=\"deleteM\" value=\"Delete\"name=\"block\">
                                                        <input type=\"button\" class=\"btn btn-default btn-sm\" value=\"Cancel\" name=\"cancel\" onclick=\"document.getElementById('delmsg').style.display='none';\">
                                                </div>";
                                    
                                ?>
			<!-- List of messages -->
			<div id="page" class="pagination2" >
				
				<ul class="list-group" id="list">
					<?php
					if(!empty($_SESSION['msg'])){
						echo $_SESSION['msg'];
						$_SESSION['msg'] = '';
					}
					if($files){
							foreach($files as $filename){
								$value = $init->getContent($filename);
								$from =  $init->within_str($value[0], "<from>", "</from>");
								$to   =	$init->within_str($value[0], "<to>", "</to>");
								$id   =  $init->within_str(max($value), "<id>", "</id>");
								$subj =  $init->within_str($value[0], "<subj>", "</subj>");
								$read =  $init->within_str($value[0], "<read>", "</read>");
								$msg  =  $init->within_str($value[0], "<msg>", "</msg>");

								$array[$filename] = array("id" => $id, "subj" =>$subj, "read"=>$read, "msg"=>$msg ,"from" => $from, "to" => $to, );
							}
                                                        // echo "<pre>";
                                                        // print_r($array);
                                                        // echo "</pre>";
							// sort associative array
							$array = $init->array_sort($array, 'id', SORT_DESC);
                                                        // echo "<pre>";
                                                        // print_r($array);
                                                        // echo "</pre>";

							foreach ($array as $key => $value) {
								if (trim(urldecode($value["from"])) == trim($uid)) {
									$ucode = urldecode($value["to"]);
								} else {
									$ucode = urldecode($value["from"]);
								}
								$ucode = $init->FindUser($ucode,$id_mail_path);
								$ucode = trim($init->GetImgTrip($ucode));
								$ucode = explode("\n", $ucode);
								$uimg  = trim($ucode[5]);
								if (isset($ucode[7]) && ($ucode[7] != NULL || $ucode[7] != "")) {
									$utrip = $ucode[7];
								} else {
									$utrip = $ucode[0];
								}
								$url = substr($key, 0,-4);
								$ff = urldecode($value["subj"]);
								
								if($init->countMessage($key) > 0 ){
									$unreadMessage = "<span class='badge pull-right'>{$init->countMessage($key)}</span>";
									$class="unread";
								}else{
									$unreadMessage = '';
									$class="read";
								} 
								// $unreadCounter = ($init->within_str(max($value), "<read>", "</read>")==1) ? 'read' : 'unread'  ;

								echo "<li class='list-group-item ".$class."' style=\"overflow:auto !important;\">
									 		<div class='media panel6'>

                                                                                        
                              <div class=\"dropdown pull-right\">
                                <a data-toggle=\"dropdown\" href=\"#\"><span class=\"caret\"></span></a>

                                <ul class=\"dropdown-menu dropdown-menu-right cuzdropdown-menu\" role=\"menu\" aria-labelledby=\"dLabel\">
                                      <li>
                                	<form method=\"POST\" class='deleteForm'>
                           	     	<input type='hidden' name='delete' value='{$url}'> 
                         	   	   <small><a href='#' class='destroy'>Delete</a></small>
											</form>
                                      </li> 

                                </ul><div class=\"clearfix\"></div>
                              </div>
                                
                                                                                        

                                                                                        
                                                                                        

                                                                                

									 			<a class='pull-left' href='#'>
									 				<div class='ic2'>
														<img class='pic2' src=\"".$livesitePath2."/ico/{$uimg}\" >
									 				</div><!--ic2-->
									 			</a>
									 			<div class='media-body'>
									 				<div class='panel6'>
									 					<a href='message.php?d={$url}' class='pull-left'>
									 						<h5 class='media-heading'>{$utrip}</h5>
									 						<div class='limit'>{$ff}</div>
									 					</a>"
									 					.$unreadMessage.
									 				"</div><!--panel6-->
									 			</div><!--media-body-->
									 		</div> <!--media-->
									 	</li>";		


							}
					}
					?>
				
				</ul>
			</div><!--hide/"" pagination-->
		</div><!--pad panel panel-default-->
	</div><!--well well-lg-->

<?php 
require 'include/footer.php';
?>
