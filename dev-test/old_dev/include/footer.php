<!--jquery stuff-->
<script src="<?php echo $livesitePath ?>js/jquery.min.js"></script>
<script src="<?php echo $livesitePath ?>js/bootstrap.min.js"></script>
<script src="<?php echo $livesitePath ?>js/jquery.validate.min.js"></script>
<script src="<?php echo $livesitePath ?>js/script.js"></script>
<script src="<?php echo $livesitePath ?>js/effect.js"></script>
<script src="<?php echo $livesitePath ?>js/dropdown-enhancement.js"></script>

<script>
    $(document).ready(function(){
      $('input[type="checkbox"]').change(function(){
	var bool=0;
          if($(this).is(":checked")){
               bool = 1;
          }else{
            bool = 0;
        }
            $.ajax({
                   type: "POST",
                   url: "<?php echo $livesitePath ?>noti.php",
                   data: { toggle: bool }
                 })
                   .done(function( msg ) {
                      console.log(msg);
                   });
      });

    $('#btnFollow').click(function(){
          var userID = $('input[name=zxcvbnm]').val();
          $("#btnFollow").toggleClass('btn-default btn-success');
          $.ajax({
              type: "POST",
              url: "<?php echo $livesitePath ?>follow.php",
              data: { userID : userID }
          })
            .done(function( msg ) {
              if( msg == 'success'){
                  $('.user-actions').removeClass('not-following');
                  $('.user-actions').addClass('is-following');
              }else{
                  $('.user-actions').removeClass('is-following');
                  $('.user-actions').addClass('not-following');
              }
            });
      });
    });
</script>

<script>
$(document).ready(function(){
  var parentLink;

  $('#emoji li img').click(function(){
        var emoji = $(this).data("alt");

        $('textarea[name="msg"]').val(function(_,val){
                return val + emoji;
        });
  });
  
  $('.destroy').click(function(){
      parentLink = $(this);
      $("#delmsg").show();
  });

  $('#deleteM').click(function(){
    $(parentLink).closest('.deleteForm').submit();
  });
});
</script>
<?php 
  // echo $messages[1];
if(basename($_SERVER['PHP_SELF'])=='message.php'){
  if($messages[1]=="FALSE"){
      if(isset($_GET['d'])){ ?>
          <script type="text/javascript">
      $(document).ready(function(){      
        var content = false;
        var scrollT = false;
        var load    = 0;
        $('#pageMessage2, #pageMessage').scroll(function(){

            if($('#pageMessage2, #pageMessage').scrollTop() == 0 && !content && !scrollT){
              scrollT = true;
              if(load==0){
                  load = load + 4;
              }else{
                  load  =  load + load;
              }

                  var firstMsg = $('.panel').first();
                  var message  = $.ajax({
                      url:      "<?php echo $livesitePath ?>loadM.php",
                      method:   "POST",
                      datatype: "json",
                      data: { load : load, data : <?php echo $_GET['d'] ?>}
                    });

                  message.done(function(data) {
                  scrollT = false; 
                  $('#pageMessage2, #pageMessage').prepend(data.array);
                  $('#pageMessage2, #pageMessage').scrollTop(firstMsg.offset().top);
                      if(data.breaker || data.start==0){
                        $('#pageMessage2, #pageMessage').prepend("<div id=\"status\" class=\"col-md-12 alert alert-warning\" style=\"text-align:center;\"><small>End of Conversation</small></div>");
                        content = true;
                      }
                });
            }
          }); 
        });
      $(document).ajaxSend(function() {
        $('#pageMessage2, #pageMessage').prepend("<div id=\"load\" class=\"col-md-12 alert alert-info loadIng\" style=\"text-align:center;\"><small>Loading..</small></div>");
      }).ajaxComplete(function() {
        $('.loadIng').hide();
      });
      </script>
  <?php 
    }  
  }
}
?>
</body>
</html>
