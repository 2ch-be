<?php
$count = new Message;
$Oid   = $count->GetID($_COOKIE['DMDM'],$id_mail_path);
$path  = $count->GetPath();
$path  = $path."/noti.txt";
if(file_exists($path)) {
  $noti = file_get_contents($path);
} else {
  file_put_contents($path, "1");
  $noti = "1";
}

if($noti == 1)
  $toggle = "<input type=\"checkbox\" name=\"switch\" class=\"onoffswitch-checkbox\" id=\"myonoffswitch\" checked>";
else
  $toggle = "<input type=\"checkbox\" name=\"switch\" class=\"onoffswitch-checkbox\" id=\"myonoffswitch\">";

function notification($directory, $inbox) {
	$i = 0;
	@chdir($directory);
	try{
		$files = glob("*.dat");
		if(!$files){
			throw new Exception("");
		}else{
			foreach($files as $filename){
				$array = file($filename);
				$value = max($array);
				$checkUnread = $inbox->within_str($value, "<read>", "</read>");
				if($checkUnread==0){
					$i++;
				}
			}
			return ($i==0) ? '' : $i;
		}
	}catch(Exception $e){
		return $e->getMessage();
	} 
}
$value  = notification($_SESSION['sLoggedDir'], $count);

?>
<nav class="navbar navbar-default" role="navigation">
	<div class="container-fluid space">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle margrf" data-toggle="collapse" data-target="#menu">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
	<div class="collapse navbar-collapse space" id="menu">
		<ul class="nav navbar-nav">
			<li><a href="<?php echo $livesitePath ?>status.php" class="fixpadd">ステータス</a></li>
			<li><a href="<?php echo $livesitePath ?>test/p.php?i=<?php echo $Oid; ?>" class="fixpadd">公開プロフィール</a></li>
			<li><a href="<?php echo $livesitePath ?>inbox.php" class="fixpadd">受信トレイ <span class="badge"><?=$value; ?></span></a></li>
			<?php
			    if(detect_device($_SERVER['HTTP_USER_AGENT'])){  
			        echo "
					<li><a href=\"#\" onclick=\"location.reload()\" class=\"fixpadd\">メールをチェック</a></li>
			        ";
			    }
			?>
			<li>
				<a href="#" class="fixpadd">
				     <div class="btn-group">
				 			 <button data-toggle="dropdown" class="btn btn-default dropdown-toggle"><span class="caret"></span></button>
					    <ul class="dropdown-menu noclose dropdownfix pos_absolute">
					      	<!-- Other items -->
						      <li class="inline">
								<span class="pull-left">電子メール通知:</span>
								&nbsp; &nbsp;
								<div class="onoffswitch pull-left" >
							   		 <?php echo $toggle; ?>
								    <label class="onoffswitch-label" for="myonoffswitch">
								        <span class="onoffswitch-inner"></span>
								        <span class="onoffswitch-switch buttonfix"></span>
								    </label>
								</div>

							</li>
					    </ul>
					</div>
				</a>      
			</li>
		</ul>
	</div><!-- /.navbar-collapse -->

	</div><!-- /.container-fluid -->

</nav>
