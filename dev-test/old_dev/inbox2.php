<?php
include('encrypt.php');
require 'include/inbox.class.php';
require 'include/header.php';

if (!isset($_COOKIE['DMDM']) && !isset($_COOKIE['MDMD'])) {
	header('location:index.php');
	exit;
}

/*********************************************
	function GetImgTrip($mail) {
		$mail = trim($mail);
		$fdir = substr($mail, 0, 1);
		$sdir = substr($mail, 1, 1);
		$impo = file_get_contents(DB_PATH."$fdir/$sdir/$mail");
		return $impo;		
	}
1402473560
//*********************************************/
// $init = new Message;
$init  = new Message;
$uid   = $init->GetID($_COOKIE['DMDM'],$id_mail_path);
// echo 
if(isset($_POST['delete'])){
	if(!file_exists($_SESSION['sLoggedDir'].'del')){
		chdir($_SESSION['sLoggedDir']);
		mkdir('del');
	}

	if(is_dir($_SESSION['sLoggedDir'].'del')){
		$delete = $_POST['delete'].".dat";
		if(file_exists($_SESSION['sLoggedDir'].$delete)){
			rename($_SESSION['sLoggedDir'].$delete, $_SESSION['sLoggedDir'].'del/'.$delete);
			$_SESSION['msg'] = "<div class=¥"alert alert-success¥">メッセージが正常に削除されました。</div>";
			header('Location: '.$livesitePath.'inbox.php');
			exit;
		}else{
			$_SESSION['msg'] = "<div class=¥"alert alert-danger¥">Doesn't Exists</div>";
			header('Location: '.$livesitePath.'inbox.php');
			exit;
		}
	}
}

if(isset($_POST['msgSubmit'])){

	$id   = strip_tags(addslashes(trim($_POST['zxcvbnm'])));
	$id   = (filter_var($id, FILTER_VALIDATE_INT)) ? $id : FALSE;
	$subj = trim($_POST['subj']);
	$inf  = trim($_POST['msg']);

	$message  = new Message;
	if($id && $id > 0){
	$subj     = $message->BeSanitize($subj);
	$sanitize = $message->BeSanitize($inf);

	if ($sanitize<>false) {
		$user   = $message->FindUser($id,$id_mail_path);
		$sender = $message->GetID($_COOKIE['DMDM'],$id_mail_path);
		if($message->CheckBanMessaging()) {
			echo "メッセージを送信することはできません、低すぎるの点BE";
			exit;
		}
		$ckp    = $message->CheckPostLimit($sender,$plimit_path,TRUE);
		if (($ckp >= 60) && ($ckp != false)) {
	        	echo "到達した後の制限は、次の分お待ちください";
	        	exit;
		}
		$ckp    = $message->CheckPostLimit($sender,$plimit_hpath,FALSE);
	        if (($ckp >= 600) && ($ckp != false)) {
	                echo "到達した後の制限は、次の1時間お待ちください";
	                exit;
	        }
		$write  = $message->WriteMDB($user,$sanitize,$sender,$subj,$id);

		if ($write) {
			$message->AddPostLimit($sender,$plimit_path,TRUE);
			$message->AddPostLimit($sender,$plimit_hpath,FALSE);
	                $_SESSION['msg'] = "<div class=¥"alert alert-success¥">正常に送信されたメッセー</div>";
		} else {
	                $_SESSION['msg'] = "<div class=¥"alert alert-danger¥">メッセージは送信されませんでした</div>";
	
		}
	} else {
		echo "長すぎるコンテンツ";
	}
	}else{
	    $_SESSION['msg'] = "<div class=¥"alert alert-danger¥">メッセージは送信されませんでした</div>";
	}
}

        $emoticon = array(
                                   'amazed' => '::amazed::',
                                  'cry' => '::cry::',
                                  'okay' => '::okay::',
                                  'treasure' => '::treasure::',
                                  'angry' => '::angry::',
                                  'gangster' => '::gangster::', 
                                 'shame' => '::shame::',
                                 'wink' => '::wink::',
                                  'annoyed' => '::annoyed::',
                                  'laugh' => '::laugh::',
                                  'sick' => '::sick::',
                                  'blush' => '::blush::',
                                  'sing' => '::sing::',
                                  'chocked' => '::chocked::',
                                  'smile' => '::smile::',
                                  'confused' => '::confused::',
                                  'ninja' => '::ninja::',
                                  'tongue' => '::tongue::',
                                  'lawyer' => '::lawyer::',
                                  'guru' => '::guru::',
  
                                );
                        $ultravariable = array();
                        foreach ($emoticon as $key => $value) {
                                $ultravariable[] = "<li class=¥"lialign¥"><img src='{$livesitePath}images/{$key}.gif' data-alt='{$value}'></li>";
                        }
                        $sEmote = implode("", $ultravariable);
?>

<body>

	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
			  <div class="modal-header">
			    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			    <h4 class="modal-title" id="myModalLabel">メッセージを送る</h4>
			  </div>
		    	<form class="form" method="post" id="sendMessage">
			 			<div class="modal-body">
								<div class="form-group">
									<div class="input-group">
							                <span class="input-group-addon">受信者 :</span>
							                <input type="text" class="form-control" name="zxcvbnm" required>
							                <div class="clearfix"></div>
							      	</div>
								</div>
						   		<div class="form-group">
						   			<div class="input-group">
						   	                <span class="input-group-addon">件名 :</span>
						   	                <input type="text" class="form-control" name="subj" required>
						   	                <div class="clearfix"></div>
						   	      	</div>
						   		</div>
						   		<!--input type="hidden" value="" name="rec"/> <!--receiver -->
						   		<div class="form-group">
						   			<textarea rows="4" class="form-control top" id='messageBox' name="msg" maxlength="250" id="msg"></textarea>
						   		</div>
						       <div class="clearfix"></div>
				 		</div>
						<div class="modal-footer">
							 <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">絵文字</button>
                                                <ul class="dropdown-menu dropdown-menu-right" role="menu" id="emoji">
                                                <?php echo $sEmote ?>
                                                </ul>
							<button type="submit" class="btn btn-primary" name="msgSubmit">送る</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="well well-sm pad3">
		<!-- navigation-->
		 <?php require 'include/nav.php' ?>


		<!-- <div class="pad panel panel-default sp fxmrgn b "> -->
			<!--Showing messages -->
			
			<?php
			$inbox = new inboxRead();
			$files = $inbox->getInbox();
                               

           	echo "	<!-- List of messages -->
				<div class=¥"btn-group¥">
					<button type=¥"button¥" class=¥"btn btn-default¥" id=¥"btnMsg¥" data-toggle=¥"modal¥" data-target=¥"#myModal¥">
						<span class=¥"glyphicon glyphicon-envelope¥"></span>
					</button>
				</div><br>	";        
			echo "<div class=¥"alert alert-danger¥" id=¥"delmsg¥" style=¥"display:none;¥">
			あなたが会話を削除してもよろしいですか？<input type=¥"submit¥" class=¥"btn btn-default btn-sm¥" id=¥"deleteM¥" value=¥"削除¥"name=¥"block¥">
            <input type=¥"button¥" class=¥"btn btn-default btn-sm¥" value=¥"キャンセル¥" name=¥"cancel¥" onclick=¥"document.getElementById('delmsg').style.display='none';¥">
            </div>";
              


			if(!empty($_SESSION['msg'])){
				echo $_SESSION['msg'];
				$_SESSION['msg'] = '';
			}                      
            ?>
		
			<div id="page" class="pagination2" >

						
<div class="clearfix"></div>
<br/>				
<ul class="list-group" id="list">
					<?php
					if($files){
							foreach($files as $filename){
								$value = $init->getContent($filename);
								$from =  $init->within_str($value[0], "<from>", "</from>");
								$to   =	$init->within_str($value[0], "<to>", "</to>");
								$id   =  $init->within_str(max($value), "<id>", "</id>");
								$subj =  $init->within_str($value[0], "<subj>", "</subj>");
								$read =  $init->within_str($value[0], "<read>", "</read>");
								$msg  =  $init->within_str($value[0], "<msg>", "</msg>");

								$array[$filename] = array("id" => $id, "subj" =>$subj, "read"=>$read, "msg"=>$msg ,"from" => $from, "to" => $to, );
							}
                                                        // echo "<pre>";
                                                        // print_r($array);
                                                        // echo "</pre>";
							// sort associative array
							$array = $init->array_sort($array, 'id', SORT_DESC);
                                                        // echo "<pre>";
                                                        // print_r($array);
                                                        // echo "</pre>";

							foreach ($array as $key => $value) {
								if (trim(urldecode($value["from"])) == trim($uid)) {
									$ucode = urldecode($value["to"]);
								} else {
									$ucode = urldecode($value["from"]);
								}
								$ucode = $init->FindUser($ucode,$id_mail_path);
								$ucode = trim($init->GetImgTrip($ucode));
								$ucode = explode("¥n", $ucode);
								$uimg  = trim($ucode[5]);
								if (isset($ucode[7]) && ($ucode[7] != NULL || $ucode[7] != "")) {
									$utrip = $ucode[7];
								} else {
									$utrip = $ucode[0];
								}
								$url = substr($key, 0,-4);
								$ff = urldecode($value["subj"]);
								
								if($init->countMessage($key) > 0 ){
									$unreadMessage = "<span class='badge pull-right'>{$init->countMessage($key)}</span>";
									$class="unread";
								}else{
									$unreadMessage = '';
									$class="read";
								} 
								// $unreadCounter = ($init->within_str(max($value), "<read>", "</read>")==1) ? 'read' : 'unread'  ;

								echo "<li class='list-group-item ".$class."' style=¥"overflow:auto !important;¥">
									 		<div class='media panel6'>

                                                                                        
                              <div class=¥"dropdown pull-right¥">
                                <a data-toggle=¥"dropdown¥" href=¥"#¥"><span class=¥"caret¥"></span></a>

                                <ul class=¥"dropdown-menu dropdown-menu-right cuzdropdown-menu¥" role=¥"menu¥" aria-labelledby=¥"dLabel¥">
                                      <li>
                                	<form method=¥"POST¥" class='deleteForm'>
                           	     	<input type='hidden' name='delete' value='{$url}'> 
                         	   	   <small><a href='#' class='destroy'>削除</a></small>
											</form>
                                      </li> 

                                </ul><div class=¥"clearfix¥"></div>
                              </div>
                                
                                                                                        

                                                                                        
                                                                                        

                                                                                

									 			<a class='pull-left' href='#'>
									 				<div class='ic2'>
														<img class='pic2' src=¥"".$livesitePath2."/ico/{$uimg}¥" >
									 				</div><!--ic2-->
									 			</a>
									 			<div class='media-body'>
									 				<div class='panel6'>
									 					<a href='message.php?d={$url}' class='pull-left'>
									 						<h5 class='media-heading'>{$utrip}</h5>
									 						<div class='limit'>{$ff}</div>
									 					</a>"
									 					.$unreadMessage.
									 				"</div><!--panel6-->
									 			</div><!--media-body-->
									 		</div> <!--media-->
									 	</li>";		


							}
					}
					?>
				
				</ul>
			</div><!--hide/"" pagination-->
		</div><!--pad panel panel-default-->
	</div><!--well well-lg-->

<?php 
require 'include/footer.php';
?>
