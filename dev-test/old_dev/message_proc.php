<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="Shift_JIS">
  </head>
  <body>
<?php

if (!isset($_COOKIE['DMDM']) && !isset($_COOKIE['MDMD'])) {
        header('location:index.php');
        exit;
}
include('encrypt.php');

$id   = strip_tags(addslashes(trim($_POST['zxcvbnm'])));
$id   = (filter_var($id, FILTER_VALIDATE_INT)) ? $id : FALSE;
$subj = trim($_POST['subj']);
$inf  = trim($_POST['msg']);

$message  = new Message;
if($id && $id > 0){
$subj     = $message->BeSanitize($subj);
$sanitize = $message->BeSanitize($inf);

if ($sanitize<>false) {
	$user   = $message->FindUser($id,$id_mail_path);
	$sender = $message->GetID($_COOKIE['DMDM'],$id_mail_path);
	if($message->CheckBanMessaging()) {
		$_SESSION['msg'] = "<div class=\"alert alert-danger\">メッセージを送信することはできません、低すぎるの点BE</div>";
		header('Location:test/p.php?i='.$id);	
		//exit;
	}
	$ckp    = $message->CheckPostLimit($sender,$plimit_path,TRUE);
	if (($ckp >= 60) && ($ckp != false)) {
        	$_SESSION['msg'] = "<div class=\"alert alert-danger\">到達した後の制限は、次の分お待ちください</div>";
        	header('Location:test/p.php?i='.$id);
		//exit;
	}
	$ckp    = $message->CheckPostLimit($sender,$plimit_hpath,FALSE);
        if (($ckp >= 600) && ($ckp != false)) {
                $_SESSION['msg'] = "<div class=\"alert alert-danger\">到達した後の制限は、次の1時間お待ちください</div>";
                header('Location:test/p.php?i='.$id);
		//exit;
        }
	$write  = $message->WriteMDB($user,$sanitize,$sender,$subj,$id);

	if ($write) {
		$message->AddPostLimit($sender,$plimit_path,TRUE);
		$message->AddPostLimit($sender,$plimit_hpath,FALSE);
		echo "正常に送信されたメッセージ";
                $_SESSION['alert'] = "<div class=\"alert alert-success\">正常に送信されたメッセー</div>";
                header('Location:test/p.php?i='.$id);
	} else {
		echo "エラー、後でもう一度お試しください";
                $_SESSION['alert'] = "<div class=\"alert alert-danger\">メッセージは送信されませんでした</div>";
                header('Location:test/p.php?i='.$id);
	}
} else {
	echo "長すぎるコンテンツ";
}
}else{
 $_SESSION['msg'] = "<div class=\"alert alert-danger\">メッセージは送信されませんでした</div>";
  header('Location:test/p.php?i='.$id);
}

?>
  </body>
</html>
