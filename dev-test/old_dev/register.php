<?php
if (isset($_COOKIE['DMDM']) && isset($_COOKIE['MDMD'])) {
        header('location:status.php');
        exit;
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="Shift_JIS">
    <title>アカウントを登録</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
  </head>
  <body>
  <div class="containerz">
    <div class="navbar-collapse">
      <ul class="nav navbar-nav">
      </ul>          
    </div>
  </div>

  <div class="container">
    <center>
      <div class="form-signin" style = "font-size: 1.2em;text-align:right;">
<?php if($_SERVER['SERVER_NAME'] == "be.2ch.net") { ?>
        <a href="http://www.2ch.net/" target="_BLANK">
        <img src="<?php echo $livesitePath ?>css/img/2ch_logo.gif">
        </a>
<?php } elseif ($_SERVER['SERVER_NAME'] == "be.bbspink.com") { ?>
        <a href="http://www.bbspink.com/" target="_BLANK">
        <img src="<?php echo $livesitePath ?>css/img/pink.png">
        </a>
<?php } ?>
        <br>
        BE 2.0 β
      </div>
    </center>
  </div>

  <div class="container">
    <center>
    <form class="form-signin" method="post" action="reg.php">
      <h3 class="form-signin-heading">アカウントを登録</h3>
      <input type="email" name="mail" placeholder="Eメール" required autofocus>
      <input type="password" name="pass" placeholder="パスワード" required>
      <input type="password" name="confpass" placeholder="パスワードを確認" required>
      <br><br>
      <input type="submit" name="reg" value="提出する"><br><br>
      <a href="login.php">[ログイン]</a>
    </form>
  </center>
  </div>
  </body>
</html>
