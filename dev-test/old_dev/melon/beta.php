<?php

$webhookContent = "";

$webhook = fopen('php://input' , 'rb');
while (!feof($webhook)) {
      $webhookContent .= fread($webhook, 4096);
}
fclose($webhook);


$account = json_decode($webhookContent);

$username = $account -> {'email'};
$username = (filter_var($username, FILTER_SANITIZE_EMAIL));
$username = strtolower($username); // force username to be lowercase

$lineItems = $account -> {'line_items'};

$pointsToAdd = 0;

foreach ($lineItems as $lineItem){
  $quantity = $lineItem -> {'quantity'};
  $quantity = (int) $quantity; // make sure $quantity is an int
  $sku = $lineItem -> {'sku'};

  $package = 0;
  if ($sku == "MP001") {$package = 5000;} // package is the amount of months
  if ($sku == "MP002") {$package = 15000;}
  if ($sku == "MP003") {$package = 25000;}
  if ($package == 0) {exit;}

  $pointsToAdd = $pointsToAdd + ($package * $quantity);
}

$customer = $account -> {'customer'};

$animal = "lobster";
system("php /home/auth/secure_html/melon/melon.php $username $pointsToAdd $animal");

?>

