<?php
session_start();
require 'include/inbox.class.php';
include('encrypt.php');
require 'include/header.php';

if (!isset($_COOKIE['DMDM']) && !isset($_COOKIE['MDMD'])) {
	header('location:index.php');
	exit;
}

$inbox = new Message;
$uid   = $inbox->GetID($_COOKIE['DMDM'],$id_mail_path);

?>

<body>
	<div class="well well-lg">
		<!-- navigation-->
		 <?php include 'include/nav.php' ?>


		<div class="pad panel panel-default sp">
			<!--Showing messages -->
			
			<?php
			// $email = Decrypt($_COOKIE['DMDM'],KEY);
			$inbox = new inboxRead(EMAIL_LOGGED);
			$init  = new Message;

			$inbox->setDirectory(MDB_PATH);
			$_SESSION['sLoggedDir'] = $inbox->directory;
			$files                  = $inbox->getInbox();

			?>
			<!-- List of messages -->
			<div id="page" class="pagination" >
				<ul class="list-group" id="list">
					<?php
					if(!empty($_SESSION['msg'])){
						echo '<p class="bg-success">'.$_SESSION['msg'].'</p>';
						$_SESSION['msg'] = '';
					}
					if($files){
							foreach($files as $filename){
								$value = $init->getContent($filename);
								$max = count($value)-2;
								// echo $value[3];
								$fetch = $init->passContent($value);
								$dat   = substr($filename, 0,-4);

								$ff = urldecode($fetch[$max][3]);
							if(!preg_match("/u_/i", $filename)){
							echo 	"<li class='list-group-item'  data-src='{$dat}'>
										<div class='media'>
											<a class='pull-left' href='#'>
												<div class='ic2'>
													<img class='pic' src='css/img/ex2.gif'>
												</div><!--ic2-->
											</a>
											<div class='media-body'>
												<div class='panel6'>
													<a href='message.php?d={$dat}'>
														<h5 class='media-heading'>(tripcode of sender)</h5>
														{$ff}
													</a>
												</div><!--panel6-->
											</div><!--media-body-->
										</div> <!--media-->
									</li>";
								}else{
										echo "<li class='list-group-item unread'  data-src='{$dat}'>
										<div class='media'>
											<a class='pull-left' href='#'>
												<div class='ic2'>
													<img class='pic' src='css/img/ex2.gif'>
												</div><!--ic2-->
											</a>
											<div class='media-body'>
												<div class='panel6'>
													<a href='message.php?d=".substr($dat, 2)."'>
														<h5 class='media-heading'>(tripcode of sender)</h5>
														{$ff}
													</a>
												</div><!--panel6-->
											</div><!--media-body-->
										</div> <!--media-->
									</li>";
									
							}
						}
					}
					?>
				
				</ul>
			</div><!--hide/"" pagination-->
		</div><!--pad panel panel-default-->
	</div><!--well well-lg-->

<?php 
require 'include/footer.php';
?>
