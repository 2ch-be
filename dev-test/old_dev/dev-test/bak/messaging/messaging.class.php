<?php
class Message {
	function BeSanitize($string) {
	  if (strlen($string) > 250) {
	    $echo = "Content too long";
	    return false;
	  } else {
	  	$string = filter_var($string, FILTER_SANITIZE_STRING);
	    $string = str_replace("<", "&lt;", $string);
	    $string = str_replace(">", "&gt;", $string);
	    $string = str_replace("\n", "<br>", $string);
	    $string = str_replace(" ", "&nbsp;", $string);
	    $string = urlencode($string);
	    return $string;
	  }
	}

	function FindUser($id,$id_mail_path) {
		$list = file($id_mail_path);
		$em   = "";
		$err  = true;
		foreach ($list as $key => $value) {
			if (substr(trim($value), 0, 9) == $id) {
				$r = explode("<><>", $value);
				$em = trim($r[1]);
				$err = false;
			}
		}
		if ($err) {
			return false;
		}
		return $em;
	}

	function GetID($cookie,$id_mail_path) {
		$email = Decrypt($_COOKIE['DMDM'], KEY);
		$list  = file($id_mail_path);
		$em    = "";
		$err   = true;
		foreach ($list as $key => $value) {
			$mail = explode("<><>", trim($value));
			if ($mail[1] == trim($email)) {
				$em  = $mail[0];
				$err = false;
			}
		}
		if ($err) {
			return false;
		}
		return $em;
	}

	function WriteMDB($user, $inf, $sender, $subj, $to) {
		$fdir = substr($user, 0, 1);
		$sdir = substr($user, 1, 1);
		$udir = str_replace("@", "-", $user);
		$path = MDB_PATH."{$fdir}/{$sdir}/{$udir}";
		if (!file_exists($path)) {
			@mkdir(MDB_PATH."{$fdir}");
			@mkdir(MDB_PATH."{$fdir}/{$sdir}");
			@mkdir(MDB_PATH."{$fdir}/{$sdir}/{$udir}");
		}
		$time = time();
		$writeData = "<id>{$time}</id><from>{$sender}</from><to>{$to}</to><subj>{$subj}</subj><msg>{$inf}</msg>\n";
		$filename  = $path."/u_".$time.".dat";
		if (!file_exists($filename)) {
			if(!file_put_contents($filename,$writeData)) {
				echo "Error!";
				return false;
			}
			// echo $filename;
			// echo $writeData;
		} else {
			$handle = fopen($filename, "a+");
			if(!fwrite($handle, $writeData)) {
				echo "Error!";
				return false;
			}
			fclose($handle);
			// echo $writeData;
		}
		return true;
	}

	function ReadMDB($user) {
		$fdir = substr($user, 0, 1);
		$sdir = substr($user, 1, 1);
		$udir = str_replace("@", "-", $user);
		$path = MDB_PATH."{$fdir}/{$sdir}/{$udir}";
		$messages = array();
		$i = 0;
		foreach (glob($path."/*.dat") as $filename) {
    	$m = file($filename);
    	$j = 0;
    	foreach ($m as $key => $value) {
    		$messages[$i][$j] = $value;
    		$j++;
    	}
    	$i++;
		}
		return $messages;
	}

	function specificMDB($user, $dat) {
		$fdir     = substr($user, 0, 1);
		$sdir     = substr($user, 1, 1);
		$udir     = str_replace("@", "-", $user);
		$path     = MDB_PATH."{$fdir}/{$sdir}/{$udir}";
		$messages = array();
		$i        = 0;
		//$dat      = (!preg_match("/u_/i", $dat)) ? $dat : "u_".$dat;

		foreach (glob($path."/{$dat}.dat") as $filename) {
    	$m = file($filename);
    	$j = 0;
    	foreach ($m as $key => $value) {
    		$messages[$i][$j] = $value;
    		$j++;
    	}
    	$i++;
		}
		return $messages;
	}

	function within_str($subject, $lsearch, $rsearch) {
	    $data = strstr($subject, $lsearch);
	    $data = str_replace($lsearch, "", $data);
	    $trim = strstr($data, $rsearch);

   		return(str_replace($trim, "", $data));
	}

	function getContent($files){
		$i        = 0;
		$j        = 0;
		$homepage = file_get_contents($files);
		$test     = explode(PHP_EOL, $homepage);
		foreach($test as $value){
			// echo $value."<br>";
			$valueTest    = preg_replace('/\<[a-z]*>/', '', $value); 
			$trim         = preg_replace('/\<\/[a-z]*>/', '|', $valueTest); 
			$valueTest1[] = rtrim($trim, '|');
		}
			return $valueTest1;
			// print_r($valueTest1);
	}

	function passContent($array){
		$fetch = array();
		foreach ($array as $formidable) {
			if($formidable!=''){
				$fetch[] = explode('|', $formidable);
			}
		}
		// print_r($array)."<br>";
		return $fetch;

	}
	
}

?>
