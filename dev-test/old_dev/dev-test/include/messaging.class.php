<?php
class Message {
	function BeSanitize($string) {
	  if (strlen($string) > 250) {
	    $echo = "Content too long";
	    return false;
	  } else {	    
	    $string = str_replace("<", "&lt;", $string);
	    $string = str_replace(">", "&gt;", $string);
	    $string = str_replace("\n", "<br>", $string);
	    $string = str_replace(" ", "&nbsp;", $string);
	    $string = urlencode($string);
	    return $string;
	  }
	}

	function FindUser($id,$id_mail_path) {
		$list = file($id_mail_path);
		$em   = "";
		$err  = true;
		foreach ($list as $key => $value) {
			if (substr(trim($value), 0, 9) == $id) {
				$r = explode("<><>", $value);
				$em = trim($r[1]);
				$err = false;
			}
		}
		if ($err) {
			return false;
		}
		return $em;
	}

	function GetID($cookie,$id_mail_path) {
		$email = Decrypt($_COOKIE['DMDM'], KEY);
		$list  = file($id_mail_path);
		$em    = "";
		$err   = true;
		foreach ($list as $key => $value) {
			$mail = explode("<><>", trim($value));
			if ($mail[1] == trim($email)) {
				$em  = $mail[0];
				$err = false;
			}

		}
		if ($err) {
			return false;
		}
		return $em;
	}

	function WriteMDB($user, $inf, $sender, $subj, $to) {
		$fdir = substr($user, 0, 1);
		$sdir = substr($user, 1, 1);
		$udir = str_replace("@", "-", $user);
		$path = MDB_PATH."{$fdir}/{$sdir}/{$udir}";
		if (!file_exists($path)) {
			@mkdir(MDB_PATH."{$fdir}");
			@mkdir(MDB_PATH."{$fdir}/{$sdir}");
			@mkdir(MDB_PATH."{$fdir}/{$sdir}/{$udir}");
		}
		if (file_exists($path."/ban.txt")) {
			$banlist = file($path."/ban.txt");
			foreach ($banlist as $key => $value) {
				if (trim($sender) == trim($value)) {
					echo "Message sending failed, You have been blocked by the user";
					return false;
				}
			}
		}
		$time = time();
		$writeData = "<id>{$time}</id><from>{$sender}</from><to>{$to}</to><subj>{$subj}</subj><msg>{$inf}</msg><read>0</read>\n";
		$filename  = $path."/".$time.".dat";
		if (!file_exists($filename)) {
			if(!file_put_contents($filename,$writeData)) {
				echo "Error!";
				return false;
			}
			// echo $filename;
			// echo $writeData;
		} else {
			$handle = fopen($filename, "w");
			if(!fwrite($handle, $writeData)) {
				echo "Error!";
				return false;
			}
			fclose($handle);
			// echo $writeData;
		}
		return true;
	}

	function BlockUser($user,$kausap) {
		$fdir 	= substr($user, 0, 1);
		$sdir 	= substr($user, 1, 1);
		$udir 	= str_replace("@", "-", $user);
		$path 	= MDB_PATH."{$fdir}/{$sdir}/{$udir}/ban.txt";
		$banlist = file($path);
		$meron = false;
		foreach ($banlist as $key => $value) {
			if (trim($value) == trim($kausap))
				$meron = true;
		}
		if (!$meron) {
			$handle = fopen($path, "a");
			$bandata = trim($kausap)."\n";
			if(!fwrite($handle, $bandata))
				return false;
			fclose($handle);
		}
		return true;
	}

	function UnblockUser($user,$kausap) {
		$fdir 	= substr($user, 0, 1);
		$sdir 	= substr($user, 1, 1);
		$udir 	= str_replace("@", "-", $user);
		$path 	= MDB_PATH."{$fdir}/{$sdir}/{$udir}/ban.txt";
		$banlist = file($path);
		foreach ($banlist as $key => $value) {
			if (trim($value) == trim($kausap))
				unset($banlist[$key]);
		}
		$banlist = implode("\n", $banlist);
		if (empty($banlist)) {
			if(!file_put_contents($path, $banlist))
				return true;
		} else {
			if(!file_put_contents($path, $banlist))
				return false;
		}		
		return true;
	}

	function CheckBanList($user,$kausap) {
		$fdir 	= substr($user, 0, 1);
		$sdir 	= substr($user, 1, 1);
		$udir 	= str_replace("@", "-", $user);
		$path 	= MDB_PATH."{$fdir}/{$sdir}/{$udir}/ban.txt";
		$banlist = file($path);
		foreach ($banlist as $key => $value) {
			if (trim($value) == trim($kausap))
				return true;
		}
		return false;
	}

	function ReadMDB($user) {
		$fdir = substr($user, 0, 1);
		$sdir = substr($user, 1, 1);
		$udir = str_replace("@", "-", $user);
		$path = MDB_PATH."{$fdir}/{$sdir}/{$udir}";
		$messages = array();
		$i = 0;
		foreach (glob($path."/*.dat") as $filename) {
            	$m = file($filename);
            	$j = 0;
            	foreach ($m as $key => $value) {
            		$messages[$i][$j] = $value;
            		$j++;
            	}
            	$i++;
        		}
		return $messages;
	}

  function specificMDB($user, $dat, $bool = FALSE) {
          $fdir     = substr($user, 0, 1);
          $sdir     = substr($user, 1, 1);
          $udir     = str_replace("@", "-", $user);
          $path     = MDB_PATH."{$fdir}/{$sdir}/{$udir}";
          $messages = array();
          if(file_exists($path."/{$dat}.dat")){
                          $i        = 0;
                          $messages = file("{$path}/{$dat}.dat"); 

                          if($bool){
                                  return $messages[0];
                          }else{
                                  $messages = array_slice($messages, -4, 4);
                                  return $messages;
                          }
          }else{
                  return false;
          }
  }
	// function specificMDB($user, $dat) {
	// 	$fdir     = substr($user, 0, 1);
	// 	$sdir     = substr($user, 1, 1);
	// 	$udir     = str_replace("@", "-", $user);
	// 	$path     = MDB_PATH."{$fdir}/{$sdir}/{$udir}";
	// 	$messages = array();
	// 	if(file_exists($path."/{$dat}.dat")){
	// 			$i        = 0;
	// 			//$dat      = (!preg_match("/u_/i", $dat)) ? $dat : "u_".$dat;

	// 			foreach (glob($path."/{$dat}.dat") as $filename) {
	//             	$m = file($filename);
	//             	$j = 0;
	//             	foreach ($m as $key => $value) {
	//                         // print_r($messages);
	//             		$messages[$i][$j] = $value;
	//             		$j++;
	//             	}
	//             	$i++;
 //        		}
	// 			return $messages;
 //  		}else{
 //  			return false;
 //  		}
	// }

	// function renameMDB($user, $dat) {
	// 	$fdir     = substr($user, 0, 1);
	// 	$sdir     = substr($user, 1, 1);
	// 	$udir     = str_replace("@", "-", $user);
	// 	$path     = MDB_PATH."{$fdir}/{$sdir}/{$udir}";
	// 	$messages = array();
	// 	$i        = 0;
	// 	$substr = $dat.".dat";
	// 	$substr2 = substr($substr, 2);
	// 	if(preg_match("/u_/i", $substr)){
	// 		rename($_SESSION['sLoggedDir'] ."/".$substr, $_SESSION['sLoggedDir'] ."/".$substr2);
	// 	}
	// }

	function within_str($subject, $lsearch, $rsearch) {
	    $data = strstr($subject, $lsearch);
	    $data = str_replace($lsearch, "", $data);
	    $trim = strstr($data, $rsearch);

   		return(str_replace($trim, "", $data));
	}

	// function getContent($files) {
	// 	$i        = 0;
	// 	$j        = 0;
	// 	$homepage = file_get_contents($files);
	// 	$test     = explode(PHP_EOL, $homepage);
	// 	foreach($test as $value){
	// 		// echo $value."<br>";
	// 		$valueTest    = preg_replace('/\<[a-z]*>/', '', $value); 
	// 		$trim         = preg_replace('/\<\/[a-z]*>/', '|', $valueTest); 
	// 		$valueTest1[] = rtrim($trim, '|');
	// 	}
	// 		return $valueTest1;
	// 		// print_r($valueTest1);
	// }

	function getContent($files) {
		$i        = 0;
		$j        = 0;
		$homepage = file_get_contents($files);
		$test     = explode(PHP_EOL, $homepage);
		return $test;
	}

	function getContent2($files) {
		$i        = 0;
		$j        = 0;
		$homepage = file($files);
		
		return $homepage;
			// print_r($valueTest1);
	}

	function passContent($array) {
		$fetch = array();
		foreach ($array as $formidable) {
			// print_r($array);
			if($formidable!=''){
				$fetch[] = explode('|', $formidable);
			}
		}
		// print_r($array)."<br>";
		return $fetch;

	}

	function GetImgTrip($mail) {
		$mail = trim($mail);
		$fdir = substr($mail, 0, 1);
		$sdir = substr($mail, 1, 1);
		$impo = file_get_contents(DB_PATH."$fdir/$sdir/$mail");
		if ($impo != NULL && $impo != "") {
			return $impo;
		} else {
			return false;
		}		
	}

	function AddPostLimit($id,$plimit_path,$mah) {
	  if($mah) {
                $handle = fopen($plimit_path.$id.".txt", "a");
                if(!fwrite($handle,"1\n"))
                        return false;
                fclose($handle);
                return true;
	  } else {
		$handle = fopen($plimit_path.$id.".txt", "a");
                if(!fwrite($handle,"1\n"))
                        return false;
                fclose($handle);
                return true;
	  }
        }

        function CheckPostLimit($id,$plimit_path,$mah) {
	  if($mah) {
                if (file_exists($plimit_path.$id.".txt")) {
                        $post_number = file($plimit_path.$id.".txt");
                        $post_number = count($post_number);
                        return $post_number;
                }
                return false;
	  } else {
		if (file_exists($plimit_path.$id.".txt")) {
                        $post_number = file($plimit_path.$id.".txt");
                        $post_number = count($post_number);
                        return $post_number;
                }
                return false;
	  }
        }

}

?>
