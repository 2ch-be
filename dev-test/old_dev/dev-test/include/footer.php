<!--jquery stuff-->
<script src="<?php echo $livesitePath ?>js/jquery.min.js"></script>
<script src="<?php echo $livesitePath ?>js/bootstrap.min.js"></script>
<script src="<?php echo $livesitePath ?>js/jquery.validate.min.js"></script>
<script src="<?php echo $livesitePath ?>js/script.js"></script>
<script src="<?php echo $livesitePath ?>js/effect.js"></script>
<script type="text/javascript">
$(document).ready(function(){      
  var content = false;
  var load    = 0;
  $('#pageMessage2, #pageMessage').scroll(function(){
      if($('#pageMessage2, #pageMessage').scrollTop() == 0 && !content){
        if(load==0){
            load = load + 4;
        }else{
            load  =  load + load;
        }
            var firstMsg = $('.panel:first');
            var message = $.ajax({
                url:      "loadM.php",
                method:   "POST",
                datatype: "json",
                data: { load : load, data : <?php echo $_GET['d'] ?>}
              });

            message.done(function(data) {
            $('#pageMessage2, #pageMessage').prepend(data.array);
            $('#pageMessage2, #pageMessage').scrollTop(firstMsg.offset().top);
                if(data.breaker){
                  $('#pageMessage2, #pageMessage').prepend("<div id=\"status\" class=\"col-md-12 alert alert-warning\" style=\"text-align:center;\"><small>End of Conversation</small></div>");
                  content = true;
                }
          });
      }
    }); 
  });
$(document).ajaxSend(function() {
  $('#pageMessage2, #pageMessage').prepend("<div id=\"load\" class=\"col-md-12 alert alert-info\" style=\"text-align:center;\"><small>Loading..</small></div>");
}).ajaxComplete(function() {
  $('#load').hide();
});


</script>
</body>
</html>
