<?php
$count = new Message;

function notification($directory, $inbox) {
	$i = 0;
	@chdir($directory);
	try{
		$files = glob("*.dat");
		if(!$files){
			throw new Exception("");
		}else{
			foreach($files as $filename){
				$array = file($filename);
				$value = max($array);
				$checkUnread = $inbox->within_str($value, "<read>", "</read>");
				if($checkUnread==0){
					$i++;
				}
			}
			return ($i==0) ? '' : $i;
		}
	}catch(Exception $e){
		return $e->getMessage();
	} 
}
$value  = notification($_SESSION['sLoggedDir'], $count);

?>
<nav class="navbar navbar-default" role="navigation">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>

	<div class="collapse navbar-collapse" id="menu">
		<ul class="nav navbar-nav">
			<li><a href="<?php echo $livesitePath ?>status.php">ステータス</a></li>
			<li><a href="<?php echo $livesitePath ?>test/p.php?i=<?php echo $uid; ?>">公開プロフィール</a></li>
			<li><a href="<?php echo $livesitePath ?>inbox.php">受信トレイ <span class="badge"><?=$value; ?></span></a></li>
		</ul>
	</div><!-- /.navbar-collapse -->

	</div><!-- /.container-fluid -->

</nav>