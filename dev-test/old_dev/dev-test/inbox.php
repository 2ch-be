<?php
include('encrypt.php');
require 'include/inbox.class.php';
require 'include/header.php';

if (!isset($_COOKIE['DMDM']) && !isset($_COOKIE['MDMD'])) {
	header('location:index.php');
	exit;
}

/*********************************************
	function GetImgTrip($mail) {
		$mail = trim($mail);
		$fdir = substr($mail, 0, 1);
		$sdir = substr($mail, 1, 1);
		$impo = file_get_contents(DB_PATH."$fdir/$sdir/$mail");
		return $impo;		
	}

//*********************************************/
$inboxk = new Message;
$uid   = $inboxk->GetID($_COOKIE['DMDM'],$id_mail_path);
//echo "--".$uid."--";
?>

<body>
	<div class="well well-sm pad3">
		<!-- navigation-->
		 <?php require 'include/nav.php' ?>


		<div class="pad panel panel-default sp fxmrgn b">
			<!--Showing messages -->
			
			<?php
			$inbox = new inboxRead();
			$init  = new Message;
			$files = $inbox->getInbox();
			?>
			<!-- List of messages -->
			<div id="page" class="pagination2" >
				<ul class="list-group" id="list">
					<?php
					if(!empty($_SESSION['msg'])){
						echo $_SESSION['msg'];
						$_SESSION['msg'] = '';
					}
					if($files){
							foreach($files as $filename){
								$value = $init->getContent($filename);
								$array[urldecode($init->within_str(max($value), "<id>", "</id>"))] = $value;
							}
							// sort associative array
							krsort($array);
							foreach ($array as $key => $value) {
								$ucode = $value[0];
								if (trim(urldecode($init->within_str($ucode, "<from>", "</from>"))) == trim($uid)) {
									$ucode = urldecode($init->within_str($ucode, "<to>", "</to>"));
								} else {
									$ucode = urldecode($init->within_str($ucode, "<from>", "</from>"));
								}
								$ucode = $inboxk->FindUser($ucode,$id_mail_path);
								$ucode = trim($inboxk->GetImgTrip($ucode));
								$ucode = explode("\n", $ucode);
								$uimg  = trim($ucode[5]);
								if (isset($ucode[7]) && ($ucode[7] != NULL || $ucode[7] != "")) {
									$utrip = $ucode[7];
								} else {
									$utrip = $ucode[0];
								}
								$url = urldecode($init->within_str($value[0], "<id>", "</id>"));
								$ff = urldecode($init->within_str(max($value), "<msg>", "</msg>"));
								$unreadCounter = ($init->within_str(max($value), "<read>", "</read>")==1) ? 'read' : 'unread'  ;

								echo "<li class='list-group-item ".$unreadCounter."'>
									 		<div class='media panel6'>
									 			<a class='pull-left' href='#'>
									 				<div class='ic2'>
														<img class='pic2' src=\"http://204.63.8.28/ico/{$uimg}\" >
									 				</div><!--ic2-->
									 			</a>
									 			<div class='media-body'>
									 				<div class='panel6'>
									 					<a href='message.php?d={$url}'>
									 						<h5 class='media-heading'>{$utrip}</h5>
									 						<div class='limit'>{$ff}</div>
									 					</a>
									 				</div><!--panel6-->
									 			</div><!--media-body-->
									 		</div> <!--media-->
									 	</li>";					
							}
					}
					?>
				
				</ul>
			</div><!--hide/"" pagination-->
		</div><!--pad panel panel-default-->
	</div><!--well well-lg-->

<?php 
require 'include/footer.php';
?>
