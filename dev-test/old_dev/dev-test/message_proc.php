<?php

if (!isset($_COOKIE['DMDM']) && !isset($_COOKIE['MDMD'])) {
        header('location:index.php');
        exit;
}
include('encrypt.php');

$id   = strip_tags(addslashes(trim($_POST['zxcvbnm'])));
$subj = addslashes(trim($_POST['subj']));
$inf  = addslashes(trim($_POST['msg']));

$message  = new Message;
$subj     = $message->BeSanitize($subj);
$sanitize = $message->BeSanitize($inf);

if ($sanitize<>false) {
	$user   = $message->FindUser($id,$id_mail_path);
	$sender = $message->GetID($_COOKIE['DMDM'],$id_mail_path);
	$ckp    = $message->CheckPostLimit($sender,$plimit_path,TRUE);
	if (($ckp >= 60) && ($ckp != false)) {
        	echo "Post limit reached, Please wait for next minute";
        	exit;
	}
	$ckp    = $message->CheckPostLimit($sender,$plimit_hpath,FALSE);
        if (($ckp >= 600) && ($ckp != false)) {
                echo "Post limit reached, Please wait for next hour";
                exit;
        }
	$write  = $message->WriteMDB($user,$sanitize,$sender,$subj,$id);

	if ($write) {
		$message->AddPostLimit($sender,$plimit_path,TRUE);
		$message->AddPostLimit($sender,$plimit_hpath,FALSE);
		echo "Message Sent";
	} else {
		echo "Error, please try again later";
	}
} else {
	echo "Content too long!";
}

?>
