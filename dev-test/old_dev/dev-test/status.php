<?php


//DMDM = email
//MDMD = password
include('encrypt.php');
// isset if login or not
if (!isset($_COOKIE['DMDM']) && !isset($_COOKIE['MDMD'])) {
	header('location:index.php');
	exit;
}

$icoimg="";

$email = Decrypt($_COOKIE['DMDM'],KEY);
$email = filter_var($email, FILTER_SANITIZE_EMAIL);

if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
exit;
}

include('inf.php');

$fdir = substr($email, 0, 1);
$sdir = substr($email, 1, 1);
$data = @file(DB_PATH."$fdir/$sdir/$email");

if(count($data)<3) {
	foreach($_COOKIE as $ky => $vl)
		setcookie($ky,"",time()-3600);
	header('location:index.php');
	exit;
}

for ($i=0; $i < count($data); $i++) {
	$data[$i] = trim($data[$i]);
}
if(count($data)>5){ 
	$mystatus = '';
	if($data[6]!=FORGOT){
		$mystatus = urldecode($data[6]);
		$mystatus = str_replace("<br>","\n",$mystatus);
	}
}

$e_ = (isset($_GET['e_'])) ? strip_tags(addslashes(filter_var($_GET['e_'], FILTER_SANITIZE_STRING))):'';
if($e_=='1'){
  $isforedit =true;
}

if (isset($_GET['ico'])) {
	$ico = $_GET['ico'];
	$ico=trim($ico);
	if(preg_match('/[^a-z_\-0-9._]/i', $ico)){
                $ico ="";
	}

        $type =Array(1 => 'gif'); //store all the image extension types in array
        $ext  = explode(".",$ico); //explode and find value after dot
	if(!(in_array($ext[1],$type))) //check image extension not in the array $type
	{
                echo "412";
                exit;
	}

        $icoimg    = "<img src=\"http://204.63.8.28/ico/".$ico."\" class='pic'/>";
        $data[5]   =$ico;
        $writeData = implode("\n",$data);
        file_put_contents(DB_PATH."$fdir/$sdir/$email", $writeData);
        header('location:status.php');
}

if (isset($data[5]) && !empty($data[5])) {
        $ico=trim($data[5]);
        if(preg_match('/[^a-z_\-0-9._]/i', $ico))
        {
                $ico ="";
        }
  	$icoimg = "<img src=\"http://204.63.8.28/ico/".$ico."\" class='pic'/>";
}
  
include "include/header.php";

$inbox = new Message;
$uid   = $inbox->GetID($_COOKIE['DMDM'],$id_mail_path);

?>

<body>
        <div class="well well-sm pad3">

        <!-- navigation-->
        <?php require 'include/nav.php'; ?>
        <!-- end navigation-->

        <!--<h3 class="form-signin-heading">Xe[^Xy[W</h3>-->

        <a href="choose.php"><div class="ic panel panel-default pull-left"><?php echo $icoimg; ?></div></a><!-- ACR“NbNX --> 
      
        <!-- start tripcode -->
        <div class="panel2 panel panel-default ">
        <h5><strong>トリップ:</strong></h5>
        <!-- tripcode -->
        <div class="tripc input-group">
                <span class="input-group-addon"><strong>#</strong></span>
                <input type="text" class="form-control" name="trip" id="trip_c" value="<?php print $data[7];?>" />
                <!-- <input type="text" class="form-control" value="B1VjUMK0na7D"/> -->
                <span class="input-group-btn">
                <button class="btn btn-default" type="submit" value="vZ" id="caltrip" />
                        <span class="glyphicon glyphicon-random"></span>
                </button>
                </span>
        </div>
      
        </div>
        <!-- end tripcode!!   -->

        <!-- start status -->
        <hr>
        <div class="panel4 panel panel-default b ">
                <strong>紹介文</strong>
                <?php if(!$isforedit){
                        $hide="";
                } else {
                        $hide="hide";
                ?>
                <form method="post">
                        <?php if(isset($echo)) echo "<span style=\"color:green\">$echo</span>"; ?>
                        <button name="isca" class="bottom btn btn-default pull-right btn-sm">
                                <span class="glyphicon glyphicon-remove"></span>
                        </button>
                        <button name="isub" class="bottom btn btn-default pull-right btn-sm">
                                <span class="glyphicon glyphicon-ok"></span>
                        </button> 
                        <div class="clearfix"></div>
                        <textarea rows="4" id="input" name="info" class="textpanel form-control" maxlength="250"><?php echo $mystatus; ?></textarea>
                </form>

                <?php
                }
                ?>

                <a href="<?php echo $_SERVER['PHP_SELF'] . '?e_=1'; ?>" class="<?php echo $hide; ?> bottom btn btn-default pull-right btn-sm">
                <span class="glyphicon glyphicon-edit"></span>
                </a>
                <div class="clearfix"></div>
                <div  class="wordwrap_ panel panel-default panel6 scroll <?php echo $hide; ?>"> <?php echo $mystatus; ?></div>

        </div>
        <!-- end status -->
        <hr>
        <div class="clearfix"></div>
    
        <div class="panel3 panel panel-default txt ">
                <?php 
                        echo 
                        "<h5><strong>Eメール:</strong> ".$data[1]."</h5>
                        <h5><strong>点数: </strong><span class='badge'> ".$data[3]."</span></h5>";
                ?>
                <strong>古いアカウントの移行</strong><br><a href="mailto:migrate@2ch.net">証明として、旧アカウントの詳細を電子メールで送信します。</a><br><br>
                
                <h4 class="text pull-right"> <a href="logout.php"><span class="label label-danger">ログアウトする</span></a> </h4>
                <h4 class="text pull-right"> <a href="change_pass.php"><span class="label label-primary">パスワードを変更する</span></a> </h4>
                <div class="clearfix"></div>
        </div>

        </div>

<?php 
        require 'include/footer.php';
?>
