<html lang="en">
  <head>
    <meta charset="Shift_JIS">
    <title>ログイン</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
  </head>
  <body>
<?php
include('config.php');

$g = $_GET;
if (isset($g['e']) && $g['e'] != '' && isset($g['k']) && $g['k'] != '') {

	$email = trim($g['e']);
	$key   = trim($g['k']);

	$email = filter_var($email, FILTER_SANITIZE_EMAIL);
	$key   = filter_var($key, FILTER_SANITIZE_STRING);

if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
	echo "bad"; 
	exit;
}

	$email = strip_tags(addslashes($g['e']));
	$key   = strip_tags(addslashes($g['k']));
	
	$fdir  = substr($email, 0, 1);
	$sdir  = substr($email, 1, 1);
	$data  = @file(DB_PATH."$fdir/$sdir/$email");

	if(count($data)>3){
		for ($i=0; $i < count($data); $i++) { 
			$data[$i] = trim($data[$i]);
		}
		$mainkey = md5($data[1].$data[2]);
		if ($mainkey != $key) {
			$mainkey = hash("sha256", $data[1].$data[2]);
			if($mainkey != $key) {
				echo "Invalid Link";
				exit;
			}
		}
		if ($data[4] != 1) {
			$ico       = isset($data[5])?$data[5]:'nida.gif';
			$writeData = $data[0]."\n".$data[1]."\n".$data[2]."\n1000\n1\n".$ico;
			file_put_contents(DB_PATH."$fdir/$sdir/$email", $writeData);
			echo "追加された1000年のポイント";
		} else {
			echo "既に確認された";
		}
	} else {
		echo "ユーザーは存在しません";
	}
} else {
	echo "Error 3";
}

?>
</body>
</html>
