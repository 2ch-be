<?php
if (!isset($_COOKIE['ADMDM']) && !isset($_COOKIE['AMDMD'])) {
        header('location:artist_login.php');
        exit;
}
include("a_cont.php");

if (!isset($_SESSION['utype'])) {
	header("location: a_out.php");
}

echo $_SESSION['artistid'];

$email   = Decrypt($_COOKIE['ADMDM'],AKEY);
$dir     = loggedEmail($email, "adb/");
// echo $dir;
$dirFull = loggedEmail($email, ADB_PATH);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Artist Page</title>
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/astyle.css">
</head>
<body>
	<div class="container body">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<center>
							<img src="css/img/2ch_logo.gif" alt="">
						</center>
					</div>
					<div class="main">
						<div class="pull-right" style="margin:10px 0px;"><a href="a_out.php" class="btn btn-danger btn-sm" style="color:white;">LOGOUT</a></div>
						<div class="clearfix"></div>
						<div class="row">
							<div class="col-md-3">
								<div class="panel panel-default">
									<div class="panel-heading">
										<center>
											<b>Options</b>
										</center>
									</div>
									<ul id="al" style="list-style:none; display:inline;">
										<?php if ($_SESSION['utype'] == "admin") { ?>
										<li><a href="?crar">Create Artist Account</a>&nbsp;&nbsp;&nbsp;</li>
										<li><a href="?vtrans">View Transactions</a>&nbsp;&nbsp;&nbsp;</li>
										<li><a href="?alist">Artist List</a></li>
										<?php } elseif ($_SESSION['utype'] == "user") { ?>
										<li><a href="?art">Art List</a></li>
										<li><a href="?upload">Upload Image</a>&nbsp;&nbsp;&nbsp;</li>
										<?php } ?>
									</ul>
								</div>
							</div>
							<div class="col-md-9">
								<?php
									if(isset($_SESSION['msg'])){
										echo $_SESSION['msg'];
										$_SESSION['msg'] = '';
									}
								?>
								<div class="panel panel-default optionlistview" style="background:#f5f5f5;">

									<?php if(isset($_GET['art'])){ 

									if(file_exists("/home/adb/imglist.log")){
										$file = file("/home/adb/imglist.log");
										if(isset($_GET['c'])){
											if($_GET['c']=="delete"){
												$id       = $_GET['id'];
												$filename = within_str($file[$id], "<name>", "</name>");
												$file[$id]= str_replace("<stat></stat>","<stat>deleted</stat>",$file[$id]);
												$data     = implode("", $file);
												$handler  = fopen("/home/adb/imglist.log", "w");
												fwrite($handler, $data);
												fclose($handler);
												/*$test = unlink("/home/auth/public_html/dev-test/premium/".$filename.".jpg");*/
												/*if($test){
													$_SESSION['msg'] = "<div class=\"alert alert-success\">We've successfully deleted the file.</div>";
													header("Location: artist.php?art");
													exit;
												}else{
													$_SESSION['msg'] = "<div class=\"alert alert-danger\">Unable to delete file. Either file doesn't exists</div>";
													header("Location: artist.php?art");
													exit;
												}*/
												$_SESSION['msg'] = "<div class=\"alert alert-success\">We've successfully deleted the file.</div>";
													header("Location: artist.php?art");
											}
										}
										$i = 0;
										foreach ($file as $key => $value) {
											$img =  within_str($value, "<name>", "</name>").".jpg";
											$stat =  within_str($value, "<stat>", "</stat>");
											if(!$stat){
												echo "<div class=\"thumbnail pull-left\" >";
													echo "<img width=\"100px\" src=\"{$livesitePath}dev-test/premium/{$img}\" alt=\"{$key}\"><br><br>";
													echo "<center>".within_str($value, "<name>", "</name>")." <br> " 
													.within_str($value, "<price>", "</price>"). " MELON <br> <a href=\"artist.php?art&c=delete&id={$i}\">delete </a></center>" ;
												echo "</div>";
											}
											$i++;
											if($i%4==0) { echo "<div class=\"clearfix\"></div>"; }
										}
									}else{
										echo "Please upload your work";
									}


										 }else if(isset($_GET['upload'])){ ?>

									<?php
										if(isset($_POST['submitupload'])){
											if(is_numeric($_POST['price'])){
										
												$price   = (int) $_POST['price'];
												$name    = $_POST['name'];
												
												$tmpName = $_FILES['uploadedfile']['tmp_name'];
												
												$fdir    = substr($email, 0, 1);
												$sdir    = substr($email, 1, 1);
												$path    = ADB_PATH."{$fdir}/{$sdir}/{$email}";
												if (!file_exists($path)) {
													@mkdir(ADB_PATH."{$fdir}");
													@mkdir(ADB_PATH."{$fdir}/{$sdir}");
													@mkdir(ADB_PATH."{$fdir}/{$sdir}/{$udir}");
												}
												if($_FILES["file"]["type"] == "image/gif"){
												$dirPath         = "/home/auth/public_html/dev-test/premium/";
												$name            = lcfirst($_POST['name'])."_".rand(10000,99999);
												$fname           = $name."gif";
													if(!file_exists($dirPath.$fname)){
														if(move_uploaded_file($tmpName, $dirPath.$fname)) {
														    $uploadedFile = true;

														} else{
														    $uploadedFile = false;
														}
													}else{
														$_SESSION['msg'] = "<div class=\"alert alert-danger\">File already exist.</div>";
														header("Location: artist.php?upload");
														exit;
													}
												}else{
													$_SESSION['msg'] = "<div class=\"alert alert-danger\">Please upload GIF image only.</div>";
													header("Location: artist.php?upload");
													exit;
												}
											

												// if($uploadedFile){
												// 	$time      = time();
												// 	$writeData = "<time>{$time}</time><name>{$name}_".rand(00000,99999)."</name><price>{$price}</price>\n";
												// 	$filename  = $path."/imagelist.txt";

												// 	if (!file_exists($filename)) {
												// 		if(!file_put_contents($filename,$writeData)) {
												// 			$writeSuccess = false;
												// 		}
												// 			$writeSuccess = true;
												// 	} else {
												// 		$handle = fopen($filename, "a");
												// 		if(!fwrite($handle, $writeData)) {
												// 			$writeSuccess = false;
												// 		}
												// 		fclose($handle);
												// 			$writeSuccess = true;
												// 	}
												// }

												if($uploadedFile){
													$handle = fopen("/home/adb/imglist.log", "a+");
													fwrite($handle, "<id>".$_SESSION['artistid']."</id><name>{$name}</name><price>{$price}</price><stat></stat>\n");
													fclose($handle);

													$_SESSION['msg'] = "<div class=\"alert alert-success\">File successfully uploaded.</div>";
													header("Location: artist.php?art");
													exit;
												}else{
													$_SESSION['msg'] = "<div class=\"alert alert-danger\">Unable to upload file. Please contact admin.</div>";
													header("Location: artist.php?upload");
													exit;
												}

											}else{
												$_SESSION['msg'] = "<div class=\"alert alert-danger\">Price must be numeric.</div>";
												header("Location: artist.php?upload");
												exit;
											}
										}
									?>

										<form role="form"  enctype="multipart/form-data" action="<?php echo $_SERVER['PHP_SELF'] ?>?upload" method="POST">
											<input type="hidden" name="MAX_FILE_SIZE" value="10000000" />
												<div class="form-group">
													<label for="name">Name:</label> <br>
													<input name="name" type="text" class="form-control" placeholder="Name"/><br />
												</div>	
												<div class="form-group">
													<label for="price">Price:</label> <br>
													<input name="price" type="text" class="form-control" placeholder="Price"/><br />
												</div>		
												<div class="form-group">
													<label for="price">Uploaded File:</label> <br>
													<input name="uploadedfile" class="form-control" type="file" placeholder="Price"/><br />
												</div>
											<input type="submit" class="btn btn-primary" name="submitupload" value="Upload File" />
											<input type="reset" class="btn btn-danger" name="submit" value="Reset" />
										</form>


									<?php }else if(isset($_GET['edit'])){
										$file  = file(ADB_PATH."imglist.log");
										// print_r(expression)
										$index = $_GET['edit'];
										$value = $file[$index]; 
										$name  = within_str($value, "<name>", "</name>");
										$price = within_str($value, "<price>", "</price>");
										$time  = within_str($value, "<time>", "</time>");
										$idVal = within_str($value, "<id>", "</id>");


										if(isset($_POST['submitedit'])){
											$index        = $_GET['edit'];
											$newName      = lcfirst($_POST['name'])."_".rand(10000,99999);
											$newPrice     = $_POST['price'];
											unset($file[$index]);

											$file[$index] = "<id>".$idVal."</id><name>{$newName}</name><price>{$newPrice}</price><stat></stat>\n";
											$data         = implode("", $file);
											$handler      = fopen(ADB_PATH."imglist.log", "w");
											fwrite($handler, $data);
											fclose($handler);
											rename("/home/auth/public_html/dev-test/premium/".$name.".gif", "/home/auth/public_html/dev-test/premium/".$newName.".gif");
											$_SESSION['msg'] = "{$name} is changed into {$newName}.";
											header("Location: artist.php?art");
											exit;

										}

									 ?>
										<form role="form"  action="<?php echo $_SERVER['PHP_SELF'] ?>?edit=<?=$index?>" method="POST">
											<input type="hidden" name="MAX_FILE_SIZE" value="10000000" />
												<div class="form-group">
													<label for="name">Name:</label> <br>
													<input name="name" type="text" class="form-control" value="<?php echo $name ?>" placeholder="Name"/><br />
												</div>	
												<div class="form-group">
													<label for="price">Price:</label> <br>
													<input name="price" type="text" class="form-control" value="<?php echo within_str($value, "<price>", "</price>") ?>" placeholder="Price"/><br />
												</div>		
											<input type="submit" class="btn btn-primary" name="submitedit" value="Submit" />
											<input type="reset" class="btn btn-danger" name="submit" value="Reset" />
										</form>
									<?php } ?>

									<!-- nakadisplay none to option for create artist account -->
									<?php if (isset($_GET['crar'])) { ?>
									<form role="form" method="post"> 
									  <div class="form-group">
									    <label for="email">Email address</label>
									    <input type="email" class="form-control" id="email" name="user" placeholder="Enter email">
									  </div>
									  <div class="form-group">
									    <label for="password">Password</label>
									    <input type="password" class="form-control" id="password" name="pass" placeholder="Password">
									  </div>
									  <div class="form-group">
									    <label for="confirmpassword">Confirm Password</label>
									    <input type="password" class="form-control" id="confirmpassword" name="cpass" placeholder="Confirm Password">
									  </div>
									  <input type="submit" class="btn btn-primary" name="asub" value="Create Artist">
									</form>
									<?php } ?>
									<!-- END nakadisplay none to option for create artist account -->
									
									<!-- option for view transaction -->
									<?php if (isset($_GET['vtrans'])) { ?>
									<table class="table table-striped" style="background:white;">
										<tr>
											<td>Number</td>
											<td>Artist Name</td>
											<td>Transaction</td>
											<td>Date</td>
										</tr>
										<tr>
											<td>1</td>
											<td>Example Name</td>
											<td>Melon Points</td>
											<td>September 24, 2014</td>
										</tr>
										<tr>
											<td>2</td>
											<td>Example Name</td>
											<td>Melon Points</td>
											<td>September 24, 2014</td>
										</tr>
										<tr>
											<td>3</td>
											<td>Example Name</td>
											<td>Melon Points</td>
											<td>September 24, 2014</td>
										</tr>
										<tr>
											<td>4</td>
											<td>Example Name</td>
											<td>Melon Points</td>
											<td>September 24, 2014</td>
										</tr>
									</table>
									<?php } ?>
									<!-- END option for view transaction -->
									<?php if (isset($_GET['alist'])) { ?>
									<div class="row">
										<div class="col-md-3">
											<div class="panel panel-default">
												<center>
													<h4>Artist List</h4>
												</center>
												<ul id="al" style="list-style:none; display:inline;">
													<?php include("a_list.php"); ?>
												</ul>
											</div>
										</div>
										<div class="col-md-9">
											<div class="panel panel-default optionlistview">
												<?php
												if (isset($_GET['v'])) {
													error_reporting(E_ALL); ini_set("display_errors", 1);
													$__utmp = file(ADB_PATH."imglist.log");
													foreach ($__utmp as $key => $value) {
														if (trim($_GET['v']) == trim(within_str($value,"<id>","</id>")) && trim(within_str($value,"<stat>","</stat>")) != "deleted") {
															echo	"<div class=\"panel panel-default alist_art\">
																			<a href=\"#\" class=\"thumbnail\">
																			<img src=\"http://be.2ch.net/dev-test/premium/".within_str($value,"<name>","</name>")."\" style=\"width:80px; height:80px;\" alt=\"\">
																			</a>
																			<span class=\"pull-right\">MP</span>
																			<span class=\"pull-left\">Artist ID</span>
																			<div class=\"clearfix\"></div>
																		</div>";
														}
													}
												}
												?>
												<div class="clearfix"></div>
											</div>

										</div>
									</div>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--script src="js/jquery-1.9.1.js"></script>
	<script src="js/bootstrap.js"></script-->
</body>
</html>

