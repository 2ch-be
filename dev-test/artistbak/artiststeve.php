<?php
if (!isset($_COOKIE['ADMDM']) && !isset($_COOKIE['AMDMD'])) {
        header('location:artist_login.php');
        exit;
}
include("a_cont.php");
if (!isset($_SESSION['utype'])) {
	header("location: a_out.php");
}

$email   = Decrypt($_COOKIE['ADMDM'],AKEY);
$dir     = loggedEmail($email, "adb/");
// echo $dir;
$dirFull = loggedEmail($email, ADB_PATH);

function FindUser($id) {
	$list = file("/home/adb/artistlist.log");
	$em   = "";
	$err  = true;
	foreach ($list as $key => $value) {
		if (substr(trim($value), 0, 9) == $id) {
			$r = explode("<><>", $value);
			$em = trim($r[1]);
			$err = false;
		}
	}
	if ($err) {
		return false;
	}
	return $em;
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Artist Page</title>
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/astyle.css">
</head>
<body>
	<div class="container body">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<center>
							<img src="css/img/2ch_logo.gif" alt="">
						</center>
					</div>
					<div class="main">
						<div class="pull-right" style="margin:10px 0px;"><a href="a_out.php" class="btn btn-danger btn-sm" style="color:white;">LOGOUT</a></div>
						<div class="pull-left" style="margin:10px 0px;">
							<h4><i>Welcome,</i><b> 
								<?php 
									$artistname = explode ("@", $_SESSION['aname']);
									echo $artistname[0];
								?>!</b></h4>
						</div>
						<div class="clearfix"></div>
						<div class="row">
							<div class="col-md-3">
								<div class="panel panel-default">
									<div class="panel-heading">
										<center>
											<b>Options</b>
										</center>
									</div>
									<ul id="al" style="list-style:none; display:inline;">
										<?php if ($_SESSION['utype'] == "admin") { ?>
										<li><a href="?crar">Create Artist Account</a>&nbsp;&nbsp;&nbsp;</li>
										<li><a href="?vtrans">View Transactions</a>&nbsp;&nbsp;&nbsp;</li>
										<li><a href="?alist">Artist List</a></li>
										<?php } elseif ($_SESSION['utype'] == "user") { ?>
										<li><a href="?art">Art List</a></li>
										<li><a href="?upload">Upload Image</a>&nbsp;&nbsp;&nbsp;</li>
										<?php } ?>
									</ul>
								</div>
							</div>
							<div class="col-md-9">
									
								<?php
									if(empty($_GET)){
										echo "<h3><b>Artist Area!</b></h3>";
									}
									if(isset($_SESSION['msg'])){
										echo $_SESSION['msg'];
										$_SESSION['msg'] = '';
									}
								?>

									<?php if(isset($_GET['art'])){ 
									echo "<div class=\"panel panel-default optionlistview\"><div class=\"panel-heading\"><center><b>Artworks</b></center></div><br>";

									if(file_exists("/home/adb/imglist.log")){
										$file = file("/home/adb/imglist.log");
										if(isset($_GET['c'])){
											if($_GET['c']=="delete"){
												$id       = $_GET['id'];
												$filename = within_str($file[$id], "<name>", "</name>");
												$file[$id]= str_replace("<stat></stat>","<stat>deleted</stat>",$file[$id]);
												$data     = implode("", $file);
												$handler  = fopen("/home/adb/imglist.log", "w");
												fwrite($handler, $data);
												fclose($handler);
												/*$test = unlink("/home/auth/public_html/dev-test/premium/".$filename.".jpg");*/
												/*if($test){
													$_SESSION['msg'] = "<div class=\"alert alert-success\">We've successfully deleted the file.</div>";
													header("Location: artist.php?art");
													exit;
												}else{
													$_SESSION['msg'] = "<div class=\"alert alert-danger\">Unable to delete file. Either file doesn't exists</div>";
													header("Location: artist.php?art");
													exit;
												}*/
												$_SESSION['msg'] = "<div class=\"alert alert-success\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>We've successfully deleted the file.</div>";
												header("Location: artist.php?art");
											}
										}
										$i = 0;
										foreach ($file as $key => $value) {
											$img  =  within_str($value, "<name>", "</name>").".gif";
											$stat =  within_str($value, "<stat>", "</stat>");
											$id =  within_str($value, "<id>", "</id>");
											if(!$stat && $id==$_SESSION['artistid']){
												echo "<div class=\"panel panel-default alist_art\">";
												echo "<a href=\"#\" class=\"thumbnail\" style=\"margin-bottom: 1em!important;\">";
												echo "<img src=\"{$livesitePath}dev-test/premium/{$img}\" style=\"height:80px;\" alt=\"\">";
												echo "</a>";
												echo "<center>";
												echo "<span>" .within_str($value, "<price>", "</price>"). " MP</span><br>";
												echo "<a href=\"artist.php?art&c=delete&id={$i}\">delete </a>";
												echo "<a href=\"artist.php?edit={$i}\">edit </a></center>";
												echo "<div class=\"clearfix\"></div>";
												echo "	</div>";
											}
											$i++;
											// if($i%4==0) { echo "<div class=\"clearfix\"></div>"; }
											
										}
										echo "<div class=\"clearfix\"></div>";
									}else{
										echo "Please upload your work";
									}

											echo "</div>";

										 }else if(isset($_GET['upload'])){ ?>
											
									<?php
										

									if(isset($_POST['submitupload'])){
										if($_FILES['uploadedfile']['size']<100000){
											if(is_numeric($_POST['price'])){
										
												$price   = (int) $_POST['price'];
												$price   = abs($price);
												$name    = $_POST['name'];
												
												$tmpName = $_FILES['uploadedfile']['tmp_name'];
												
												$fdir    = substr($email, 0, 1);
												$sdir    = substr($email, 1, 1);
												$path    = ADB_PATH."{$fdir}/{$sdir}/{$email}";
												if (!file_exists($path)) {
													@mkdir(ADB_PATH."{$fdir}");
													@mkdir(ADB_PATH."{$fdir}/{$sdir}");
													@mkdir(ADB_PATH."{$fdir}/{$sdir}/{$udir}");
												}
												// echo $_FILES["file"]["type"];
												// die;
												if($_FILES["uploadedfile"]["type"] == "image/gif"){
												$dirPath         = "/home/auth/public_html/dev-test/premium/";
												$name            = rand(1000000,9999999);
												$fname           = $name.".gif";
													if(!file_exists($dirPath.$fname)){
														if(move_uploaded_file($tmpName, $dirPath.$fname)) {
														    $uploadedFile = true;

														} else{
														    $uploadedFile = false;
														}
													}else{
														$_SESSION['msg'] = "<div class=\"alert alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>File already exist.</div>";
														header("Location: artist.php?upload");
														exit;
													}
												}else{
													$_SESSION['msg'] = "<div class=\"alert alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>Please upload GIF image only.</div>";
													header("Location: artist.php?upload");
													exit;
												}
											

												// if($uploadedFile){
												// 	$time      = time();
												// 	$writeData = "<time>{$time}</time><name>{$name}_".rand(00000,99999)."</name><price>{$price}</price>\n";
												// 	$filename  = $path."/imagelist.txt";

												// 	if (!file_exists($filename)) {
												// 		if(!file_put_contents($filename,$writeData)) {
												// 			$writeSuccess = false;
												// 		}
												// 			$writeSuccess = true;
												// 	} else {
												// 		$handle = fopen($filename, "a");
												// 		if(!fwrite($handle, $writeData)) {
												// 			$writeSuccess = false;
												// 		}
												// 		fclose($handle);
												// 			$writeSuccess = true;
												// 	}
												// }

												if($uploadedFile){
													$handle = fopen("/home/adb/imglist.log", "a+");
													fwrite($handle, "<id>".$_SESSION['artistid']."</id><name>{$name}</name><price>{$price}</price><stat></stat>\n");
													fclose($handle);

													$_SESSION['msg'] = "<div class=\"alert alert-success\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>File successfully uploaded.</div>";
													header("Location: artist.php?art");
													exit;
												}else{
													$_SESSION['msg'] = "<div class=\"alert alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>Unable to upload file. Please contact admin.</div>";
													header("Location: artist.php?upload");
													exit;
												}

											}else{
												$_SESSION['msg'] = "<div class=\"alert alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>Price must be numeric.</div>";
												header("Location: artist.php?upload");
												exit;
											}
										}else{
												$_SESSION['msg'] = "<div class=\"alert alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>File size is more than 100kb.</div>";
												header("Location: artist.php?upload");
												exit;
										}
									}
										
									?>

										<div class="panel panel-default optionlistview">
											<div class="panel-heading">
												<center><b>Upload Artwork</b></center>
											</div><br>
											<form role="form"  style="width:50%;" enctype="multipart/form-data" action="<?php echo $_SERVER['PHP_SELF'] ?>?upload" method="POST">
													<div class="form-group">
														<label for="price">Price:</label> <br>
														<input name="price" type="text" class="form-control" placeholder="Price"/><br />
													</div>		
													<div class="form-group">
														<label for="price">Uploaded File: </label> <br>
														<input name="uploadedfile" class="form-control" type="file" placeholder="Price"/>
														<span style="font-size:12px;margin-top:3px;" class="pull-right">(only .gif image is accepted)</span>
														<div class="clearfix"></div>
													</div>
												<input type="submit" class="btn btn-primary" name="submitupload" value="Upload File" />
												<input type="reset" class="btn btn-danger" name="submit" value="Reset" />
											</form>
										</div>


									<?php }else if(isset($_GET['edit'])){
										echo "<div class=\"panel panel-default optionlistview\">";

										$file  = file(ADB_PATH."imglist.log");
										// print_r(expression)
										$index = $_GET['edit'];
										$value = $file[$index]; 
										$name  = within_str($value, "<name>", "</name>");
										$price = within_str($value, "<price>", "</price>");
										$time  = within_str($value, "<time>", "</time>");
										$idVal = within_str($value, "<id>", "</id>");


										if(isset($_POST['submitedit'])){
											$index        = $_GET['edit'];
											// $newName      = rand(1000000,9999999);
											$newPrice     = abs($_POST['price']);
											unset($file[$index]);

											$file[$index] = "<id>".$idVal."</id><name>{$name}</name><price>{$newPrice}</price><stat></stat>\n";
											$data         = implode("", $file);
											$handler      = fopen(ADB_PATH."imglist.log", "w");
											fwrite($handler, $data);
											fclose($handler);
											// rename("/home/auth/public_html/dev-test/premium/".$name.".gif", "/home/auth/public_html/dev-test/premium/".$newName.".gif");
											$_SESSION['msg'] = "<div class=\"alert alert-success\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>Successfully edit.</div>";
											header("Location: artist.php?art");
											exit;

										}
									 ?>
									 <div class="panel panel-default alist_art">
									 	<a href="#" class="thumbnail">
									 		<img src="<?php echo $livesitePath ?>dev-test/premium/<?php echo $name ?>" style="height:80px;" alt="">
									 	</a>	
									 </div>
									<div class="clearfix"></div>
									<br>
										<form role="form"  action="<?php echo $_SERVER['PHP_SELF'] ?>?edit=<?=$index?>" style="width:50%;" method="POST">
												<div class="form-group">
													<label for="price">Price:</label> <br>
													<input name="price" type="text" class="form-control" value="<?php echo within_str($value, "<price>", "</price>") ?>" placeholder="Price"/><br />
												</div>		
											<input type="submit" class="btn btn-primary" name="submitedit" value="Submit" />
										</form>
									<?php echo "</div>";} ?>

									<!-- nakadisplay none to option for create artist account -->
									<?php if (isset($_GET['crar'])) { ?>
										<div class="panel panel-default optionlistview">
										<div class="panel-heading">
											<center><b>Add New Artist</b></center>
										</div><br>
										<center>
											<form role="form" method="post" style="width:50%;"> 
											  <div class="form-group">
											    <label for="email" class="pull-left">Email address</label>
											    <input type="email" class="form-control" id="email" name="user" placeholder="Enter email">
											  </div>
											  <div class="form-group">
											    <label for="password" class="pull-left">Password</label>
											    <input type="password" class="form-control" id="password" name="pass" placeholder="Password">
											  </div>
											  <div class="form-group">
											    <label for="confirmpassword" class="pull-left">Confirm Password</label>
											    <input type="password" class="form-control" id="confirmpassword" name="cpass" placeholder="Confirm Password">
											  </div>
											  <input type="submit" class="btn btn-primary pull-left" name="asub" value="Create Artist">
											  <div class="clearfix"></div>
											</form>
										</center>
									</div>
									<?php } ?>
									<!-- END nakadisplay none to option for create artist account -->
									
									<!-- option for view transaction -->
									<?php if (isset($_GET['vtrans'])) { ?>
									<div class="panel panel-default optionlistview">
										<div class="panel-heading">
											<center><b>Transactions</b></center>
										</div><br>
										<?php
										if (!file_exists(ADB_PATH."artistlist.log")) {
											echo "No transactions";
										} else {
											if (!file_exists(ADB_PATH."transaction.log")) {
												echo "No transactions";
												exit;
											} else {
												echo "<div class=\"panel-group\" id=\"accordion\"> 
												<div class=\"panel panel-default\">
												";
												$arus = file(ADB_PATH."artistlist.log");
												$i    = 0;
												foreach ($arus as $key => $value) {
													$tmar 				= explode("<><>", $value);
													$usemail[$i]	= $tmar[1];
													$arid[$i]	    = $tmar[0];
													$i++;
												}
												$vtrans = file(ADB_PATH."transaction.log");
												foreach ($vtrans as $key2 => $value2) {
													$arr = within_str($value2,"<id>","</id>");
													$val = within_str($value2,"<price>","</price>");
													$kk  = array_search($arr, $arid);
													/*echo $arr."-".$val."-".$kk."<br>";*/
													if (isset($arval[$kk])) {
														$arval[$kk] = $arval[$kk]+$val;
													} else {
														$arval[$kk] = $val;
													}
												}
												$breakerArid = 0;
$ownnow = array();
												foreach ($arid as $key => $value) {
													if ($value!=66666666) {
														$kk  = array_search($value, $arid);
														if (isset($arval[$kk])) {

$ownnow[trim($usemail[$kk])] = $arval[$kk];
/*var_dump($usemail);*/
															/*echo "<div class=\"panel-heading\">
															<a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#".$breakerArid."\">
																			<strong>".$usemail[$kk]."</strong> - 
																			<strong>".@$arval[$kk]." Melon Points</strong>
																		</a>
																		</div>
															<div id=\"".$breakerArid."\" class=\"panel-collapse collapse\">
															  <div class=\"panel-body\">
															  		";
														  			foreach ($test as $key2 => $value2) {

														  			}
													  		echo "
															  </div>
															</div>
																		";*/

														$breakerArid++;
														}
													}
												}

/*var_dump($ownnow);*/

												$breaker = 0;
												foreach ($vtrans as $key => $value) {
													$test[within_str($value,"<id>","</id>")][$breaker] = $value;	
													$breaker++;
												}

												// echo "<pre>";
												// print_r($test);
												// echo "</pre>";
												$count = 0;
												$array = "";

												foreach ($test as $key => $value) {
													foreach ($value as $key2 => $value2) {
														$array[within_str($value2, "<name>", "</name>")] = 0;
													}
												}
/*var_dump($test);*/
												foreach ($test as $key => $value) {

													echo "<div class=\"panel-heading\">
														<a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#".$key."\">
													".FindUser($key)."  ".$ownnow[trim(FindUser($key))]."</a></div>
													<div id=\"".$key."\" class=\"panel-collapse collapse\">
													";
													foreach ($value as $key2 => $value2) {
														echo "<img width='30px' height='30px' src=\"".$livesitePath."premium/".within_str($value2, "<name>", "</name>").".gif\">".within_str($value2, "<price>", "</price>")."ASD<br>";
													}
													echo "</div></div>";
												}

											// 	foreach ($arid as $key => $value) {
											// 		if ($value!=66666666) {
											// 			$kk  = array_search($value, $arid);
											// 			/*if ($arval[$kk] != "" || $arval[$kk] != NULL) {*/
											// 			if (isset($arval[$kk])) {



											// 				echo "<div class=\"panel-heading\">
											// 				<a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#".$breakerArid."\">
											// 								<strong>".$usemail[$kk]."</strong> - 
											// 								<strong>".@$arval[$kk]." Melon Points</strong>
											// 							</a>
											// 							</div>
											// 				<div id=\"".$breakerArid."\" class=\"panel-collapse collapse\">
											// 				  <div class=\"panel-body\">
											// 				  		";
											// 			  			foreach ($test as $key2 => $value2) {

											// 			  			}
											// 		  		echo "
											// 				  </div>
											// 				</div>
											// 							";

											// 			$breakerArid++;
											// 			}
											// 		}
											// 	}
											// }
										}
									}
										?>
										</div>
									</div>
									</div>


									<?php } ?>
									<!-- END option for view transaction -->
									<?php if (isset($_GET['alist'])) { ?>
									<div class="row">
										<div class="col-md-3">
											<div class="panel panel-default">
												<center>
													<h4>Artist List</h4>
												</center>
												<ul id="al" class="wordwrap_" style="list-style:none; display:inline;">
													<?php include("a_list.php"); ?>
												</ul>
											</div>
										</div>
										<div class="col-md-9">
											<div class="panel panel-default optionlistview">
												<div class="panel-heading"><center><b>Artworks</b></center></div><br>
												<?php												
												if (isset($_GET['v'])) {
													error_reporting(E_ALL); ini_set("display_errors", 1);
													if (!file_exists(ADB_PATH."imglist.log")) {
														# code...
													} else {
														$__utmp = file(ADB_PATH."imglist.log");
														foreach ($__utmp as $key => $value) {
															if (trim($_GET['v']) == trim(within_str($value,"<id>","</id>")) && trim(within_str($value,"<stat>","</stat>")) != "deleted") {
																echo	"<div class=\"panel panel-default alist_art\">
																				<a href=\"#\" class=\"thumbnail\" style=\"margin-bottom: 1em!important;\">
																				<img src=\"http://be.2ch.net/dev-test/premium/".within_str($value,"<name>","</name>")."\" style=\"height:80px;\" alt=\"\">
																				</a>
																				<span class=\"pull-right\">".within_str($value, "<price>", "</price>")."MP</span>
																				<div class=\"clearfix\"></div>
																			</div>";
															}
														}
													}
												} else {
													echo "Select Artist";
												}
												?>
												<div class="clearfix"></div>
											</div>

										</div>
									</div>
									<?php } ?>
									<div class="clearfix"></div>
								<!-- </div> -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="js/jquery-1.9.1.js"></script>
	<script src="js/bootstrap.js"></script>
</body>
</html>

