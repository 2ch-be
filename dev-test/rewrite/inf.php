<?php
// include('include/config.php');

if(count($_POST)>0){
	//if (isset($_POST['info']) && !empty($_POST['info'])) {
	if (isset($_POST['info'])) {
		if(isset($_POST['isca'])){
			header('location:status.php');
			exit;
		}
		$inf = $_POST['info'];

		if (strlen($inf) > 250) {
			$_SESSION['msg'] = "<div class=\"alert alert-success\">Content too long</div>";
			header('Location:'.$livesitePath.'status.php');
			exit;
		}else{
			$inf = str_replace("<", "&lt;", $inf);
			$inf = str_replace(">", "&gt;", $inf);
			$inf = str_replace("\n", "<br>", $inf);
			$inf = str_replace(" ", "&nbsp;", $inf);
			$inf = urlencode($inf);
			$email = Decrypt($_COOKIE['DMDM'],KEY);
			$email = filter_var($email, FILTER_SANITIZE_EMAIL);
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				$_SESSION['msg'] = "<div class=\"alert alert-danger\">Invalid email</div>";
				header('Location:'.$livesitePath.'status.php');
				exit;
			}else{
				$fdir = substr($email, 0, 1);
				$sdir = substr($email, 1, 1);
				$data = @file(DB_PATH."$fdir/$sdir/$email");
				if(count($data)>3) {
					for ($i=0; $i < count($data); $i++) {
						$data[$i] = trim($data[$i]);
					}
					$writeData = $data[0]."\n".$data[1]."\n".$data[2]."\n".$data[3]."\n".$data[4]."\n".$data[5]."\n".$inf."\n".$data[7]."\n";
					if(file_put_contents(DB_PATH."$fdir/$sdir/$email", $writeData)) {
						$_SESSION['msg'] = "<div class=\"alert alert-success\">Successfully updated</div>";
						header('Location:'.$livesitePath.'status.php');
						exit;
					} else {
						var_dump(file_put_contents(DB_PATH."$fdir/$sdir/$email", $writeData));
						echo DB_PATH."$fdir/$sdir/$email";
						die();
						$_SESSION['msg'] = "<div class=\"alert alert-danger\">Error while updating database</div>";
						header('Location:'.$livesitePath.'status.php');
						exit;
					}
				} else {
					echo "<div class=\"alert alert-danger\">User doesn't Exists</div>";
					header('Location:'.$livesitePath.'status.php');
					exit;
				}
			}
		}
	} else {
		echo "Error 1";
	}
}
?>
