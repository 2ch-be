<html lang="en">
  <head>
    <meta charset="Shift_JIS">
    <title>パスワードを変更する</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
  </head>
  <body>
<?php
include('include/config.php');
$g = $_POST;
if(count($g)>1){
  if (isset($g['opass']) && isset($g['npass']) && isset($g['cpass']) && !empty($g['opass']) && !empty($g['npass']) && !empty($g['cpass'])) {
    $opass = filter_var($g['opass'], FILTER_SANITIZE_STRING);
    $npass = filter_var($g['npass'], FILTER_SANITIZE_STRING);
    $cpass = filter_var($g['cpass'], FILTER_SANITIZE_STRING);

    $opass = strip_tags(addslashes($opass));
    $npass = strip_tags(addslashes($npass));
    $cpass = strip_tags(addslashes($cpass));

    if ($npass != $cpass) {
      echo "新しいパスワードが一致していません。";
      exit;
    } else {
    if(!preg_match("/[a-z0-9 \!\"\#\$\%\&\'\(\)\*\+\,\-\.\/\:\;\<\=\>\?\@\[\]\^\_\{\}\|\~]{4,32}+/i",$npass)) {
	echo "Invalid password!";
	exit;
      }
    }
    
    $email = Decrypt($_COOKIE['DMDM'],KEY);
    $email = filter_var($email, FILTER_SANITIZE_EMAIL);

if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
echo "bad"; exit;}

    $fdir = substr($email, 0, 1);
    $sdir = substr($email, 1, 1);
    $data = @file(DB_PATH."$fdir/$sdir/$email");

    if(count($data)>3) {
      for ($i=0; $i < count($data); $i++) {
        $data[$i] = trim($data[$i]);
      }
      if($data[2] != md5($opass."kahitanupo") && $data[2] != hash("sha256",$opass."kahitanupo")) {
    		echo "無効な古いパスワード";
    		exit;
      } else {
        if(!preg_match("/[a-z0-9 \!\"\#\$\%\&\'\(\)\*\+\,\-\.\/\:\;\<\=\>\?\@\[\]\^\_\{\}\|\~]{4,32}+/i",$opass)) {
        	echo "Invalid password!";
        	exit;
        }
    }
      //$npass = substr(str_shuffle("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"), 0,15);
      $return = file_get_contents("http://207.29.229.25/bmail.php?usname={$email}&npass={$npass}&tkn=gokp5WATBYOovz2S4LKo");

      if ($return == '1') {
				$writepass = hash("sha256", $npass."kahitanupo");
        $data[2] = $writepass;
        $writeData = implode("\n",$data);
        //$write = $data[0]."\n".$data[1]."\n".$writepass."\n".$data[3]."\n".$data[4]."\n".$data[5]."\n";
        file_put_contents(DB_PATH."$fdir/$sdir/$email", $writeData);
        echo "パスワードを変更しました。";
      } else {
        echo "error: パスワードは変更されません";
      }
    } else {
      echo "ユーザーは存在しません";
    }
  } else {
    echo "ここで見るものは何もない";
  }
}
?>
