<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>BE Points Tool</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <style type="text/css">
      body {
        padding-top: 10px;
        padding-bottom: 40px;
        background-color: #f5f5f5;
      }

      .form-signin {
        max-width: 300px;
        padding: 19px 29px 29px;
        margin: 0 auto 20px;
        background-color: #fff;
        border: 1px solid #e5e5e5;
        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }
      .form-signin .form-signin-heading,
      .form-signin .checkbox {
        margin-bottom: 10px;
      }
      .form-signin input[type="text"],
      .form-signin input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
      }
      .navbar-nav {
        float: left;
        margin: 0;
      }
      .nav {
        margin-bottom: 0;
        padding-left: 0;
        list-style: none;
      }
      .navbar-collapse {
        content: " ";
      }
      .navbar-nav>li {
        float: left;
      }
      .nav>li {
        position: relative;
        display: block;
      }
      a {
        color: #428bca;
        text-decoration: none;
        font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
        font-size: 14px;
      }
      .navbar-nav>li>a {
        padding-top: 15px;
        padding-bottom: 15px;
      }
      .nav>li>a {
        position: relative;
        display: block;
        padding: 10px 15px;
      }
      .containerz {
        content: " ";
        display: table;
        margin-bottom: 30px;
      }
    </style>
  </head>
  <body><center>
  <div class="containerz">
    <div class="navbar-collapse">
      <ul class="nav navbar-nav">
        <li><a href="?spoint">Set BE Points</a></li>
        <li><a href="?mpoint">Subtract BE Points</a></li>
        <li><a href="?apoint">Add BE Points</a></li>
      </ul>
    </div>
  </div>
	<?php if(isset($_GET['spoint'])) {
	  echo "<div class=\"container\">
              <form class=\"form-signin\" method=\"post\" action=\"val_input.php\">
                <h2 class=\"form-signin-heading\">Set BE Points</h2>
                <input name=\"email\" type=\"text\" placeholder=\"Email address\" required>
                <input name=\"points\" type=\"text\" placeholder=\"Points to be set\" required>
                <br>
                <button class=\"btn btn-large btn-primary\" type=\"submit\">Submit</button>
              </form>
            </div>";
	} elseif(isset($_GET['mpoint'])) {
	  echo "<div class=\"container\">
              <form class=\"form-signin\" method=\"post\" action=\"chk_input.php\">
                <h2 class=\"form-signin-heading\">Subtract BE Points</h2>
		<input type=\"hidden\" name=\"opr\" value=\"sub\">
                <input name=\"id\" type=\"text\" placeholder=\"ID\" required>
                <input name=\"points\" type=\"text\" placeholder=\"Points to subtract\" required>
                <br>
                <button class=\"btn btn-large btn-primary\" type=\"submit\">Submit</button>
              </form>
            </div>";
	} elseif(isset($_GET['apoint'])) {
          echo "<div class=\"container\">
              <form class=\"form-signin\" method=\"post\" action=\"chk_input.php\"> 
                <h2 class=\"form-signin-heading\">Add BE Points</h2>
		<input type=\"hidden\" name=\"opr\" value=\"add\">
                <input name=\"id\" type=\"text\" placeholder=\"ID\" required>
                <input name=\"points\" type=\"text\" placeholder=\"Points to add\" required>
                <br>
                <button class=\"btn btn-large btn-primary\" type=\"submit\">Submit</button>
              </form>
            </div>";
        } else {
	  echo "<div class=\"container\">
              <form class=\"form-signin\" method=\"post\" action=\"val_input.php\">
                <h2 class=\"form-signin-heading\">Set BE Points</h2>
                <input name=\"email\" type=\"text\" placeholder=\"Email address\" required>
                <input name=\"points\" type=\"text\" placeholder=\"Points to be set\" required>
                <br>
                <button class=\"btn btn-large btn-primary\" type=\"submit\">Submit</button>
              </form>
            </div>";
	}
	?>

</center>
  </body>
</html>
