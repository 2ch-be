<?php
if (!isset($_COOKIE['DMDM']) && !isset($_COOKIE['MDMD'])) {
        header('location:index.php');
        exit;
}
include('encrypt.php');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="Shift_JIS">
    <title>パスワードを変更する</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
  </head>
  <body>
  <div class="containerz">
    <div class="navbar-collapse">
      <ul class="nav navbar-nav">
      </ul>          
    </div>
  </div>

  <div class="container">
    <center>
      <div class="form-signin" style = "font-size: 1.2em;text-align:right;">
        <?php if(detect_device2($_SERVER['HTTP_USER_AGENT']) == "PLSDISPLAY") { ?>
  <a href="http://be.2ch.net/">
    <img src="/css/img/2ch_logo.gif">
  </a>
<?php } else { ?>
    <img src="/css/img/2ch_logo.gif">
<?php } ?>
  <br>
        BE 2.0
      </div>
    </center>
  </div>

  <div class="container">
    <center>
    <form class="form-signin" method="post" action="c.php">
      <h3 class="form-signin-heading">パスワードを変更する</h3>
      <input type="password" name="opass" placeholder="古いパスワード" required autofocus>
      <input type="password" name="npass" placeholder="新しいパスワード" required>
      <input type="password" name="cpass" placeholder="新しいパスワードを再入力" required>
      <br><br>
      <input name="login" type="submit" value="変更する">
      <br><br><br>
	<a href="status.php">ステータスページ</a>
    </form>
  </center>
  </div>
  </body>
</html>
