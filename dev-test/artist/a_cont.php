<?php
include('encrypt_artist.php');
if (isset($_POST['asub'])) {
	if (isset($_POST['user']) && !empty($_POST['user']) && !empty($_POST['pass']) && !empty($_POST['cpass'])) {

		$email    = trim($_POST['user']);
		$pass     = trim($_POST['pass']);
		$confpass = trim($_POST['cpass']);
		
		$email    = filter_var($email, FILTER_SANITIZE_EMAIL);
		$pass     = filter_var($pass, FILTER_SANITIZE_STRING);
		$confpass = filter_var($confpass, FILTER_SANITIZE_STRING);

		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		echo "bad"; exit; }
		
		// Sanitize
		$email    = strip_tags(addslashes($email)); 
		$pass     = strip_tags(addslashes($pass));
		$confpass = strip_tags(addslashes($confpass));
		
		$fdir     = substr($email, 0, 1);
		$sdir     = substr($email, 1, 1);

		if ($pass != $confpass) {
			// echo "Error try again";
			echo "Password Mismatch!";
			exit;
		} else {
			if(!preg_match("/[a-z0-9 \!\"\#\$\%\&\'\(\)\*\+\,\-\.\/\:\;\<\=\>\?\@\[\]\^\_\{\}\|\~]{4,32}+/i",$pass))
				die("Invalid Password!");
		}

		if (!file_exists(ADBD_PATH."$fdir/$sdir/$email")) {
			@mkdir(ADBD_PATH."$fdir");
			@mkdir(ADBD_PATH."$fdir/$sdir");
			mkdir(ADBD_PATH."$fdir/$sdir/$email");
		}

		if (file_exists(ADBD_PATH."$fdir/$sdir/$email/info.txt")) {
			echo "User already exist!";
			exit;
		}

		$pass = hash("sha256", $pass."kahitanupo");
		$rid  = rand(11,99).rand(11,99).rand(11,99).rand(11,99);
		$writeData = "$rid\n$email\n$pass\nuser";
		file_put_contents(ADBD_PATH."$fdir/$sdir/$email/info.txt", $writeData);
		if (file_exists(ADBD_PATH."$fdir/$sdir/$email/info.txt")) {
			$ff = fopen(ADBD_PATH."artistlist.log", "a+");
      fwrite($ff, $rid."<><>".$email."\n");  // NEW CHANGE
      fclose($ff);
	    /*$return = file_get_contents("http://207.29.229.25/zz.php?usname={$email}&npass={$pass}&tkn=gokp5WATBYOovz2S4LKo");
	    if ($return == '1') {
		    $ff = fopen(ADBD_PATH."idlist.log", "a+");
	      fwrite($ff, $id."<><>".$email."\n");  // NEW CHANGE
	      fclose($ff);*/
	      echo "Successful! User created.";
		  /*}*/
	  }
	} else {
		echo "NG";
	}
}
?>
