<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Artist Page</title>
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/astyle.css">
	<script src="js/jquery-1.9.1.js"></script>
	<script src="js/bootstrap.js"></script>

	
	<script>
	$(document).ready(function(){
		$("[name=modalClose]").click(function(){
			$(".alert").remove();
		});
	});

	$(document).on('submit', '#categoryForm', function(){
			//hide the notification messages
			$('#success, #error').fadeOut();
	        
	        //show the loading image
         	$(".alert").remove();
			var val       = $('input[name=category]').val();
			var str       = $('#categoryForm').serialize();
	        $('.formloader').css({'background':'#F9F9F9 url("http://russoil.eu/images/load.gif") no-repeat center', 'opacity':.5, width:$(this).parent().width(), height:$(".modal-content").height(), position:'absolute', top:0});
			

	        //process
	        $.ajax({type:'POST', url: 'ajax/ajax.php', data: str, 

	        	success:function(response) {
	            if(response.a=='success'){
	                //success message
	             	$("#categoryForm").prepend('<div class="alert alert-success" id="alert">The '+val+' is successfully added</div>');
	                $('input[type=text]').val('');
	                $("#categoryList").append('<a class="list-group-item" href="?art&cat='+val+'">'+val+'</a>')
	                $("select[name=class]").append('<option value="'+response.b+'">'+val+'</option>')
	            }else{
	            	$("#categoryForm").prepend('<div class="alert alert-warning" id="alert">An error occured. Please contact the admin.</div>');
			        $('.formloader').remove();
	            }
	            
		            //remove the loading image
			        $('.formloader').remove();
	        }});        
			
			//stop the process
			return false;
		});
	</script>
</head>