<div class="col-md-3">
	<div class="panel panel-default">
		<div class="panel-heading">
			<center>
				<b>Options</b>
			</center>
		</div>
		<div class="list-group">
			<?php if ($_SESSION['utype'] == "admin") { ?>
			<a class="list-group-item <?php echo (isset($_GET['crar'])) ? "active" : "";  ?>" href="?crar">Create Artist Account</a>
			<a class="list-group-item <?php echo (isset($_GET['vtrans'])) ? "active" : "";  ?>" href="?vtrans">View Transactions</a>
			<a class="list-group-item <?php echo (isset($_GET['alist'])) ? "active" : "";  ?>" href="?alist">Artist List</a>
			<?php } elseif ($_SESSION['utype'] == "user") { ?>
			<a class="list-group-item <?php echo (isset($_GET['art'])) ? "active" : "";  ?>" href="?art">Art List (Uncategorized)  </a>
			<a class="list-group-item <?php echo (isset($_GET['usercat'])) ? "active" : "";  ?>" href="?usercat">User Category List</a>
			<a class="list-group-item <?php echo (isset($_GET['upload'])) ? "active" : "";  ?>" href="?upload">Upload Image</a>
			<?php } ?>
		</div>
	</div>
<!-- 	start panel -->	
<?php if($_SESSION['utype'] == "user") { 
	$category = file(ADBD_PATH."class.log");
	if(count($category)>0){
	?>
	<div class="panel panel-default">
		<div class="panel-heading">
			<center>
				<b>Category List</b>
			</center>
		</div>
		<div id="categoryList" class="list-group">
			<?php
						$getCat = (isset($_GET['cat'])) ? $_GET['cat'] : ""; 
					foreach ($category as $key => $value) {
						$name    = within_str($value, "<classname>", "</classname>");
						$id    = within_str($value, "<id>", "</id>");
						$current = ($getCat==$name) ? "active" : ""; 
						echo '<a class="list-group-item '.$current.'" href="?art&cat='.$name.'">'.ucfirst($name).'   <span class="badge pull-right">'.countClass($id, $_SESSION['artistid']).'</span></a>';						
					}
				
			?>
		</div>
	</div>
<?php }
}
?>
<!-- 	end panel heading -->
</div>
