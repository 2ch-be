<?php
ob_start();
if (!isset($_COOKIE['ADMDM']) && !isset($_COOKIE['AMDMD'])) {
        header('location:artist_login.php');
        exit;
}
include("a_cont.php");
if (!isset($_SESSION['utype'])) {
	header("location: a_out.php");
}

$email   = Decrypt($_COOKIE['ADMDM'],AKEY);
$dir     = loggedEmail($email, "adb/");
// echo $dir;
$howmany = file(ADBD_PATH."/transaction.log");
$dirFull = loggedEmail($email, ADBD_PATH);

function FindUser($id) {
	$list = file(ADBD_PATH."artistlist.log");
	$em   = "";
	$err  = true;
	foreach ($list as $key => $value) {
		if (substr(trim($value), 0, 9) == $id) {
			$r = explode("<><>", $value);
			$em = trim($r[1]);
			$err = false;
		}
	}
	if ($err) {
		return false;
	}
	return $em;
}


function FindClass($id) {
	$list = file(ADBD_PATH."class.log");
	$em   = "";
	$err  = true;
	// print_r($list);
	// echo ucfirst($id);
	foreach ($list as $key => $value) {
		$nameCat = within_str($value, "<classname>", "</classname>");
		if($id==$nameCat){
			$idest = within_str($value, "<id>", "</id>");
			break;
		}
	}
	return $idest;
}

function countClass($id, $sessionID) {
	$list = file(ADBD_PATH."imglist.log");
	$id = (empty($id)) ? "" : $id;
	$count = 0;
	foreach ($list as $key => $value) {
		$nameCat = within_str($value, "<classid>", "</classid>");
		$userID = within_str($value, "<id>", "</id>");
		$stats = within_str($value, "<stat>", "</stat>");
		if($id==$nameCat && $sessionID==$userID && $stats!="deleted"){
			$count++;
		}
	}
	return $count;
}


?>
<!DOCTYPE html>
<html lang="en">
<?php include('include/header.php'); ?>
<body>
	<div class="container body">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<center>
							<img src="css/img/2ch_logo.gif" alt="">
						</center>
					</div>
					<div class="main">
						<div class="pull-right" style="margin:10px 0px;"><a href="a_out.php" class="btn btn-danger btn-sm" style="color:white;">LOGOUT</a></div>
						<div class="pull-left" style="margin:10px 0px;">
							<h4><i>Welcome,</i><b> 
								<?php 
									$artistname = explode ("@", $_SESSION['aname']);
									echo $artistname[0];
								?>!</b></h4>
						</div>
						<div class="clearfix"></div>
						<div class="row">
							<?php include('include/nav.php'); ?>
							<div class="col-md-9">
									
								<?php
									if(empty($_GET)){
										echo "<h3><b>Artist Area!</b></h3>";
									}
									if(isset($_SESSION['msg'])){
										echo $_SESSION['msg'];
										$_SESSION['msg'] = '';
									}
								?>

									<?php if(isset($_GET['usercat'])){
									$category = file(ADBD_PATH."class.log");
									if(isset($_GET['delete'])){
										$id      = $_GET['delete'];
										$count   = 0;
										$breaker = 0;
										foreach ($category as $key => $value) {
											$forCat = within_str($value, "<id>", "</id>");
											$aid    = within_str($value, "<aid>", "</aid>");
											$name    = within_str($value, "<classname>", "</classname>");
											if($id==$forCat && $aid==$_SESSION['artistid']){
												$breaker = $count;
											}
											$count++;
										}
										echo $category[$breaker];
										unset($category[$breaker]);
										$data    = implode("", $category);
										$handler = fopen(ADBD_PATH."class.log", "w");
										fwrite($handler, $data);
										fclose($handler);

										$file    = file(ADBD_PATH."imglist.log");
										$test    = implode("", $file);
										$data    = str_replace("<classid>{$id}</classid>","<classid></classid>",$test);
										$handler = fopen(ADBD_PATH."imglist.log", "w");
										fwrite($handler, $data);
										fclose($handler);
										$_SESSION['msg'] = "<div class=\"alert alert-success\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>You've successfully edit the category</div>";
										header("Location: artist.php?usercat");
										exit;
									}

									if(!isset($_GET['edit'])){
											if(count($category)>0){
													$getCat = (isset($_GET['cat'])) ? $_GET['cat'] : ""; 
												$count = 0;
												foreach ($category as $key => $value) {
													$name    = within_str($value, "<classname>", "</classname>");
													$id      = within_str($value, "<id>", "</id>");
													$aid     = within_str($value, "<aid>", "</aid>");
													$current = ($getCat==$name) ? "active" : ""; 

													if($aid==$_SESSION['artistid']){

													echo '<p class="list-group-item '.$current.'" href="?art&cat='.$name.'"><span class="name">'.$name.'</span> 
													<a href="?usercat&edit='.$id.'" class="edit pull-right" title="Quick Edit" class="pull-right"><span class="glyphicon glyphicon-pencil"></span></a> 
													&nbsp;&nbsp;&nbsp;
													<a title="Remove Item"  data-toggle="modal" data-target="#'.$id.'"  class="pull-right"><span class="glyphicon glyphicon-ban-circle"></span></a></p>';
													?>

														<!-- Modal -->
														<div class="modal fade" id="<?=$id?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
														  <div class="modal-dialog">
														    <div class="modal-content">
														      <div class="modal-header">
														        <button type="button" class="close" data-dismiss="modal" name="modalClose"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
														        <h4 class="modal-title" id="myModalLabel">Delete Category</h4>
														      </div>
														        	<div class="modal-body">
        															    Are you sure you want to delete?
														        	</div>
														        	<div class="modal-footer">
																		        <button type="button" class="btn btn-default" data-dismiss="modal" name="modalClose">Close</button>
																		        <a href="?usercat&delete=<?=$id?>" class="btn btn-primary"> Delete </a>
														        	</div>
														    </div>
														  </div>
														</div>

													<?php
													$count++;
													}
												}

											if($count<0)
											echo "<h5>You've not created any category</h5>";

											}
									}else{
										$id = $_GET['edit'];

										$count = 0;
										foreach ($category as $key => $value) {
											$forCat = within_str($value, "<id>", "</id>");
											$aid    = within_str($value, "<aid>", "</aid>");
											$name   = within_str($value, "<classname>", "</classname>");
											if($id==$forCat && $aid==$_SESSION['artistid']){
												$count++;
												break;
											}
										}

										if($count<0)
											echo "<h5>You've not created any category</h5>";

										if(isset($_POST['submitedit'])){
											
											$idPOST       = $_POST['id'];
											$categoryPOST = trim($_POST['category']);

											if(empty($categoryPOST)){
												$_SESSION['msg'] = "<div class=\"alert alert-warning\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>You've entered blank on category field.</div>";
												header("Location: artist.php?usercat");
												exit;
											}

											
											$count        = 0;
											$breaker      = 0;
												foreach ($category as $key => $value) {
													$forCat = within_str($value, "<id>", "</id>");
													$aid    = within_str($value, "<aid>", "</aid>");
													$name   = within_str($value, "<classname>", "</classname>");
													if($forCat==$idPOST && $aid==$_SESSION['artistid']){
														$breaker = $count;
														break;
													}
													$count++;
												}
												// echo 	$category[$breaker];
											$category[$breaker] = str_replace("<classname>{$name}</classname>","<classname>{$categoryPOST}</classname>",$category[$breaker]);
											$data               = implode("", $category);
											// echo $data;
											// 	die();
											$handler            = fopen(ADBD_PATH."class.log", "w");
											fwrite($handler, $data);
											fclose($handler);
											$_SESSION['msg'] = "<div class=\"alert alert-success\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>You've successfully edit the category</div>";
											header("Location: artist.php?usercat");
											exit;
										}
									?>
									<div class="panel panel-default"><div class="panel-heading"><center><b> Edit Category </b></center> </div><br>
									<div class="panel panel-body">
											<form action="?usercat&edit=<?=$forCat ?>" method="POST">
												<label> Category Name </label>
												<input type="text" name="category" value="<?=$name?>" class="form-control" id="categoryInput">
												<input type="hidden" name="id" value="<?=$forCat?>" class="form-control">
												<div class="clearfix"></div><br>
												<input type="submit" name="submitedit" class="btn btn-success">
											</form>
									<?php
									}
										?>
									<?php }elseif(isset($_GET['art'])){ ?>
								<!-- Modal -->
								<div class="modal fade" id="addCat" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								  <div class="modal-dialog">
								    <div class="modal-content">
								      <div class="modal-header">
								        <button type="button" class="close" data-dismiss="modal" name="modalClose"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
								        <h4 class="modal-title" id="myModalLabel">Insert Category</h4>
								      </div>
							        	<form role="form" method="post" id="categoryForm" action="<?php echo $_SERVER['PHP_SELF'] ?>?art"> 
									      <div class="modal-body">
										          <div class="form-group">
										            <label for="category">Category Name</label>
										            <input type="text" class="form-control" id="category" name="category" placeholder="Enter Category Name">
										          </div>
									      </div>
									      <div class="modal-footer">
										        <button type="button" class="btn btn-default" data-dismiss="modal" name="modalClose">Close</button>
										        <input type="submit" name="category_submit" class="btn btn-primary" value="Submit">
									      </div>
									    </form>
								    </div>
								  </div>
								</div>
								<?php $title = (isset($_GET['cat'])) ? ucfirst(trim($_GET['cat'])) : "Others"; ?>
									<div class="panel panel-default"><div class="panel-heading"><center><b> <?= $title ?> </b></center> </div><br>
									<div class="panel panel-body">
									<button type="button"  data-toggle="modal" data-target="#addCat" class="btn btn-default btn-md btn-success pull-right">
									  <span class="glyphicon glyphicon-star"></span> Add Category
									</button>
								<?php
									if(file_exists(ADBD_PATH."imglist.log")){
										$classList = file(ADBD_PATH."class.log");
										$file      = file(ADBD_PATH."imglist.log");
										if(isset($_GET['c'])){
											if($_GET['c']=="delete"){
												$id       = $_GET['id'];
												$filename = within_str($file[$id], "<name>", "</name>");
												$file[$id]= str_replace("<stat></stat>","<stat>deleted</stat>",$file[$id]);
												$data     = implode("", $file);
												$handler  = fopen(ADBD_PATH."imglist.log", "w");
												fwrite($handler, $data);
												fclose($handler);
												$_SESSION['msg'] = "<div class=\"alert alert-success\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>We've successfully deleted the file.</div>";
												header("Location: artist.php?art");
											}
										}
										$i = 0;
										// echo "<pre>"; 
										// print_r($file);
										// echo"</pre>";
										if(isset($_POST['submit'])){
											$class = $_POST['class'];
											foreach ($_POST['check'] as $key => $value) {
												$tmp   = $file[$value];
												$name  = within_str($tmp, "<name>", "</name>");
												$price = within_str($tmp, "<price>", "</price>");
												$time  = within_str($tmp, "<time>", "</time>");
												$idVal = within_str($tmp, "<id>", "</id>");
												unset($file[$value]); //remove indexed value 
												$file[$value] = "<id>{$idVal}</id><name>{$name}</name><price>{$price}</price><stat></stat><classid>{$class}</classid>\n";
											}

											$data         = implode("", $file);
											$handler      = fopen(ADBD_PATH."imglist.log", "w");
											fwrite($handler, $data);
											fclose($handler);

											$_SESSION['msg'] = "<div class=\"alert alert-success\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>Successfully edit.</div>";
											header("Location: artist.php?art");
											exit;
											
										}
										
										echo '<form action="'.$_SERVER['PHP_SELF'].'?art" method="POST">';
										echo "<select name=\"class\" id=\"\" >";
											echo "<option value=''>Others</option>";
										foreach ($classList as $key => $value) {
											$id        = within_str($value, "<id>", "</id>");
											$classname = within_str($value, "<classname>", "</classname>");
											$selected = ($_GET['cat']==$classname) ? "selected" : "";
											echo "<option value='".$id."' $selected>{$classname}</option>";
										}
										echo "</select><div class=\"clearfix\"></div>";


										$classActive = (isset($_GET['cat']) ? FindClass($_GET['cat']) : "");
										$breaker = 0;
										foreach ($file as $key => $value) {
											$img   =  within_str($value, "<name>", "</name>").".gif";
											$stat  =  within_str($value, "<stat>", "</stat>");
											$id    =  within_str($value, "<id>", "</id>");
											$class =  within_str($value, "<classid>", "</classid>");
											
											if(!$stat && $id==$_SESSION['artistid'] && $class==$classActive){
												$hmctr = 0;
												$hmimg = explode(".gif", trim($img));

													foreach ($howmany as $key9 => $value9) {
														if (within_str(trim($value9), "<name>", "</name>") == trim($hmimg[0])) {
															$hmctr++;
														}
														/*echo within_str(trim($value9), "<name>", "</name>");*/
													}

												echo "<div class=\"panel panel-default alist_art pull-left\">";
												echo '<input type="checkbox" name="check[]" value="'.$i.'">';
												echo "<a href=\"#\" class=\"thumbnail\" style=\"margin-bottom: 1em!important;\">";
												echo "<img src=\"{$livesitePath}dev-test/premium/{$img}\" style=\"height:80px;\" alt=\"\">";
												echo "</a>";
												echo "<center>";
												echo "<span>" .within_str($value, "<price>", "</price>"). " MP<br>{$hmctr} time/s bought</span><br>";
												echo "<a href=\"artist.php?art&c=delete&id={$i}\">delete </a>";
												echo "<a href=\"artist.php?edit={$i}\">edit </a></center>";
												echo "<div class=\"clearfix\"></div>";
												echo "</div>";
												$breaker++;
											}
											$i++;											
										}
										$category = (!isset($_GET['cat'])) ? " (uncategorized) " : " (".$_GET['cat'].") ";
										echo ($breaker<=0) ? "<h5>No artwork{$category}uploaded</h5>" : "";
										echo "<div class=\"clearfix\"></div>";
									}else{
										echo "Please upload your work";
									}
									if($breaker>0){
										echo '<br><input type="submit" class="btn btn-success btn-sm" name="submit" value="Save Changes">';
									}
									echo "</div>";
									echo "</div>";
									echo '</form>';

									 }else if(isset($_GET['upload'])){
									if(isset($_POST['submitupload'])){
										if($_FILES['uploadedfile']['size']<100000){
											if(is_numeric($_POST['price'])){
												$class   = $_POST['class'];
												$price   = (int) $_POST['price'];
												$price   = round(abs($price));
												
												if($price < 0 || $price < 100){
													$price = 100;
												}elseif($price > 5000){
													$price = 5000;
												}
												
												
												$tmpName = $_FILES['uploadedfile']['tmp_name'];
												
												$fdir    = substr($email, 0, 1);
												$sdir    = substr($email, 1, 1);
												$path    = ADBD_PATH."{$fdir}/{$sdir}/{$email}";
												if (!file_exists($path)) {
													@mkdir(ADBD_PATH."{$fdir}");
													@mkdir(ADBD_PATH."{$fdir}/{$sdir}");
													@mkdir(ADBD_PATH."{$fdir}/{$sdir}/{$udir}");
												}
												if($_FILES["uploadedfile"]["type"] == "image/gif"){
												$dirPath         = "/home/auth/public_html/dev-test/premium/";
												$name            = rand(1000000,9999999);
												$fname           = $name.".gif";
													if(!file_exists($dirPath.$fname)){
														if(move_uploaded_file($tmpName, $dirPath.$fname)) {
														    $uploadedFile = true;
														} else{
														    $uploadedFile = false;
														}
													}else{
														$_SESSION['msg'] = "<div class=\"alert alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>File already exist.</div>";
														header("Location: artist.php?upload");
														exit;
													}
												}else{
													$_SESSION['msg'] = "<div class=\"alert alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>Please upload GIF image only.</div>";
													header("Location: artist.php?upload");
													exit;
												}
												if($uploadedFile){
													$handle = fopen(ADBD_PATH."imglist.log", "a+");
													fwrite($handle, "<id>".$_SESSION['artistid']."</id><name>{$name}</name><price>{$price}</price><stat></stat><classid>{$class}</classid>\n");
													fclose($handle);

													$_SESSION['msg'] = "<div class=\"alert alert-success\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>File successfully uploaded.</div>";
													header("Location: artist.php?art");
													exit;
												}else{
													$_SESSION['msg'] = "<div class=\"alert alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>Unable to upload file. Please contact admin.</div>";
													header("Location: artist.php?upload");
													exit;
												}

											}else{
												$_SESSION['msg'] = "<div class=\"alert alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>Price must be numeric.</div>";
												header("Location: artist.php?upload");
												exit;
											}
										}else{
												$_SESSION['msg'] = "<div class=\"alert alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>File size is more than 100kb.</div>";
												header("Location: artist.php?upload");
												exit;
										}
									}
										
									?>

										<div class="panel panel-default">
											<div class="panel-heading">
												<center><b>Upload Artwork</b></center>
											</div><br>
											<div class="panel-body">
												<form role="form"  style="width:50%;" enctype="multipart/form-data" action="<?php echo $_SERVER['PHP_SELF'] ?>?upload" method="POST">
														<div class="form-group">
															<label for="price">Price:</label> <br>
															<input name="price" type="text" class="form-control" placeholder="Price"/><br />
														</div>		
														<div class="form-group">
															<label for="price">Uploaded File: </label> <br>
															<input name="uploadedfile" class="form-control" type="file" placeholder="Price"/>
															<span style="font-size:12px;margin-top:3px;" class="pull-right">(only .gif image is accepted)</span>
															<div class="clearfix"></div>
														</div>	
														<div class="form-group">
															<label for="price">Category: </label> <br>
															<?php
															$classList = file(ADBD_PATH."class.log");
															echo "<select name=\"class\" id=\"\" class=\"form-control\">";
																echo "<option value=''>Others</option>";
															foreach ($classList as $key => $value) {
																$id        = within_str($value, "<id>", "</id>");
																$classname = within_str($value, "<classname>", "</classname>");
																echo "<option value='".$id."'>{$classname}</option>";
															}
															echo "</select><div class=\"clearfix\"></div>";
															?>
														</div>
													<input type="submit" class="btn btn-primary" name="submitupload" value="Upload File" />
													<input type="reset" class="btn btn-danger" name="submit" value="Reset" />
												</form>
											</div>
										</div>
									<?php }else if(isset($_GET['edit'])){
										$file  = file(ADBD_PATH."imglist.log");

										// print_r(expression)
										$index = $_GET['edit'];
										$value = $file[$index]; 
										$class = within_str($value, "<classid>", "</classid>");
										$name  = within_str($value, "<name>", "</name>");
										$price = within_str($value, "<price>", "</price>");
										$time  = within_str($value, "<time>", "</time>");
										$idVal = within_str($value, "<id>", "</id>");


										if(isset($_POST['submitedit'])){
											$index        = $_GET['edit'];

											$class        = $_POST['class'];
											$newPrice     = round(abs($_POST['price']));
											unset($file[$index]);

											if($newPrice < 0 || $newPrice < 100){

												$newPrice = 100;
											}elseif($newPrice > 5000){
												$newPrice = 5000;
											}
											
											$file[$index] = "<id>".$idVal."</id><name>{$name}</name><price>{$newPrice}</price><stat></stat><classid>{$class}</classid>\n";
											
											$data         = implode("", $file);
											$handler      = fopen(ADBD_PATH."imglist.log", "w");
											fwrite($handler, $data);
											fclose($handler);
											// rename("/home/auth/public_html/dev-test/premium/".$name.".gif", "/home/auth/public_html/dev-test/premium/".$newName.".gif");
											$_SESSION['msg'] = "<div class=\"alert alert-success\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>Successfully edit.</div>";
											header("Location: artist.php?art");
											exit;

										}
									 ?>

									 <div class="panel panel-default">
									 <div class="panel-body">
									 	<div class="panel panel-default alist_art">
									 		<a href="#" class="thumbnail">
									 			<img src="<?php echo $livesitePath ?>dev-test/premium/<?php echo $name ?>" style="height:80px;" alt="">
									 		</a>	
									 	</div>
										<div class="clearfix"></div>
										<br>
											<form role="form"  action="<?php echo $_SERVER['PHP_SELF'] ?>?edit=<?=$index?>" style="width:50%;" method="POST">
													<div class="form-group">
														<label for="price">Price:</label> <br>
														<input name="price" type="text" class="form-control" value="<?php echo within_str($value, "<price>", "</price>") ?>" placeholder="Price"/><br />
													</div>		
													<div class="form-group">
														<label for="price">Category: </label> <br>
														<?php
														$classList = file(ADBD_PATH."class.log");
														echo "<select name=\"class\" id=\"\" class=\"form-control\">";
															echo "<option value=''>Others</option>";
														foreach ($classList as $key => $value) {
															$id        = within_str($value, "<id>", "</id>");
															$classname = within_str($value, "<classname>", "</classname>");
															$selected  = ($class==$id) ? "selected" : "";
															echo "<option value='".$id."' $selected>{$classname}</option>";
														}
														echo "</select><div class=\"clearfix\"></div>";
														?>
													</div>
												<input type="submit" class="btn btn-primary" name="submitedit" value="Submit" />
											</form>
										</div>
									 </div>
									<?php } ?>

									<!-- nakadisplay none to option for create artist account -->
									<?php if (isset($_GET['crar'])) { ?>
										<div class="panel panel-default">
										<div class="panel-heading">
											<center><b>Add New Artist</b></center>
										</div><br>
										<div class="panel-body">
											<center>
												<form role="form" method="post" style="width:50%;"> 
												  <div class="form-group">
												    <label for="email" class="pull-left">Email address</label>
												    <input type="email" class="form-control" id="email" name="user" placeholder="Enter email">
												  </div>
												  <div class="form-group">
												    <label for="password" class="pull-left">Password</label>
												    <input type="password" class="form-control" id="password" name="pass" placeholder="Password">
												  </div>
												  <div class="form-group">
												    <label for="confirmpassword" class="pull-left">Confirm Password</label>
												    <input type="password" class="form-control" id="confirmpassword" name="cpass" placeholder="Confirm Password">
												  </div>
												  <input type="submit" class="btn btn-primary pull-left" name="asub" value="Create Artist">
												  <div class="clearfix"></div>
												</form>
											</center>
										</div>
									</div>
									<?php } ?>
									<!-- END nakadisplay none to option for create artist account -->
									
									<!-- option for view transaction -->
									<!-- option for view transaction -->
									<?php if (isset($_GET['vtrans'])) { ?>
									<div class="panel panel-default">
										<div class="panel-heading">
											<center><b>Transactions</b></center>
										</div><br>
										<div class="panel-body">
										<?php
										if (!file_exists(ADBD_PATH."artistlist.log")) {
											echo "No transactions";
										} else {
											if (!file_exists(ADBD_PATH."transaction.log")) {
												echo "No transactions";
												exit;
											} else {
												echo "<div class=\"panel-group\" id=\"accordion\"> 
												";
												$arus = file(ADBD_PATH."artistlist.log");
												$i    = 0;
												foreach ($arus as $key => $value) {
													$tmar 				= explode("<><>", $value);
													$usemail[$i]	= $tmar[1];
													$arid[$i]	    = $tmar[0];
													$i++;
												}
												$vtrans = file(ADBD_PATH."transaction.log");

												foreach ($vtrans as $key2 => $value2) {
													$arr = within_str($value2,"<id>","</id>");
													$val = within_str($value2,"<price>","</price>");
													$kk  = array_search($arr, $arid);
													/*echo $arr."-".$val."-".$kk."<br>";*/
													if (isset($arval[$kk])) {
														$arval[$kk] = $arval[$kk]+$val;
													} else {
														$arval[$kk] = $val;
													}
												}

												$breakerArid = 0;
												$ownnow = array();
												foreach ($arid as $key => $value) {
													if ($value!=66666666) {
														$kk  = array_search($value, $arid);
														if (isset($arval[$kk])) {
															$ownnow[trim($usemail[$kk])] = $arval[$kk];
															$breakerArid++;
														}
													}
												}

												foreach ($vtrans as $key => $value) {
													$imagesList[] = within_str($value,"<name>","</name>");
												}

												asort($imagesList);
												$breaker = 0;
												foreach ($vtrans as $key => $value) {
													$id = within_str($value,"<id>","</id>");
													$name = within_str($value,"<name>","</name>");
														$test[$id][$breaker]= [ "name" => $name,"count" => 0 , "total" => 0, "price" => within_str($value,"<price>","</price>") ] ;
														$test2[] = [ "name" => within_str($value,"<name>","</name>"), "artist_id" => within_str($value,"<id>","</id>"), "price" => within_str($value,"<price>","</price>")  ];
														$breaker++;
												}

												// $array =array_count_values(array_map(function($test2) {
												//     return $test2['name'];
												// }, $test2));
												
												$zeroList = array_map(function($test2) {
													// if($test2['price']>0){
												    	return $test2['name'];
													// }
												}, $test2);
												// print_r($test);
											$count_values = array();
											foreach ($test as $a) {
											  foreach ($a as $b) {
											  	// echo $b['name']."<br>";
											  	if(isset($count_values[$b['name']])){
											  		$count_values[$b['name']]["count"]++;
											  		$count_values[$b['name']]["price"] += $b['price'];
											    	// $count_values[$b['name']] = [ "count" =>  ];
											  	}else{
											    	$count_values[$b['name']] = [ "count" => 1, "price" => $b['price'] ];
											  	}



											  }
											}
											
												$zeroList  = array_filter($zeroList);
												$arrayTest = array_count_values($zeroList);
												// print_r($arrayTest);
												$ultimateFinalArray = array();
												foreach ($test as $key => $value) {
													foreach ($arrayTest as $key2 => $value2) {
														foreach ($value as $key3 => $value3) {
															if($value3['name']==$key2){
																// print_r($value);
															// echo $value3['name'];
																// $ultimateFinalArray[$key][$key2] = array( "count" => $value2, "total" => $count_values['']); 
																$ultimateFinalArray[$key][$key2] = array( "count" => $value2, "total" => $count_values[$value3['name']]['price']); 
															}
														}
													}
												}
												
												foreach ($ultimateFinalArray as $key => $value) {
													echo "
													<div class=\"panel panel-default\">
														<div class=\"panel-heading\">
															<a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#".$key."\">
															".FindUser($key)." ".$ownnow[FindUser($key)]."</a></div>
															<div id=\"".$key."\" class=\"panel-collapse collapse\">
															<div class=\"panel-body\">
															";

													foreach ($value as $key2 => $value2) {
														echo "<img src=\"{$livesitePath}/premium/{$key2}\" width=\"32px\" height=\"32px\" alt=\"{$key2}\">"."x ".$value2['count']." = ".$value2['total']."<br>";
													}
													echo "</div></div></div>";
												}
										}
									}
										?>
										</div>
										</div>
									</div>
									</div>


									<?php } ?>
									<!-- END option for view transaction -->
									<?php if (isset($_GET['alist'])) { ?>
									<div class="row">
										<div class="col-md-3">
											<div class="panel panel-default">
												<center>
													<h4>Artist List</h4>
												</center>
												<ul id="al" class="wordwrap_" style="list-style:none; display:inline;">
													<?php include("a_list.php"); ?>
												</ul>
											</div>
										</div>
										<div class="col-md-9">
											<div class="panel panel-default">
												<div class="panel-heading"><center><b>Artworks</b></center></div><br>
												<div class="panel-body">
												<?php												
													if (isset($_GET['v'])) {
														error_reporting(E_ALL); ini_set("display_errors", 1);
														if (!file_exists(ADBD_PATH."imglist.log")) {
															# code...
														} else {
															$__utmp = file(ADBD_PATH."imglist.log");
															foreach ($__utmp as $key => $value) {
																if (trim($_GET['v']) == trim(within_str($value,"<id>","</id>")) && trim(within_str($value,"<stat>","</stat>")) != "deleted") {
																	echo	"<div class=\"panel panel-default alist_art\">
																					<a href=\"#\" class=\"thumbnail\" style=\"margin-bottom: 1em!important;\">
																					<img src=\"http://be.2ch.net/dev-test/premium/".within_str($value,"<name>","</name>")."\" style=\"height:80px;\" alt=\"\">
																					</a>
																					<span class=\"pull-right\">".within_str($value, "<price>", "</price>")."MP</span>
																					<div class=\"clearfix\"></div>
																				</div>";
																}
															}
														}
													} else {
														echo "Select Artist";
													}
													?>
													</div>									
												<div class="clearfix"></div>
											</div>

										</div>
									</div>
									<?php } ?>
									<div class="clearfix"></div>
								<!-- </div> -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>

<!--  -->