<?php
session_start();
include('config.php');


function Encrypt($str, $key = "your_key") {
	return enc_dec_wkey("encrypt", $str, $key);
}
function Decrypt($str, $key = "your_key") {
	return enc_dec_wkey("decrypt", $str, $key);
}
function enc_dec_wkey($mode, $str, $key='') {
	if($key==='') return $str;
	if($mode=== "decrypt") $str = base64_decode($str);
	$key = str_replace(chr(32),'',$key);
	if(strlen($key) < 8) exit('key error');
	$kl = strlen($key) < 32 ? strlen($key) : 32;
	$k = array();
	for ($i = 0; $i < $kl; $i++) $k[$i] = ord($key{$i}) & 0x1F;
	$j = 0;
	for($i = 0; $i < strlen($str); $i++) {
	$e = ord($str{$i});
	$str{$i} = $e & 0xE0 ? chr($e^$k[$j]): chr($e);
	$j++;
	$j = $j == $kl ? 0 : $j;
	}
	if($mode == "encrypt")
	return base64_encode($str);
	else
	return $str;
}

function loggedEmail($email, $directory){
	$strReplaced = str_replace("@", "-", $email);
	$firstChar   = substr($email, 0, 1);
	$secondChar  = substr($email, 1, 1);
	$directory = $directory.$firstChar."/".$secondChar."/".$strReplaced."/";
	return $directory;
}
// Iphone true 
// Safari true
function detect_device($value){
        $iPod    = stripos($value,"iPod");
        $iPhone  = stripos($value,"iPhone");
        $iPad    = stripos($value,"iPad");
        $Android = stripos($value,"Android");
        if($iPod || $iPhone || $iPad || $Android){
            return TRUE;
        }else{
            return FALSE;
        }
}

function detect_device2($value){
             $breaker    = 2;
             $iPod       = preg_match("/iPod/i", $value);  
             $iPhone     = preg_match("/iPhone/i", $value); 
             $iPad       = preg_match("/iPad/i", $value);
             // $Android = stripos($value,"Android");
             $webOS      = (preg_match("/Safari/i",$value)) ? 2 : 0;
           //do something with this information
            $finalBreaker = $breaker+$webOS+$iPhone+$iPod+$iPad;
        $value = array(3);
        if($finalBreaker==3){
           return "DONTDISPLAY";
        }else{
            return "PLSDISPLAY";
        }
}

function Smilify($subject, $livesitePath){
    $smilies = array(
        '::amazed::'  => 'amazed',
        '::cry::' => 'cry',
        '::okay::' => 'okay',
        '::treasure::' => 'treasure',
        '::angry::' => 'angry',
        '::gangster::'  => 'gangster',
        '::shame::'  => 'shame',
        '::wink::'  => 'wink',
        '::annoyed::' => 'annoyed',
        '::laugh::'  => 'laugh',
        '::sick::' => 'sick',
        '::blush::'  => 'blush',
        '::sing::' => 'sing',
        '::chocked::'  => 'chocked',
        '::smile::' => 'smile',
        '::confused::'  => 'confused',
        '::ninja::'  => 'ninja',
        '::tongue::' => 'tongue',
        '::lawyer::'  => 'lawyer',
        '::guru::' => 'guru',
  
    );


    $replace = array();
    foreach ($smilies as $smiley => $imgName) {
        array_push($replace, ' <img src="'.$livesitePath.'images/'.$imgName.'.gif" alt="'.$smiley.'"/> ');
    }
    $subject = str_replace(array_keys($smilies), $replace, $subject);
    return $subject;
}


function androidappdetector($finalBreaker){
        if($finalBreaker=="bemessenger_android"){
           return "DONTDISPLAY";
        }else{
            return "PLSDISPLAY";
        }
}

if (isset($_COOKIE['DMDM']) && isset($_COOKIE['MDMD'])) {
	if(!isset($_SESSION['sLoggedDir'])){
        $email                  =  Decrypt($_COOKIE['DMDM'], KEY);
        $_SESSION['sLoggedDir'] = loggedEmail($email, MDB_PATH);
	}
}

?>
