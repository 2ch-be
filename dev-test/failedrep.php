<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="Shift_JIS">
  </head>
  <body>
<?php
if (!isset($_COOKIE['DMDM']) && !isset($_COOKIE['MDMD'])) {
	header('location:index.php');
  exit;
}
session_start();
include('encrypt.php');

//$sub = addslashes(trim($_POST['zxcvbnm']));
$sub = Decrypt(addslashes(trim($_POST['zxcvbnm'])), KEY);
$msg = trim($_POST['msg']);
$sbj = trim($_POST['sbj']);

if (strlen($msg) > 250) {
	$_SESSION['msg'] = "<div class=\"alert alert-danger\">長すぎるコンテンツ</div>";
	header('Location:message.php?d='.$file);
	exit;
}

$from = substr($sub, 0, 9);
$to   = substr($sub, 9, 9);
$file = substr($sub, 18);

$reply          = new Message;
if($reply->CheckBanMessaging()) {
$_SESSION['msg'] = "<div class=\"alert alert-danger\">BEポイントが足りないので、メッセージを送信することができません。</div>";
	header('Location:message.php?d='.$file);
	exit;
}

$msg            = $reply->BeSanitize($msg);
$reply_receiver = $reply->FindUser($to,$id_mail_path);

$fdir           = substr($reply_receiver, 0, 1);
$sdir           = substr($reply_receiver, 1, 1);
$udir           = str_replace("@", "-", $reply_receiver);
$file_path      = MDB_PATH."{$fdir}/{$sdir}/{$udir}/{$file}.dat";
$file_del_path  = MDB_PATH."{$fdir}/{$sdir}/{$udir}/del/{$file}.dat";
//$ufile_path     = MDB_PATH."{$fdir}/{$sdir}/{$udir}/u_{$file}.dat";
$tmp_copy_path  = MDB_PATH."{$fdir}/{$sdir}/{$udir}";

if (file_exists($tmp_copy_path."/ban.txt")) {
                        $banlist = file($tmp_copy_path."/ban.txt");
                        foreach ($banlist as $key => $value) {
                                if (trim($from) == trim($value)) {
                                        //echo "Message sending failed, You have been blocked by the user";
                                        $_SESSION['msg'] = "<div class=\"alert alert-danger\"> メッセージ送信が失敗し、ユーザーによってブロックされています </div>";
                                        header('Location:message.php?d='.$file);
                                        exit;
                                }
                        }
                }

$sender_path = $reply->FindUser($from,$id_mail_path);
if ($sender_path == false) {
	echo "NG";
	exit;
}

$allow_post = $reply->CheckPostLimit($from,$plimit_path,TRUE);
if (($allow_post >= 60) && ($allow_post != false)) {
        $_SESSION['msg'] = "<div class=\"alert alert-danger\">到達した後の制限は、次の分お待ちください</div>";
        header('Location:message.php?d='.$file);
	exit;
}

$allow_post = $reply->CheckPostLimit($from,$plimit_hpath,FALSE);
if (($allow_post >= 600) && ($allow_post != false)) {
        $_SESSION['msg'] = "<div class=\"alert alert-danger\">到達した後の制限は、次の1時間お待ちください</div>";
        header('Location:message.php?d='.$file);
	exit;
}

$fdir2 = substr($sender_path, 0, 1);
$sdir2 = substr($sender_path, 1, 1);
$udir2 = str_replace("@", "-", $sender_path);
$sender_file_path = MDB_PATH."{$fdir2}/{$sdir2}/{$udir2}/{$file}.dat";
//$usender_file_path = MDB_PATH."{$fdir2}/{$sdir2}/{$udir2}/u_{$file}.dat";
date_default_timezone_set("Asia/Tokyo");
$time = time();

$writeData 	 = "<id>{$time}</id><from>{$from}</from><to>{$to}</to><subj>{$sbj}</subj><msg>{$msg}</msg><read>0</read>\n";
$writeDataSender = "<id>{$time}</id><from>{$from}</from><to>{$to}</to><subj>{$sbj}</subj><msg>{$msg}</msg><read>1</read>\n";

if (file_exists($file_del_path)) {
	file_put_contents($file_path, "");
	rename($file_del_path, $from."_".$file_del_path);
	//unlink($file_del_path);
}

//echo $file_path."<br>";
//echo $sender_file_path;
if (!file_exists($file_path)) {
	//echo "--una--";
	if (file_exists($sender_file_path)) {
		if (!file_exists($tmp_copy_path)) {
			@mkdir(MDB_PATH."{$fdir}");
			@mkdir(MDB_PATH."{$fdir}/{$sdir}");
			@mkdir(MDB_PATH."{$fdir}/{$sdir}/{$udir}");
		}
		$aa = file($sender_file_path);
		$aa = implode("\n", $aa);
		$aa = str_replace("\n\n", "\n", $aa); // A pain before I found this bug!
		file_put_contents($file_path, $aa);
		$handle = fopen($sender_file_path, "a");
		//if(!fwrite($handle, $writeData)) {
		if(!fwrite($handle, $writeDataSender)) {
			echo "Error 1!";
			exit;
		}
		fclose($handle);
	} else {
		echo "Error 2!";
	}
	if (trim($sender_file_path) <> trim($file_path)) {
		$handle = fopen($file_path, "a");
		if(!fwrite($handle, $writeData)) {
			echo "Error 3!";
			exit;
		}
		fclose($handle);
	}

	$add_post = $reply->AddPostLimit($from,$plimit_path,TRUE);
	$add_post = $reply->AddPostLimit($from,$plimit_hpath,FALSE);

#	$_SESSION['msg'] = "<div class=\"alert alert-success\">正常に送信されたメッセージ</div>";
	$_SESSION['msg'] = "<div class=\"alert alert-success\">正常に送信されたメッセージ</div>";
	header('Location:message.php?d='.$file);	
	// header('Location:inbox.php');

} else {			
	//echo "--pangalawa--";
		$handle = fopen($sender_file_path, "a");
		//if(!fwrite($handle, $writeData)) {
		if(!fwrite($handle, $writeDataSender)) {
			echo "Error 4";
			exit;
		}
		fclose($handle);
	if (trim($sender_file_path) <> trim($file_path)) {
		$handle = fopen($file_path, "a");
		if(!fwrite($handle, $writeData)) {
			echo "Error 5";
			exit;
		}
		fclose($handle);
	}

$add_post = $reply->AddPostLimit($from,$plimit_path,TRUE);
$add_post = $reply->AddPostLimit($from,$plimit_hpath,FALSE);
#	$_SESSION['msg'] = "<div class=\"alert alert-success\">正常に送信されたメッセージ</div>";
	$_SESSION['msg'] = "<div class=\"alert alert-success\">メッセージを送信しました。</div>";
	// header('Location:inbox.php');	
	header('Location:message.php?d='.$file);	
}

?>
  </body>
</html>
