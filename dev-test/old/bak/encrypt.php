<?php

include('config.php');

function Encrypt($str, $key = "your_key") {
	return enc_dec_wkey("encrypt", $str, $key);
}
function Decrypt($str, $key = "your_key") {
	return enc_dec_wkey("decrypt", $str, $key);
}
function enc_dec_wkey($mode, $str, $key='') {
	if($key==='') return $str;
	if($mode=== "decrypt") $str = base64_decode($str);
	$key = str_replace(chr(32),'',$key);
	if(strlen($key) < 8) exit('key error');
	$kl = strlen($key) < 32 ? strlen($key) : 32;
	$k = array();
	for ($i = 0; $i < $kl; $i++) $k[$i] = ord($key{$i}) & 0x1F;
	$j = 0;
	for($i = 0; $i < strlen($str); $i++) {
	$e = ord($str{$i});
	$str{$i} = $e & 0xE0 ? chr($e^$k[$j]): chr($e);
	$j++;
	$j = $j == $kl ? 0 : $j;
	}
	if($mode == "encrypt")
	return base64_encode($str);
	else
	return $str;
}
?>
