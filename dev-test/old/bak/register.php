<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="shift_jis">
    <title>アカウントを登録</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
  </head>
  <body>
  <div class="containerz">
    <div class="navbar-collapse">
      <ul class="nav navbar-nav">
      </ul>          
    </div>
  </div>
  <div class="container">
    <center>
    <form class="form-signin" method="post" action="reg.php">
      <h3 class="form-signin-heading">アカウントを登録</h3>
      <input type="email" name="mail" placeholder="Eメール" required autofocus>
      <input type="password" name="pass" placeholder="パスワード" required>
      <input type="password" name="confpass" placeholder="パスワードを確認" required>
      <br><br>
      <button name="reg" type="submit">提出する</button><br><br>
      <a href="login.php">[ログイン]</a>
    </form>
  </center>
  </div>
  </body>
</html>
