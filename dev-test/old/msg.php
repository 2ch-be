<?php
error_reporting(E_ALL); ini_set("display_errors", 1);
define('MDB_PATH', '/home/mdb/');
include('messaging.class.php');

$id_mail_path	= "/home/db/idlist.log";
$id  = strip_tags(addslashes(trim($_POST['zxcvbnm'])));
$inf = strip_tags(addslashes(trim($_POST['msg'])));

$message = new Message;
$em	     = $message->BeSanitize($inf);
if ($em<>false) {
	$user  = $message->FindUser($id,$id_mail_path);
	$write = $message->WriteMDB($user,$em);

	if ($write) {
		echo "Message Sent";
	} else {
		echo "Error, please try again later";
	}
} else {
	echo "Content too long!";
}

?>
