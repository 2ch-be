<?php
class Message {
	function BeSanitize($string) {
	  if (strlen($string) > 250) {
	    $echo = "Content too long";
	    return false;
	  } else {
	  	$string = filter_var($string, FILTER_SANITIZE_STRING);
	    $string = str_replace("<", "&lt;", $string);
	    $string = str_replace(">", "&gt;", $string);
	    $string = str_replace("\n", "<br>", $string);
	    $string = str_replace(" ", "&nbsp;", $string);
	    $string = urlencode($string);
	    return $string;
	  }
	}

	function FindUser($id,$id_mail_path) {
		$list = file($id_mail_path);
		$em = "";
		foreach ($list as $key => $value) {
			if (substr($value, 0, 9) == $id) {
				$r = explode("<><>", $value);
				$em = trim($r[1]);
			}
		}
		return $em;
	}

	function WriteMDB($user, $inf) {
		$fdir = substr($user, 0, 1);
		$sdir = substr($user, 1, 1);
		$udir = str_replace("@", "-", $user);
		$path = MDB_PATH."{$fdir}/{$sdir}/{$udir}";
		if (!file_exists($path)) {
			@mkdir(MDB_PATH."{$fdir}");
			@mkdir(MDB_PATH."{$fdir}/{$sdir}");
			@mkdir(MDB_PATH."{$fdir}/{$sdir}/{$udir}");
		}
		$time = time();
		$writeData = "<id>{$time}</id><c_id></c_id><from>UNDECIDED_YET</from><msg>{$inf}</msg>\n";
		$filename  = $path."/u_".$time.".dat";
		if (!file_exists($filename)) {
			if(!file_put_contents($filename,$writeData)) {
				echo "Error!";
				return false;
			}
			echo $filename;
			echo $writeData;
		} else {
			$handle = fopen($filename, "a+");
			if(!fwrite($handle, $writeData)) {
				echo "Error!";
				return false;
			}
			fclose($handle);
			echo $writeData;
		}
		return true;
	}
}
?>