<?php

//DMDM = email
//MDMD = password
include('encrypt.php');
// isset if login or not
if (!isset($_COOKIE['DMDM']) && !isset($_COOKIE['MDMD'])) {
	header('location:index.php');
	exit;
}
$icoimg="";


$email = Decrypt($_COOKIE['DMDM'],KEY);
$email = filter_var($email, FILTER_SANITIZE_EMAIL);

if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
exit;
}

include('inf.php');

$fdir = substr($email, 0, 1);
$sdir = substr($email, 1, 1);
$data = @file(DB_PATH."$fdir/$sdir/$email");

if(count($data)<3) {
	foreach($_COOKIE as $ky => $vl)
		setcookie($ky,"",time()-3600);
	header('location:index.php');
	exit;
}

for ($i=0; $i < count($data); $i++) {
	$data[$i] = trim($data[$i]);
}
if(count($data)>5){
	$mystatus = '';
	if($data[count($data)-1]!=FORGOT){
		$mystatus = urldecode($data[count($data)-1]);
		$mystatus = str_replace("<br>","\n",$mystatus);
	}
}

$e_ = (isset($_GET['e_'])) ? strip_tags(addslashes(filter_var($_GET['e_'], FILTER_SANITIZE_STRING))):'';
if($e_=='1'){
	$isforedit=true;
}

if (isset($_GET['ico'])) {
	$ico = $_GET['ico'];
	$ico=trim($ico);
	if(preg_match('/[^a-z_\-0-9._]/i', $ico))
	{
	    $ico="";
	}

	$type=Array(1 => 'gif'); //store all the image extension types in array
	$ext = explode(".",$ico); //explode and find value after dot
	if(!(in_array($ext[1],$type))) //check image extension not in the array $type
	{
		echo "412";exit;
	}

	$icoimg = "<img src=\"http://204.63.8.28/ico/".$ico."\" height=\"100%\"/>";
	$data[5]=$ico;
	$writeData = implode("\n",$data);
	file_put_contents(DB_PATH."$fdir/$sdir/$email", $writeData);
	header('location:status.php');
}

if (isset($data[5]) && !empty($data[5])) {

$ico=trim($data[5]);
if(preg_match('/[^a-z_\-0-9._]/i', $ico))
{
    $ico="";
}
	$icoimg = "<img src=\"http://204.63.8.28/ico/".$ico."\" height=\"100%\"/>";
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="shift_jis">
    <title>ステータスページ</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <script type="text/javascript" src="js/jquery-1.9.1.js"></script>
    <script type="text/javascript">
    		$(document).ready(function() {
    			$('#trip_c').keyup(function(e){
				var code = (e.keyCode ? e.keyCode : e.which);
				if (code==13) {
					e.preventDefault();
					calctrip();
				}
    			});
    			$('#caltrip').click(function(){
				calctrip();
    			});
		});
		function calctrip(){
    				$.post(
    					'trip.php',
    					{s:$('#trip_c').val()},
    					function(e){
    						$('#trip_c').val(e);
    						$('#caltrip').attr('disabled','disabled');
    						setTimeout(function(){
							$('#caltrip').removeAttr('disabled');
    						},1000);
    					}
    				);
		}
    </script>    <style>
	.wordwrap_ {
		white-space: pre; /* CSS 2.0 */
		white-space: pre-wrap; /* CSS 2.1 */
		white-space: pre-line; /* CSS 3.0 */
		white-space: -pre-wrap; /* Opera 4-6 */
		white-space: -o-pre-wrap; /* Opera 7 */
		white-space: -moz-pre-wrap; /* Mozilla */
		word-wrap: break-word; /* IE 5+ */
	} 
    </style>
  </head>
  <body>
  <div class="containerz">
    <div class="navbar-collapse">
      <ul class="nav navbar-nav">
      </ul>          
    </div>
  </div>
  <div class="container">
    <center>
    <div class="form-signin">
      <h3 class="form-signin-heading">ステータスページ</h3><br>
      <a href="choose.php"><div class="ico"><?php echo $icoimg; ?></div></a>アイコンはクリックすると変更できます <br>
      <div class="info"><br>
      	<?php echo "Eメール: ".$data[1]."<br>点数: ".$data[3]; ?><br>
      	<span>Trip:</span>
      	<input type="text" name="trip" id="trip_c" />
      	<input type="submit" value="Calculate" id="caltrip" />
      	<br>
      	<a href="/test/p.php?i=<?php echo $data[0]; ?>">Profile</a>
      	<br><br>
        <form method="post">
        	<?php if(isset($echo)) echo "<span style=\"color:green\">$echo</span>"; ?>
        	<?php if(isset($isforedit)):?>
          <textarea id="input" name="info" cols="37" style="height:110px;width:100%" maxlength="250"><?php echo $mystatus; ?></textarea>
          <br>
          <button name="isub" type="submit">提出する</button>
          <button name="isca" type="submit">キャンセル</button>
          <?php else: ?>
          <div style="padding:2px;padding-bottom:10px;border:1px solid #dfdfdf;width:100%" class="wordwrap_"><?php echo $mystatus; ?></div>
          <?php
          	if(!empty($mystatus)){
          		print "<a href=\"status.php?e_=1\">編集</a>";
          	}else{
          		print "<a href=\"status.php?e_=1\">加える</a>";
          	}
          ?>
          <?php endif ?>
        </form>
        <br><br>
      	古いアカウントの移行 <br><a href="mailto:migrate@2ch.net">証明として、旧アカウントの詳細を電子メールで送信します。</a><br><br>
      	<a href="change_pass.php">パスワードを変更する</a><br>
      	<a href="logout.php" style="color:red">ログアウトする</a>
      </div>
    </div>
  </center>
  </div>
  </body>
</html>
