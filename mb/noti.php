<?php
error_reporting(E_ALL); ini_set("display_errors", 1);
require_once '../include/messaging.class.php';
$init = new Message;
if (!isset($_COOKIE['DMDM']) && !isset($_COOKIE['MDMD'])) {
	echo "NG";
	exit;
}

$email = urldecode(trim($_GET['un']));
if(filter_var($email, FILTER_VALIDATE_EMAIL))
	$email = $email;
else
	die("NG");

define('MDB_PATH', '/home/mdb/');


function loggedEmail($email, $directory){
        $strReplaced = str_replace("@", "-", $email);
        $firstChar   = substr($email, 0, 1);
        $secondChar  = substr($email, 1, 1);
        $directory   = $directory.$firstChar."/".$secondChar."/".$strReplaced."/";
        return $directory;
}

function within_str($subject, $lsearch, $rsearch) {
        $data = strstr($subject, $lsearch);
        $data = str_replace($lsearch, "", $data);
        $trim = strstr($data, $rsearch);

        return(str_replace($trim, "", $data));
}

function notification($directory,$init) {
        $i = 0;
        @chdir($directory);
        try{
                $files = glob("*.dat");
  foreach($files as $filename){
                                                                $value = $init->getContent($filename);
//echo "<pre>";
//print_r($value);
//echo "</pre>";                                                             
//echo max($value);   
$from =  $init->within_str(max($value), "<from>", "</from>");
                                                               // $to   = $init->within_str(max($value), "<to>", "</to>");
                                                                $id   =  $init->within_str(max($value), "<id>", "</id>");
                                                               // $subj =  $init->within_str(max($value), "<subj>", "</subj>");
                                                                $read =  $init->within_str(max($value), "<read>", "</read>");
                                                               // $msg  =  $init->within_str(max($value), "<msg>", "</msg>");
 								$array[$filename] = array("id" => $id, "read"=>$read, "from" => $from);
                                                               // $array[$filename] = array("id" => $id, "subj" =>$subj, "read"=>$read, "msg"=>$msg ,"from" => $from, "to" => $to, );
                                                        }
//print_r($array);
//$array = $init->array_sort($array, 'id', SORT_ASC);
//echo "<pre>";
//print_r($array);
//echo "</pre>";
                if(!$files){
                        throw new Exception("");
                }else{
		//	print_r($array);
			$array = $init->array_sort($array, 'id', SORT_ASC);  
                      foreach($array  as $filename => $value){
                               // $array = file($filename);
                               // $value = max($array);
                                //echo $value;
				$checkUnread = $value['read'];
                              if($checkUnread==0){
	                            	$date = $value['id'];
				 $i++;
                             }
                       }
                        return ($i==0) ? '' : $i.",".$date;
                }
        }catch(Exception $e){
                return $e->getMessage();
        }
}


$userDirectory = loggedEmail($email, MDB_PATH);

$count = notification($userDirectory,$init);
echo (empty($count)) ? "0,0" : $count;
?>
