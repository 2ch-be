<?php
session_start();
date_default_timezone_set("Asia/Tokyo");

function Encrypt($str, $key = "your_key") {
	return enc_dec_wkey("encrypt", $str, $key);
}
function Decrypt($str, $key = "your_key") {
	return enc_dec_wkey("decrypt", $str, $key);
}
function enc_dec_wkey($mode, $str, $key='') {
	if($key==='') return $str;
	if($mode=== "decrypt") $str = base64_decode($str);
	$key = str_replace(chr(32),'',$key);
	if(strlen($key) < 8) exit('key error');
	$kl = strlen($key) < 32 ? strlen($key) : 32;
	$k = array();
	for ($i = 0; $i < $kl; $i++) $k[$i] = ord($key{$i}) & 0x1F;
	$j = 0;
	for($i = 0; $i < strlen($str); $i++) {
	$e = ord($str{$i});
	$str{$i} = $e & 0xE0 ? chr($e^$k[$j]): chr($e);
	$j++;
	$j = $j == $kl ? 0 : $j;
	}
	if($mode == "encrypt")
	return base64_encode($str);
	else
	return $str;
}

function within_str($subject, $lsearch, $rsearch) {
    $data = strstr($subject, $lsearch);
    $data = str_replace($lsearch, "", $data);
    $trim = strstr($data, $rsearch);

	return(str_replace($trim, "", $data));
}

function loggedEmail($email, $directory){
	$strReplaced = $email;
	$firstChar   = substr($email, 0, 1);
	$secondChar  = substr($email, 1, 1);
	$directory   = $directory.$firstChar."/".$secondChar."/".$strReplaced."/";
	return $directory;
}

function detect_device2($value){
             $breaker    = 2;
             $iPod       = preg_match("/iPod/i", $value);
             $iPhone     = preg_match("/iPhone/i", $value);
             $iPad       = preg_match("/iPad/i", $value);
             // $Android = stripos($value,"Android");
             $webOS      = (preg_match("/Safari/i",$value)) ? 2 : 0;
           //do something with this information
            $finalBreaker = $breaker+$webOS+$iPhone+$iPod+$iPad;
        $value = array(3);
        if($finalBreaker==3){
           return "DONTDISPLAY";
        }else{
            return "PLSDISPLAY";
        }
}

$livesitePath  = "http" . (isset($_SERVER['HTTPS']) ? 's' : '')."://".$_SERVER['HTTP_HOST']."/";
$livesitePath2 = "http" . (isset($_SERVER['HTTPS']) ? 's' : '')."://".$_SERVER['HTTP_HOST'];
define('AKEY', "KungAnoAnoLang123456789");
define('ADB_PATH', "/home/adb/");
define('AFORGOT', "0._{_forgot_}_.0");

?>
