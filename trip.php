<?php
header("Content-type: text/html; charset=Shift_JIS");
include('encrypt.php');

if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH']!='XMLHttpRequest' )){ 
	header('location:index.php'); 
}
// isset if login or not
if (!isset($_COOKIE['DMDM']) && !isset($_COOKIE['MDMD'])) {
	header('location:index.php');
	exit;
}
function mktripcode($pw)
{
    /*$pw=mb_convert_encoding($pw,'SJIS','UTF-8');
    $pw=str_replace('&','&amp;',$pw);
    $pw=str_replace('"','&quot;',$pw);
    $pw=str_replace("'",'&#39;',$pw);
    $pw=str_replace('<','&lt;',$pw);
    $pw=str_replace('>','&gt;',$pw);
    
    $salt=substr($pw.'H.',1,2);
    $salt=preg_replace('/[^.\/0-9:;<=>?@A-Z\[\\\]\^_`a-z]/','.',$salt);
    $salt=strtr($salt,':;<=>?@[\]^_`','ABCDEFGabcdef');
    
    $trip=substr(crypt($pw,$salt),-10);*/
    	$trip = str_replace("+", "%2B", $pw);
		$trip = urldecode(unescape($trip));
		$trip = key2trip($trip);
    return $trip;
}
	/*
		JavaScript‚Åescape‚³‚ê‚½•¶Žš—ñ‚ðShift_JIS‚É•ÏŠ·
		Decode %uXXXX to Shift_JIS
		http://www.webdb.co.jp/aki/jsescapephp.html
	*/
	function unescape($para, $type = "Shift_JIS") {
		$ret = "";

		for($pos = 0; $pos < strlen($para); $pos++) {
			if(substr($para, $pos,2) == "%u") {
				$ret .= mb_convert_encoding(chr(hexdec(substr($para, $pos + 2, 2))) . chr(hexdec(substr($para, $pos + 4, 2))),$type, "UTF-16");
				$pos += 5;
			}

			else {
				$ret .= substr($para, $pos, 1);
			}
		}

		return $ret;
	}

	/*
		•¶Žš—ñ‚ðƒnƒ“ƒhƒ‹{ƒgƒŠƒbƒvŒ`Ž®‚Åo—Í‚·‚éƒ‹[ƒ`ƒ“
		Generate a name and a tripcode from string
	*/
	function str2name($str) {
		// "#"‚ªŠÜ‚Ü‚ê‚Ä‚¢‚È‚©‚Á‚½‚ç‘f’Ê‚µ
		if(strpbrk($str, "#") == false) {
			$handle = $str;
			$tripcode = "";
		}

		// ƒgƒŠƒbƒvƒL[‚ð•ÏŠ·
		else {
			list($handle, $key) = explode("#", $str, 2);
			$tripcode = key2trip($key);
		}

		// §ŒäƒR[ƒh‚ð”¼ŠpƒXƒy[ƒX‚É’uŠ·
		$handle = preg_replace("/[\x01-\x1f\x7f]/", "", $handle);

		// •¶Žš—ñ‚Ìæ“ª‚¨‚æ‚Ñ––”ö‚É‚ ‚éƒzƒƒCƒgƒXƒy[ƒX‚ðŽæ‚èœ‚­
		$handle = trim($handle);

		// “ÁŽê•¶Žš‚ðHTMLƒGƒ“ƒeƒBƒeƒB‚É•ÏŠ·‚·‚é
		$handle = htmlspecialchars($handle, ENT_QUOTES | ENT_HTML5, "Shift_JIS");

		// š‚ð™‚É’uŠ·
		$handle = str_replace("š", "™", $handle);
		// Ÿ‚ðž‚É’uŠ·
		$handle = str_replace("Ÿ", "ž", $handle);

		// ƒnƒ“ƒhƒ‹{ƒgƒŠƒbƒv‚ð•Ô‚·
		$str = $handle;
		if(!empty($tripcode))
			$str .= " Ÿ".$tripcode;

		return $str;
	}

	/*
		ƒgƒŠƒbƒv¶¬ƒ‹[ƒ`ƒ“
		Generate a tripcode from string
	*/
	function key2trip($key) {
		// 12ƒoƒCƒg–¢–ž‚ðˆ—
		if(strlen($key) < 12) {
			// ƒL[‚ª‹ó‚È‚ç0x80‚ð‘ã“ü
			if($key == "")
				$key = "\x80";

			// salt‚ð¶¬
			$salt = substr($key."H.", 1, 2);
			$salt = preg_replace("/[^\.-z]/", ".", $salt);
			$salt = strtr($salt, ":;<=>?@[\\]^_`", "ABCDEFGabcdef");

			// 10Œ…ƒgƒŠƒbƒv¶¬
			$trip = key2trip10($key, $salt);
		}

		// 12ƒoƒCƒgˆÈã‚Í•ªŠò
		else {
			// æ“ª‚ð’Šo
			$mark = substr($key, 0, 1);

			// "#"‚©‚çŽn‚Ü‚é‚à‚Ì‚ðˆ—
			if($mark == "#") {
				// ¶ƒL[Œ`Ž®‚Ìê‡10Œ…ƒgƒŠƒbƒv¶¬
				if(preg_match("|^#([[:xdigit:]]{16})([./0-9A-Za-z]{0,2})$|", $key, $str)) {
					$trip = key2trip10(hex2bin($str[1]), substr($str[2]."..", 0, 2));
				}

				// ‚»‚êˆÈŠO‚Í–¢ŽÀ‘•
				else
					$trip = "???";
			}

			// "$"‚©‚çŽn‚Ü‚é‚à‚Ì‚Í15Œ…ƒgƒŠƒbƒv¶¬
			else if($mark == "$")
				$trip = "???";
				//$trip = key2trip15($key);

			// ‚»‚êˆÈŠO‚Í12Œ…ƒgƒŠƒbƒv¶¬
			else
				$trip = key2trip12($key);
		}

		return $trip;
	}

	/*
		10Œ…ƒgƒŠƒbƒv¶¬ƒ‹[ƒ`ƒ“
		Generate a 10-characters tripcode
	*/
	function key2trip10($key, $salt) {
		// 0x80‚ðI’[‚Æ‚µ‚Äˆµ‚¤ˆ—
		// Replace 0x80 by 0x00 to be compatible with Perl
		$key = preg_replace("/\x80/", "\x00", $key);

		// ƒgƒŠƒbƒv•ÏŠ·
		$trip = crypt($key, $salt);
		$trip = substr($trip, -10);

		return $trip;
	}

	/*
		12Œ…ƒgƒŠƒbƒv¶¬ƒ‹[ƒ`ƒ“
		Generate a 12-characters tripcode
	*/
	function key2trip12($key) {
		$trip = sha1($key, true);
		$trip = base64_encode($trip);
		$trip = substr($trip, 0, 12);
		$trip = str_replace("+", ".", $trip);

		return $trip;
	}

	/*
		15Œ…ƒgƒŠƒbƒv¶¬ƒ‹[ƒ`ƒ“
		Generate a 15-characters tripcode
		It is used in 2ch.sc
	*/
	function key2trip15($key) {
		$trip = sha1($key, true);
		$trip = base64_encode($trip);
		$trip = substr($trip, 3, 15);

		$trip = strtr($trip, "+/", ".!");

		// ƒL[2•¶Žš–Ú‚ª”¼ŠpƒJƒi‚Ìê‡‚Í”¼ŠpƒJƒiƒgƒŠƒbƒv‚É’uŠ·
		// Replace ASCII by Halfwidth Katakana
		if(preg_match("/^[$][\xA1-\xDF]/", $key)) {
			$ascii	= "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.";
			$kana	= "¡¢£¤¥¦§¨©ª«¬­®¯°±²³´µ¶·¸¹º»¼½¾¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞß";
			$trip	= strtr($trip, $ascii, $kana);
		}

		return $trip;
	}


if(isset($_POST['s'])){
	$pw = $_POST['s'];
	// $pw = filter_var(urldecode($_POST['s']), FILTER_SANITIZE_STRING);
	//if(empty($pw)) exit;
	if(strlen($pw) == 0) {
	  $pw = "\n";
	} else {
	  $pw = mktripcode($pw);
	}
	print($pw);
	
	
	
	
	
	$email = Decrypt($_COOKIE['DMDM'],KEY);
	$email = filter_var($email, FILTER_SANITIZE_EMAIL);
	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		die("Invalid email");
	}else{
		$fdir = substr($email, 0, 1);
		$sdir = substr($email, 1, 1);
		$data = @file(DB_PATH."$fdir/$sdir/$email");
		if(count($data)>3) {
			for ($i=0; $i < count($data); $i++) {
				$data[$i] = trim($data[$i]);
			}
			
			$writeData = $data[0]."\n".$data[1]."\n".$data[2]."\n".$data[3]."\n".$data[4]."\n".$data[5]."\n".$data[6]."\n".$pw;
			if(file_put_contents(DB_PATH."$fdir/$sdir/$email", $writeData)) {
			} else {
				$echo = "failed!";
			}
		}
	}
}
?>
